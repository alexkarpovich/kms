import {combineReducers} from 'redux';
import {LessonTypeConst} from 'constants';
import PageModel from 'models/Page';
import LessonTypeModel from 'models/LessonType';


function item(state = LessonTypeModel, action) {
     switch(action.type) {
         case LessonTypeConst.LOAD_LESSON_TYPE_REQUEST:
             return {...state, isFetching: false, error: null};

         case LessonTypeConst.LOAD_LESSON_TYPE_SUCCESS:
             return {...state, ...action.data, isFetching: false};

         case LessonTypeConst.LOAD_LESSON_TYPE_FAILURE:
             return {...state, error: action.error, isFetching: false};

         default:
             return state;
     }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case LessonTypeConst.LOAD_LESSON_TYPE_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case LessonTypeConst.LOAD_LESSON_TYPE_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case LessonTypeConst.LOAD_LESSON_TYPE_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
