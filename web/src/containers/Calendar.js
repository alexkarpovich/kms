import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {routeActions} from 'react-router-redux';
import * as EventActions from 'actions/event';

class Calendar extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className="calendar-container">
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...EventActions
        }, dispatch)
    })
)(Calendar)
