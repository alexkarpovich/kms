import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

export default class LessonItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {lesson, order} = this.props;

        return (
            <li>
                <Link to={`/lesson/${lesson.id}`}>
                    {`${order+1}. ${lesson.name}`}
                </Link>
            </li>
        );
    }
}
