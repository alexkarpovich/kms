import {LessonConst} from 'constants';
import api from 'helpers/api';

export function loadLessonPage(params) {
    return async dispatch => {
        dispatch({type: LessonConst.LOAD_LESSON_PAGE_REQUEST});

        try {
            let data = await api({url: 'lesson/', query: params});

            dispatch({type: LessonConst.LOAD_LESSON_PAGE_SUCCESS, data})
        } catch(error) {
            dispatch({type: LessonConst.LOAD_LESSON_PAGE_FAILURE, error});
        }
    }
}

export function loadLesson(lessonId) {
    return async dispatch => {
        dispatch({type: LessonConst.LOAD_LESSON_REQUEST});

        try {
            let data = await api({url: `lesson/${lessonId}/`});

            dispatch({type: LessonConst.LOAD_LESSON_SUCCESS, data});
        } catch (error) {
            dispatch({type: LessonConst.LOAD_LESSON_FAILURE, error});
        }
    };
}

export function createLesson(lesson) {
    return async dispatch => {
        dispatch({type: LessonConst.CREATE_LESSON_REQUEST});

        try {
            await api({url: `lesson/`, method: 'post', data: lesson});

            dispatch({type: LessonConst.CREATE_LESSON_SUCCESS})
        } catch (error) {
            dispatch({type: LessonConst.CREATE_LESSON_FAILURE, error});
        }
    };
}

export function updateLesson(lesson) {
    return async dispatch => {
        dispatch({type: LessonConst.UPDATE_LESSON_REQUEST});

        try {
            await api({url: `lesson/${lesson.id}/`, method: 'put', data: lesson});

            dispatch({type: LessonConst.UPDATE_LESSON_SUCCESS});
        } catch (error) {
            dispatch({type: LessonConst.UPDATE_LESSON_FAILURE, error});
        }
    };
}

export function deleteLesson(lessonId) {
    return async dispatch => {
        dispatch({type: LessonConst.DELETE_LESSON_REQUEST});

        try {
            await api({url: `lesson/${lessonId}/`, method: 'delete'});

            dispatch({type: LessonConst.DELETE_LESSON_SUCCESS});
        } catch (error) {
            dispatch({type: LessonConst.DELETE_LESSON_FAILURE, error});
        }
    };
}
