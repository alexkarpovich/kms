import keymirror from 'keymirror';

export const LessonConst = keymirror({
    LOAD_LESSON_REQUEST: null,
    LOAD_LESSON_SUCCESS: null,
    LOAD_LESSON_FAILURE: null,

    LOAD_LESSON_PAGE_REQUEST: null,
    LOAD_LESSON_PAGE_SUCCESS: null,
    LOAD_LESSON_PAGE_FAILURE: null,

    CREATE_LESSON_REQUEST: null,
    CREATE_LESSON_SUCCESS: null,
    CREATE_LESSON_FAILURE: null,

    UPDATE_LESSON_REQUEST: null,
    UPDATE_LESSON_SUCCESS: null,
    UPDATE_LESSON_FAILURE: null,

    DELETE_LESSON_REQUEST: null,
    DELETE_LESSON_SUCCESS: null,
    DELETE_LESSON_FAILURE: null,
});
