import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {routeActions} from 'react-router-redux';
import * as GroupActions from 'actions/group';
import * as PermissionActions from 'actions/permission';

class Group extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className="group-container">
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...GroupActions,
            ...PermissionActions
        }, dispatch)
    })
)(Group);
