import './UserView.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import Markdown from 'react-markdown';
import PathHelper from 'helpers/path';
import LessonItem from './LessonItem';
import CourseItem from './CourseItem';

class UserView extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let {actions, params} = this.props;

        actions.loadUser(params.userId);
        actions.loadUserSkills(params.userId);
        actions.loadUserLessons(params.userId);
        actions.loadUserCourses(params.userId);
    }

    render() {
        let {user:{item: user, skills, lessons, courses}} = this.props;
        let avatarSrc = !!user.avatar && user.avatar.path ?
            PathHelper.image(user.avatar.path, 256, 256) : '/images/avatar256x256.png';

        return (
            <div className="user-view">
                <section className="content-header">

                </section>
                <section className="content">
                    <div className="profile">
                        <div className="avatar">
                            <img src={avatarSrc}/>
                        </div>
                        <div className="info">
                            <div className="full-name">
                                {`${user.firstName} ${user.lastName}`}
                            </div>
                            <div>
                                <label>
                                    <FormattedMessage id="user.username.title" />:
                                </label>
                                <span>{' ' + user.username}</span>
                            </div>
                            <div>
                                <label>
                                    <FormattedMessage id="user.email.title" />:
                                </label>
                                <span>{' ' + user.email}</span>
                            </div>
                            <div>
                                <label>
                                    <FormattedMessage id="user.phone.title" />:
                                </label>
                                <span>{' ' + user.phone}</span>
                            </div>
                            <div>
                                <label>
                                    <FormattedMessage id="user.skype.title" />:
                                </label>
                                <span>{' ' + user.skype}</span>
                            </div>
                            <div className="skills">
                                <label>
                                    <FormattedMessage id="skill.title.many" />:
                                </label>
                                {skills.data.map(item =>
                                    <span key={item.id} className="label label-success">
                                        {`${item.skill.name} | ${item.level}`}
                                    </span>)
                                }
                            </div>

                        </div>
                    </div>
                    <div className="row">
                        <div className="lessons col-lg-5 col-md-5">
                            <h4>
                                <FormattedMessage id="lesson.title.many" />
                            </h4>

                            <div className="items">
                            {lessons.data.length ?
                                lessons.data.map((lesson, key) =>
                                    <LessonItem key={key} order={key} lesson={lesson} />) :
                                <FormattedMessage id="lesson.empty" />
                            }
                            </div>
                        </div>
                        <div className="courses col-lg-5 col-md-5">
                            <h4>
                                <FormattedMessage id="course.title.many" />
                            </h4>

                            <div className="items">
                            {courses.data.length ?
                                courses.data.map((course, key) =>
                                    <CourseItem key={key} order={key} course={course} />) :
                                <FormattedMessage id="course.empty" />
                            }
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(UserView);
