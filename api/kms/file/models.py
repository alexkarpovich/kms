from django.db import models


class File(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    path = models.FileField()
    filename = models.CharField(max_length=255, blank=True, default='')
    content_type = models.CharField(max_length=30, blank=True, default='')
