from rest_framework import serializers
from .models import File


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        read_only_fields = ('created', 'path', 'filename', 'content_type')
