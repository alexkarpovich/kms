import React, {Component, PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import {Grid, GridConfig, GridMapping} from 'components/common/grid/Grid';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';


export default class EventTypeList extends Component {
    render() {
        let {eventType, actions} = this.props;

        let gridConfig = new GridConfig();
        let gridMapping = new GridMapping({
            actions: {
                loadPage: actions.loadEventTypePage,
                deleteEntity: actions.deleteEventType
            }
        });

        let columns = [
            {key: 'id', name: 'common.id.title'},
            {key: 'name', name: 'common.name.title'},
            {key: 'description', name: 'common.description.title'}
        ];

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="eventtype.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <Grid
                                entity={eventType.page}
                                config={gridConfig}
                                mapping={gridMapping}
                                columns={columns}
                                rootUrl={'/admin/event-type/'}
                            />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
