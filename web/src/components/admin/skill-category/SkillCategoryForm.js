import React, {Component, PropType} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import CommonComponent from 'components/common/CommonComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import SkillCategoryModel from 'models/SkillCategory';

class SkillCategoryForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {skillCategory:{item:category}, params} = props;

        this.state = {
            skillCategory: !!params.skillCategoryId ? {...category, parent_id: category.parent ?
                category.parent.id : null} : {...SkillCategoryModel}
        };

        this.handleChange = ::this.handleChange;
        this.createSkillCategory = ::this.createSkillCategory;
        this.updateSkillCategory = ::this.updateSkillCategory;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        /* TODO: Load all skillCategories */
        actions.loadSkillCategoryPage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        if (params.skillCategoryId) {
            actions.loadSkillCategory(params.skillCategoryId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {params, skillCategory: {item:category}} = newProps;

        if (this.state.skillCategory !== category && !!params.skillCategoryId) {
            this.state.skillCategory = {...category, parent_id: category.parent ? category.parent.id : null};
        }
    }

    updateSkillCategory() {
        let {actions, routeActions} = this.props;
        let skillCategory = {...this.state.skillCategory};
        delete skillCategory.parent;

        actions.updateSkillCategory(skillCategory).then(() => {
            routeActions.push('/admin/skill-category');
        });
    }

    createSkillCategory() {
        let {actions, routeActions} = this.props;

        let skillCategory = {...this.state.skillCategory};
        delete skillCategory.parent;

        actions.createSkillCategory(skillCategory).then(() => {
            routeActions.push('/admin/skill-category');
        });
    }

    handleChange(e) {
        let {id, type, value, checked} = e.target;

        this.state.skillCategory[id] = type == 'checkbox' ? checked : value;
        this.setState(this.state);
    }

    render() {
        let {intl: {formatMessage}, params, skillCategory:{page: skillCategoryPage}} = this.props;
        let isEdit = !!params.skillCategoryId;
        let {skillCategory} = this.state;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        console.log(skillCategoryPage);

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="skillCategory.title.many"/>
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title"/>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="name"
                                                onChange={this.handleChange}
                                                value={skillCategory.name}
                                                placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="description">
                                                <FormattedMessage id="common.description.title"/>
                                            </label>
                                        <textarea
                                            className="form-control"
                                            id="description"
                                            onChange={this.handleChange}
                                            value={skillCategory.description}
                                            placeholder={formatMessage({id: 'common.description.title'})}>
                                        </textarea>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="parent">
                                                <FormattedMessage id="common.parent.title"/>
                                            </label>
                                            <select
                                                id="parent_id"
                                                className="form-control"
                                                value={skillCategory.parent_id}
                                                onChange={this.handleChange}>
                                                <option>{formatMessage({id: 'common.action.select.title'})}</option>
                                                { skillCategoryPage.results.map(item => {
                                                    if ([item.id, item.parent ? item.parent.id : null].indexOf(skillCategory.id) === -1) {
                                                        return <option key={item.id} value={item.id}>{item.name}</option>;
                                                    }
                                                }) }
                                            </select>
                                        </div>

                                        <div className="checkbox">
                                            <label>
                                                <input
                                                    type="checkbox"
                                                    id="enabled"
                                                    onChange={this.handleChange}
                                                    checked={skillCategory.enabled}
                                                    placeholder={formatMessage({id: 'common.enabled.title'})}
                                                />
                                                <FormattedMessage id="common.enabled.title"/>
                                            </label>
                                        </div>

                                        <button type="button" className={submitClasses}
                                                onClick={isEdit ? this.updateSkillCategory : this.createSkillCategory}>
                                            <FormattedMessage id="grid.action.submit.title.up"/>
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(SkillCategoryForm);
