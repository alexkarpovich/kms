import './Grid.less';
import React, {Component, PropTypes} from 'react';
import GridHeader from './GridHeader';
import GridBody from './GridBody';
import GridFooter from './GridFooter';
import GridConfig from './settings/config';
import GridMapping from './settings/mapping.js';
import PageModel from 'models/Page';

class Grid extends Component {
    constructor(props, context) {
        super(props, context);
        let {config} = this.props;

        this.state = {
            pageNumber: 1,
            pageSize: config.paginateBy.default,
            ordering: {by: 'id', direction: '+'},
            searchValue: ''
        };
    }

    static propTypes = {
        config: PropTypes.object.isRequired,
        mapping: PropTypes.object.isRequired,
        columns: PropTypes.array.isRequired,
        rootUrl: PropTypes.string
    };

    static childContextTypes = {
        config: PropTypes.object.isRequired,
        mapping: PropTypes.object.isRequired,
        columns: PropTypes.array.isRequired,
        rootUrl: PropTypes.string,
        setPaginateBy: PropTypes.func,
        setPageNumber: PropTypes.func,
        setOrdering: PropTypes.func,
        search: PropTypes.func,
        deleteEntity: PropTypes.func
    };

    getChildContext() {
        return {
            config: this.props.config,
            mapping: this.props.mapping,
            columns: this.props.columns,
            rootUrl: this.props.rootUrl,
            setPaginateBy: ::this.setPaginateBy,
            setPageNumber: ::this.setPageNumber,
            setOrdering: ::this.setOrdering,
            search: ::this.search,
            deleteEntity: ::this.deleteEntity
        };
    }

    componentDidMount() {
        this.loadPage();
    }

    setPaginateBy(paginateBy) {
        this.state.pageSize = paginateBy;

        this.loadPage();
    }

    setPageNumber(pageNumber) {
        this.state.pageNumber = pageNumber;

        this.loadPage();
    }

    setOrdering(ordering) {
        this.state.ordering = ordering;

        this.loadPage();
    }

    search(searchValue) {
        this.state.searchValue = searchValue;
        this.state.pageNumber = 1;

        this.loadPage();
    }

    deleteEntity(entityId) {
        var {mapping} = this.props;

        mapping.actions.deleteEntity(entityId).then(() => {
            this.loadPage();
        });
    }

    loadPage() {
        let {mapping} = this.props;
        let {pageNumber, pageSize, ordering, searchValue} = this.state;

        mapping.actions.loadPage({
            page: pageNumber,
            size: pageSize,
            /* Temp solution */
            ordering: (ordering.direction === '+' ? '' : '-') + ordering.by,
            search: searchValue
        });
    }

    render() {
        let {entity} = this.props;
        let {pageNumber, pageSize, ordering} = this.state;
        let items = entity.results;

        return (
            <div className="box grid">
                <GridHeader />

                { entity.isFetching ?
                    <div className="overlay">
                        <i className="fa fa-spinner fa-spin"></i>
                    </div>
                    : null }

                <GridBody
                    entities={items}
                    ordering={ordering}
                    isLoading={entity.isFetching}
                />

                <GridFooter
                    pageNumber={pageNumber}
                    pageSize={pageSize}
                    count={entity.count}
                    hasPrev={entity.hasPrev}
                    hasNext={entity.hasNext}
                />
            </div>
        );
    }
}

export {Grid, GridConfig, GridMapping};
