import SimpleMDE from 'simplemde/dist/simplemde.min';
import React, {Component, PropTypes} from 'react';

let simplemde = null;

export default class MarkdownEditor extends Component {
    constructor(props) {
        super(props);

        this.onChange = ::this.onChange;
    }

    static propTypes = {
        id: React.PropTypes.string.isRequired,
        content: React.PropTypes.string,
        onChange: React.PropTypes.func
    };

    componentDidMount() {
        let {id, content} = this.props;
        let markdownEditor = this;

        simplemde = new SimpleMDE({
            element: $(`#${id}`)[0]
        });
        simplemde.value(content);
        simplemde.codemirror.on('change', markdownEditor.onChange);
    }

    componentWillReceiveProps(newProps) {
        if (simplemde && simplemde.value() !== newProps.content) {
            simplemde.value(newProps.content);
        }
    }

    onChange() {
        let {onChange} = this.props;

        onChange && onChange(simplemde.value());
    }

    render() {
        let {id} = this.props;
        return (
            <div className="markdown-editor">
                <textarea id={id}></textarea>
            </div>
        );
    }
}
