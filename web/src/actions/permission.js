import {PermissionConst} from 'constants';
import api from 'helpers/api';

export function loadPermissionPage(params) {
    return async dispatch => {
        dispatch({type: PermissionConst.LOAD_PERMISSION_PAGE_REQUEST});

        try {
            let data = await api({url: 'permission/', query: params});

            dispatch({type: PermissionConst.LOAD_PERMISSION_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: PermissionConst.LOAD_PERMISSION_PAGE_FAILURE, error});
        }
    }
}

export function loadPermission(permissionId) {
    return async dispatch => {
        dispatch({type: PermissionConst.LOAD_PERMISSION_REQUEST});

        try {
            let data = await api({url: `permission/${permissionId}/`});

            dispatch({type: PermissionConst.LOAD_PERMISSION_SUCCESS, data});
        } catch (error) {
            dispatch({type: PermissionConst.LOAD_PERMISSION_FAILURE, error});
        }
    };
}

export function createPermission(permission) {
    return async dispatch => {
        dispatch({type: PermissionConst.CREATE_PERMISSION_REQUEST});

        try {
            await api({url: `permission/`, method: 'post', data: permission});

            dispatch({type: PermissionConst.CREATE_PERMISSION_SUCCESS});
        } catch (error) {
            dispatch({type: PermissionConst.CREATE_PERMISSION_FAILURE, error});
        }
    };
}

export function updatePermission(permission) {
    return async dispatch => {
        dispatch({type: PermissionConst.UPDATE_PERMISSION_REQUEST});

        try {
            await api({url: `permission/${permission.id}/`, method: 'put', data: permission});

            dispatch({type: PermissionConst.UPDATE_PERMISSION_SUCCESS});
        } catch (error) {
            dispatch({type: PermissionConst.UPDATE_PERMISSION_FAILURE, error});
        }
    };
}

export function deletePermission(permissionId) {
    return async dispatch => {
        dispatch({type: PermissionConst.DELETE_PERMISSION_REQUEST});

        try {
            await api({url: `permission/${permissionId}/`, method: 'delete'});

            dispatch({type: PermissionConst.DELETE_PERMISSION_SUCCESS});
        } catch (error) {
            dispatch({type: PermissionConst.DELETE_PERMISSION_FAILURE, error});
        }
    };
}
