import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import Select from 'react-select';
import CommonComponent from 'components/common/CommonComponent';
import CourseModel from 'models/Course';

class CourseForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {course, params} = props;

        let lessonIds = course.item.lessons.map(lesson => lesson.id);
        let skillIds = course.item.skills.map(skill => skill.id);

        this.state = {
            course: !!params.courseId ? {...course.item, lesson_ids: lessonIds, skill_ids: skillIds} : {...CourseModel}

        };

        this.handleChange = ::this.handleChange;
        this.arrayChange = ::this.arrayChange;
        this.createCourse = ::this.createCourse;
        this.updateCourse = ::this.updateCourse;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        actions.loadSkillPage({
            page: 1,
            size: 1000,
            ordering: 'id'
        });

        actions.loadLessonPage({
            page: 1,
            size: 1000,
            ordering: 'id'
        });

        if (!!params.courseId) {
            actions.loadCourse(params.courseId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {params, course} = newProps;

        if (this.state.course !== course.item && params.courseId) {
            let lessonIds = course.item.lessons.map(lesson => lesson.id);
            let skillIds = course.item.skills.map(skill => skill.id);

            this.state.course = {...course.item, lesson_ids: lessonIds, skill_ids: skillIds};
        }
    }

    updateCourse() {
        let {actions, routeActions} = this.props;

        actions.updateCourse(this.state.course).then(() => {
            routeActions.push('/course');
        });
    }

    createCourse() {
        let {actions, routeActions} = this.props;

        let course = {...this.state.course};
        delete course.lessons;
        delete course.skills;

        actions.createCourse(course).then(() => {
            routeActions.push('/course');
        });
    }

    handleChange(e) {
        this.state.course[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    arrayChange(items, name) {
        this.state.course[name] = items.map(item => item.value);
        this.setState(this.state);
    }

    render() {
        let {intl: {formatMessage}, params, skill, lesson} = this.props;
        let isEdit = !!params.courseId;
        let {course} = this.state;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        let skills = skill.page.results.map(item => ({
            value: item.id,
            label: item.name
        }));

        let lessons = lesson.page.results.map(item => ({
            value: item.id,
            label: item.name
        }));

        return (
            <div className="course-creator">
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="course.title.many" />
                    </h1>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title" />
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="name"
                                                onChange={this.handleChange}
                                                value={course.name}
                                                placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="description">
                                                <FormattedMessage id="common.description.title" />
                                            </label>
                                            <textarea
                                                className="form-control input-sm"
                                                id="description"
                                                onChange={this.handleChange}
                                                value={course.description}
                                                placeholder={formatMessage({id: 'common.description.title'})}>
                                            </textarea>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="complexity">
                                                <FormattedMessage id="common.complexity.title" />
                                            </label>
                                            <input
                                                type="number"
                                                className="form-control input-sm"
                                                id="complexity"
                                                onChange={this.handleChange}
                                                value={course.complexity}
                                                placeholder={formatMessage({id: 'common.complexity.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="skills">
                                                <FormattedMessage id="skill.title.many" />
                                            </label>
                                            <Select
                                                name="skill_ids"
                                                options={skills}
                                                value={course.skill_ids}
                                                onChange={(skills) => this.arrayChange(skills, 'skill_ids')}
                                                multi={true}
                                                placeholder={formatMessage({id: 'common.action.select.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="lessons">
                                                <FormattedMessage id="lesson.title.many" />
                                            </label>
                                            <div className="form-group">
                                                <Link to={'/lesson/create'} className="btn btn-xs btn-primary">
                                                    <span className="fa fa-plus fa-lg"></span>
                                                    {' Lesson'}
                                                </Link>
                                            </div>
                                            <Select
                                                name="lesson_ids"
                                                options={lessons}
                                                value={course.lesson_ids}
                                                onChange={(lessons) => this.arrayChange(lessons, 'lesson_ids')}
                                                multi={true}
                                                placeholder={formatMessage({id: 'common.action.select.title'})}
                                            />

                                        </div>

                                        <button type="button" className={submitClasses} onClick={isEdit ? this.updateCourse : this.createCourse}>
                                            <FormattedMessage id="grid.action.submit.title.up" />
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(CourseForm);
