'use strict';

import React, {Component} from 'react';
import { Link } from 'react-router';
import {FormattedMessage, formatMessage} from 'react-intl'

export default class GridHeader extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            searchValue: '',
            paginateBy: this.context.config.default
        };

        this.handleChange = ::this.handleChange;
        this.handleKeyPress = ::this.handleKeyPress;
        this.changePaginateBy = ::this.changePaginateBy;
        this.search = ::this.search;
    }

    static contextTypes = {
        config: React.PropTypes.object.isRequired,
        rootUrl: React.PropTypes.string,
        setPaginateBy: React.PropTypes.func,
        search: React.PropTypes.func
    };

    handleChange(e) {
        this.state[e.target.name] = e.target.value;
        this.setState(this.state);
    }

    handleKeyPress(e) {
        if (e.charCode == 13) {
            this.search();
        }
    }

    changePaginateBy(e) {
        this.state.paginateBy = e.target.value;
        this.setState(this.state);
        this.context.setPaginateBy(this.state.paginateBy);
    }

    search() {
        this.context.search(this.state.searchValue);
    }

    render() {
        let {config} = this.context;
        let {searchValue, paginateBy} = this.state;

        let paginateByOptions = config.paginateBy.items.map(item => {
            return <option key={item} value={item}>{item}</option>;
        });

        return (
            <div className="box-header">
                <h3 className="box-title">
                    { config.gridControls.createNew ?
                        <div className="input-group-sm">
                            <Link to={this.context.rootUrl + 'create'} className="btn btn-xs btn-default">
                                <span className="fa fa-plus fa-lg"></span>
                            </Link>
                        </div>
                        : null }
                </h3>
                <div className="box-tools pull-right">
                    <div className="clearfix">
                        <div className="tool">
                            { config.gridControls.searchField ?
                                <div className="input-group search-field">
                                    <input type="text"
                                           name="searchValue"
                                           className="form-control input-sm"
                                           value={searchValue}
                                           onChange={this.handleChange}
                                           onKeyPress={this.handleKeyPress}
                                           aria-describedby="search-addon"
                                           placeholder="Search"/>
                                <span className="input-group-btn">
                                    <button type="button"
                                            className="btn btn-default btn-sm"
                                            onClick={this.search}>
                                        <i className="fa fa-search"></i>
                                    </button>
                                </span>
                                </div>
                                : null }
                        </div>
                        <div className="tool">
                            { config.gridControls.paginateBy ?
                                <div className="input-group input-group-sm">
                                    <select className="paginate-by form-control"
                                            value={paginateBy}
                                            onChange={this.changePaginateBy}>
                                        {paginateByOptions}
                                    </select>
                                </div>
                                : null }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
