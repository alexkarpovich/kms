import {combineReducers} from 'redux';
import {LessonConst} from 'constants';
import LessonModel from 'models/Lesson';
import PageModel from 'models/Page';


function item(state = LessonModel, action) {
    switch(action.type) {
        case LessonConst.LOAD_LESSON_REQUEST:
            return {...state, isFetching: false, error: null};

        case LessonConst.LOAD_LESSON_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case LessonConst.LOAD_LESSON_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case LessonConst.LOAD_LESSON_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case LessonConst.LOAD_LESSON_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case LessonConst.LOAD_LESSON_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
