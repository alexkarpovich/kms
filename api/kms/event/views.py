from common.mixins import FilterModelViewSetMixin
from .models import Event, EventType
from .serializers import EventSerializer, EventTypeSerializer


class EventTypeViewSet(FilterModelViewSetMixin):
    queryset = EventType.objects.all()
    serializer_class = EventTypeSerializer
    search_fields = ('id', 'name', 'description')
    ordering_fields = ('id', 'name', 'description')


class EventViewSet(FilterModelViewSetMixin):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    search_fields = ('id', 'name', 'description', 'type__name', 'start', 'end')
    ordering_fields = ('id', 'name', 'description', 'type', 'start', 'end')
