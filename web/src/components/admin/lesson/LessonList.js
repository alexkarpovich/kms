import React, {Component, PropType} from 'react';
import {FormattedMessage} from 'react-intl';
import {Grid, GridConfig, GridMapping} from 'components/common/grid/Grid';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';


export default class LessonList extends Component {
    render() {
        let {lesson, actions} = this.props;
        let gridConfig = new GridConfig();
        let gridMapping = new GridMapping({
            actions: {
                loadPage: actions.loadLessonPage,
                deleteEntity: actions.deleteLesson
            }
        });

        let columns = [
            {key: 'id', name: 'common.id.title'},
            {key: 'name', name: 'common.name.title'},
            {key: 'type', name: 'common.type.title'},
            {key: 'content', name: 'common.content.title'},
        ];

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="lesson.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <Grid
                                entity={lesson.page}
                                config={gridConfig}
                                mapping={gridMapping}
                                columns={columns}
                                rootUrl={'/admin/lesson/'}
                            />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
