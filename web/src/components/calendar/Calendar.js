import $ from 'jquery';
import 'fullcalendar/dist/fullcalendar';
import React, {Component, PropTypes} from 'react';

function prepareEvents(events) {
    return events.map(event => {
            return {
                title: event.name,
                description: event.description,
                start: event.start,
                end: event.end
            };
        });
}

export default class Calendar extends Component {
    constructor(props) {
        super(props);

        let {event} = props;

        this.state = {
            eventPage: event.page
        };
    }

    componentDidMount() {
        let {actions} = this.props;
        let {eventPage} = this.state;

        /* TODO: temp solution */
        actions.loadEventPage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        let preparedEvents = prepareEvents(eventPage.results);

        $('#calendar').fullCalendar({
            firstDay: 1,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            events: preparedEvents
        });
    }

    render() {
        return (
            <div className="calendar">
                <div id="calendar"></div>
            </div>
        );
    }
}
