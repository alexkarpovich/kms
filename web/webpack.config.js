var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var _bower = path.join(__dirname, 'bower_components/');

module.exports = {
    devtool: 'eval',
    entry: {
        bundle: [
            'webpack-dev-server/client?http://local.kms.com:3003',
            'webpack/hot/only-dev-server',
            './src/index'
        ]
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: '[name].js',
        publicPath: '/static/'
    },
    resolve: {
        modulesDirectories: ['node_modules', 'bower_components'],
        root: [path.join(__dirname, 'src')],
        alias: {
            images: '../images',
            actions: 'actions',
            components: 'components',
            containers: 'containers',
            constants: 'constants',
            configs: 'configs',
            utils: 'utils',
            helpers: 'helpers',
            models: 'models',
            moment: path.join(_bower, 'momentjs/moment'),
            jquery: path.join(_bower, 'jquery/dist/jquery')
        }
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new ExtractTextPlugin('style.css'),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        })
    ],

    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['react-hot', 'babel'],
                include: path.join(__dirname, 'src'),
                exclude: [/bower_components/, /node_modules/]
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract('style', 'css!less')
            },
            {
                test: /\.(png|jpe?g|gif|svg|eot|woff|woff2|ttf)(\?.*$|$)/,
                loader: 'file'
            },
            {
                test: /\.json$/,
                loader: 'json'
            }
        ]
    }
};
