import {SkillCategoryConst} from 'constants';
import api from 'helpers/api';

export function loadSkillCategoryPage(params) {
    return async dispatch => {
        dispatch({type: SkillCategoryConst.LOAD_SKILL_CATEGORY_PAGE_REQUEST});

        try {
            let data = await api({url: 'skill-category/', query: params});

            dispatch({type: SkillCategoryConst.LOAD_SKILL_CATEGORY_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: SkillCategoryConst.LOAD_SKILL_CATEGORY_PAGE_FAILURE, error});
        }
    }
}

export function loadSkillCategory(skillCategoryId) {
    return async dispatch => {
        dispatch({type: SkillCategoryConst.LOAD_SKILL_CATEGORY_REQUEST});

        try {
            let data = await api({url: `skill-category/${skillCategoryId}/`});

            dispatch({type: SkillCategoryConst.LOAD_SKILL_CATEGORY_SUCCESS, data});
        } catch (error) {
            dispatch({type: SkillCategoryConst.LOAD_SKILL_CATEGORY_FAILURE, error});
        }
    };
}

export function createSkillCategory(skillCategory) {
    return async dispatch => {
        dispatch({type: SkillCategoryConst.CREATE_SKILL_CATEGORY_REQUEST});

        try {
            await api({url: `skill-category/`, method: 'post', data: skillCategory});

            dispatch({type: SkillCategoryConst.CREATE_SKILL_CATEGORY_SUCCESS});
        } catch (error) {
            dispatch({type: SkillCategoryConst.CREATE_SKILL_CATEGORY_FAILURE, error});
        }
    };
}

export function updateSkillCategory(skillCategory) {
    return async dispatch => {
        dispatch({type: SkillCategoryConst.UPDATE_SKILL_CATEGORY_REQUEST});

        try {
            await api({url: `skill-category/${skillCategory.id}/`, method: 'put', data: skillCategory});

            dispatch({type: SkillCategoryConst.UPDATE_SKILL_CATEGORY_SUCCESS});
        } catch (error) {
            dispatch({type: SkillCategoryConst.UPDATE_SKILL_CATEGORY_FAILURE, error});
        }
    };
}

export function deleteSkillCategory(skillCategoryId) {
    return async dispatch => {
        dispatch({type: SkillCategoryConst.DELETE_SKILL_CATEGORY_REQUEST});

        try {
            await api({url: `skill-category/${skillCategoryId}/`, method: 'delete'});

            dispatch({type: SkillCategoryConst.DELETE_SKILL_CATEGORY_SUCCESS});
        } catch (error) {
            dispatch({type: SkillCategoryConst.DELETE_SKILL_CATEGORY_FAILURE, error});
        }
    };
}
