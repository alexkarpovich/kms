# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import markdownx.models


class Migration(migrations.Migration):

    dependencies = [
        ('lesson', '0003_auto_20160222_1940'),
    ]

    operations = [
       migrations.AlterField(
            model_name='lesson',
            name='content',
            field=markdownx.models.MarkdownxField(blank=True, default=''),
        ),
    ]
