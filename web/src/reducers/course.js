import {combineReducers} from 'redux';
import {CourseConst} from 'constants';
import CourseModel from 'models/Course';
import PageModel from 'models/Page';


function item(state = CourseModel, action) {
    switch(action.type) {
        case CourseConst.LOAD_COURSE_REQUEST:
            return {...state, isFetching: false, error: null};

        case CourseConst.LOAD_COURSE_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case CourseConst.LOAD_COURSE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case CourseConst.LOAD_COURSE_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case CourseConst.LOAD_COURSE_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case CourseConst.LOAD_COURSE_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
