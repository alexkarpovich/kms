# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('skill', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='skill',
            name='description',
            field=models.CharField(max_length=1000, default='', blank=True),
        ),
        migrations.AlterField(
            model_name='skill',
            name='name',
            field=models.CharField(default='', max_length=255),
        ),
        migrations.AlterField(
            model_name='skillcategory',
            name='description',
            field=models.CharField(max_length=1000, default='', blank=True),
        ),
        migrations.AlterField(
            model_name='skillcategory',
            name='name',
            field=models.CharField(default='', max_length=255),
        ),
    ]
