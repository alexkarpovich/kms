import './LessonList.less';
import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {FormattedMessage, injectIntl} from 'react-intl';
import LessonItem from './LessonItem';

class LessonList extends Component {
    constructor(props) {
        super(props);

        this.loadLessons = ::this.loadLessons;
        this.searchLessons = ::this.searchLessons;
        this.handleKeyPress = ::this.handleKeyPress;
    }

    componentDidMount() {
        this.loadLessons();
    }

    loadLessons(searchValue) {
        let {actions} = this.props;

        actions.loadLessonPage({
            page: 1,
            size: 1000,
            ordering: 'id',
            search: searchValue ? searchValue : ''
        });
    }

    searchLessons() {
        let searchValue = this.refs.search.value;
        this.loadLessons(searchValue);
    }

    handleKeyPress(e) {
        if (e.charCode == 13) {
            this.searchLessons();
        }
    }

    render() {
        let {lesson: {page: lessonPage}, actions, routeActions, auth: {authUser}} = this.props;
        return (
            <div className="lesson-list">
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="lesson.title.many" />
                    </h1>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="actions">
                                <div className="new-lesson">
                                    <div className="form-group">
                                        <Link to={'/lesson/create'} className="btn btn-sm btn-primary">
                                            <span className="fa fa-plus fa-lg"></span>
                                            {' Lesson'}
                                        </Link>
                                    </div>
                                </div>
                                <div className="search">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            ref="search"
                                            className="form-control input-sm"
                                            onKeyPress={this.handleKeyPress}
                                            placeholder="Search..."
                                        />
                                        <span className="input-group-btn">
                                            <button
                                                className="btn btn-sm btn-default"
                                                type="button"
                                                onClick={this.searchLessons}
                                            >
                                                Search
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                { lessonPage.results.map(lesson => {
                                    return <LessonItem
                                        key={lesson.id}
                                        lesson={lesson}
                                        deleteLesson={actions.deleteLesson}
                                        loadLessons={this.loadLessons}
                                        routeActions={routeActions}
                                        authUser={authUser}
                                    />
                                }) }
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(LessonList);
