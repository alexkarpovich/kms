from rest_framework import status
from rest_framework.views import APIView
from common.mixins import AllowAnyMixin
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from auth.serializers import RegisterSerializer

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

def get_auth_token(user):
    payload = jwt_payload_handler(user)

    return jwt_encode_handler(payload)


class AuthView(AllowAnyMixin, APIView):
    def post(self, request):
        serializer = RegisterSerializer(data=request.data, context={'request': request})

        if serializer.is_valid():
            user = serializer.save()
            token = get_auth_token(user)

            return Response({'user': serializer.data, 'token': token}, status=status.HTTP_201_CREATED)

        return Response({'errors': serializer.errors, 'type': 'register'}, status=status.HTTP_403_FORBIDDEN)
