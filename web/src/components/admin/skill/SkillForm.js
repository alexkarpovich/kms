import $ from 'jquery';
import 'eonasdan-bootstrap-datetimepicker';
import React, {Component, PropType} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import CommonComponent from 'components/common/CommonComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import SkillModel from 'models/Skill';

class SkillForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {skill, params} = props;

        this.state = {
            skill: !!params.skillId ? {...skill.item, category_id: skill.item.category.id} : {...SkillModel}
        };

        this.handleChange = ::this.handleChange;
        this.createSkill = ::this.createSkill;
        this.updateSkill = ::this.updateSkill;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        /* TODO: Load all skillType pages */
        actions.loadSkillCategoryPage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        if (params.skillId) {
            actions.loadSkill(params.skillId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {params, skill} = newProps;

        if (this.state.skill !== skill.item && !!params.skillId) {
            this.state.skill = {...skill.item, category_id: skill.item.category.id};
        }
    }

    updateSkill() {
        let {actions, routeActions} = this.props;

        actions.updateSkill(this.state.skill).then(() => {
            routeActions.push('/admin/skill');
        });
    }

    createSkill() {
        let {actions, routeActions} = this.props;

        actions.createSkill(this.state.skill).then(() => {
            routeActions.push('/admin/skill');
        });
    }

    handleChange(e) {
        let {id, type, value, checked} = e.target;

        this.state.skill[id] = type == 'checkbox' ? checked : value;
        this.setState(this.state);
    }

    render() {
        let {intl: {formatMessage}, params, skillCategory} = this.props;
        let isEdit = !!params.skillId;
        let {skill} = this.state;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="skill.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title" />
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="name"
                                                onChange={this.handleChange}
                                                value={skill.name}
                                                placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="description">
                                                <FormattedMessage id="common.description.title" />
                                            </label>
                                                <textarea
                                                    className="form-control"
                                                    id="description"
                                                    onChange={this.handleChange}
                                                    value={skill.description}
                                                    placeholder={formatMessage({id: 'common.description.title'})} >
                                                </textarea>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="category">
                                                <FormattedMessage id="common.category.title" />
                                            </label>
                                            <select
                                                id="category_id"
                                                className="form-control"
                                                value={skill.category_id}
                                                onChange={this.handleChange}>
                                                <option>{formatMessage({id: 'common.action.select.title'})}</option>
                                                { skillCategory.page.results.map(item => {
                                                    return <option key={item.id} value={item.id}>{item.name}</option>;
                                                }) }
                                            </select>
                                        </div>

                                        <div className="checkbox">
                                            <label>
                                                <input
                                                    type="checkbox"
                                                    id="enabled"
                                                    onChange={this.handleChange}
                                                    checked={skill.enabled}
                                                    placeholder={formatMessage({id: 'common.enabled.title'})}
                                                />
                                                <FormattedMessage id="common.enabled.title" />
                                            </label>
                                        </div>

                                        <button type="button" className={submitClasses} onClick={isEdit ? this.updateSkill : this.createSkill}>
                                            <FormattedMessage id="grid.action.submit.title.up" />
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(SkillForm);
