Knowledge Management System (KMS)
========================
Requirements
------------
On windows or Linux:

- VirtualBox
- Vagrant
- Ansible
- NodeJS
- npm
- bower

Add to hosts file follows:
127.0.0.1   local.kms.com

Install
----------
- git clone https://<username>@bitbucket.org/alexkarpovich/kms.git
- cd kms
- vagrant up --provision
- cd web
- npm install
- sudo bower install --allow-root

Start Web
----------
- npm start

Params to connect to PostgreSQL outside (PyCharm):
----------
1. Host name/IP address: "127.0.0.1"
2. Port: "5432"
3. Database: "kms"
4. User: "kms"
5. Password: "kms"
6. SSH - Host name/IP address: "192.168.33.10"
7. SSH - Port: "22"
8. SSH - User: "vagrant"
9. SSH - Password: "vagrant"

Activate virtualenv and django manage tasks:
----------
- vagrant ssh
- source ~/.virtualenv/bin/activate && cd /app/api/kms
- python manage.py runserver 0.0.0.0:8000
- open the browser: localhost:8000
- open new bash tab
- vagrant ssh
- source ~/.virtualenv/bin/activate && cd /app/async
- python app.py
- open the browser: localhost:8080

Access Django admin:
----------
local.kms.com:8000/admin
Username: "kms"
Password: "kms"

Access Web:
----------
local.kms.com:3003/