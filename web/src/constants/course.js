import keymirror from 'keymirror';

export const CourseConst = keymirror({
    LOAD_COURSE_REQUEST: null,
    LOAD_COURSE_SUCCESS: null,
    LOAD_COURSE_FAILURE: null,

    LOAD_COURSE_PAGE_REQUEST: null,
    LOAD_COURSE_PAGE_SUCCESS: null,
    LOAD_COURSE_PAGE_FAILURE: null,

    CREATE_COURSE_REQUEST: null,
    CREATE_COURSE_SUCCESS: null,
    CREATE_COURSE_FAILURE: null,

    UPDATE_COURSE_REQUEST: null,
    UPDATE_COURSE_SUCCESS: null,
    UPDATE_COURSE_FAILURE: null,

    DELETE_COURSE_REQUEST: null,
    DELETE_COURSE_SUCCESS: null,
    DELETE_COURSE_FAILURE: null,
});
