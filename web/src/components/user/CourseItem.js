import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';

export default class CourseItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {course, order} = this.props;

        return (
            <li>
                <Link to={`/course/${course.id}`}>
                    {`${order+1}. ${course.name}`}
                </Link>
            </li>
        );
    }
}
