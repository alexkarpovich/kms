import {createStore, compose, applyMiddleware} from 'redux';
import {reduxReactRouter, browserHistory} from 'react-router';
import {syncHistory} from 'react-router-redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import reducer from '../reducers';

const logger = createLogger();
const reduxRouter = syncHistory(browserHistory);

const finalCreateStore = compose(
    applyMiddleware(thunk, logger, reduxRouter)
)(createStore);

export default function configureStore(initialState) {
    return finalCreateStore(reducer, initialState);
}
