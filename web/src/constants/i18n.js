'use strict';

import keymirror from 'keymirror';

export var I18nConst = keymirror({
    LOCALE_CHANGED: null,
    CHANGE_LOCALE: null
});
