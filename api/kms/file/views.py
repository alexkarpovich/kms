import uuid
import os
from PIL import Image
from django.conf import settings
from rest_framework.parsers import FormParser, MultiPartParser, JSONParser
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from common.mixins import AllowAnyModelViewSetMixin
from .models import File
from .serializers import FileSerializer


def unique_filename(extension):
    return '{}{}'.format(uuid.uuid4(), extension)


def make_crop_set(filename, coords=None):
    file_path = os.path.join(settings.IMAGE_ROOT, filename)
    origin = Image.open(file_path)

    if coords:
        left = int(coords.get('x'))
        top = int(coords.get('y'))
        right = int(coords.get('x2'))
        bottom = int(coords.get('y2'))

        cropped = origin.crop((left, top, right, bottom))
    else:
        size = min(origin.size)
        cropped = origin.crop((0, 0, size, size))

    for size in settings.IMAGE_SIZE_SET:
        tmp = cropped.copy()
        dist_dir = os.path.join(settings.IMAGE_ROOT, 'x'.join(map(str, size)))
        dist_file_path = os.path.join(dist_dir, filename)

        if not os.path.exists(dist_dir):
            os.makedirs(dist_dir)

        tmp.resize(size, Image.ANTIALIAS).save(dist_file_path)


class FileViewSet(AllowAnyModelViewSetMixin):
    queryset = File.objects.all()
    serializer_class = FileSerializer
    parser_classes = (MultiPartParser, FormParser, JSONParser, )

    @detail_route(methods=['post'])
    def crop(self, request, pk=None):
        coords = request.data.get('coords')
        image = File.objects.get(pk=pk)
        filename = os.path.basename(image.path.path)
        make_crop_set(filename, coords)

        return Response({})

    def perform_create(self, serializer):
        f = self.request.FILES['file']
        upload_as = self.request.data.get('uploadAs')
        filename = f.name
        _, extension = os.path.splitext(filename)
        new_filename = unique_filename(extension)

        if upload_as == 'file':
            dist_dir = 'files/'
            dist_path = settings.FILE_ROOT
        else:
            dist_dir = 'images/'
            dist_path = settings.IMAGE_ROOT

        file_path = os.path.join(dist_path, new_filename)

        with open(file_path, 'wb+') as dist:
            for chunk in f.chunks():
                dist.write(chunk)

        serializer.save(path=os.path.join(dist_dir, new_filename),
                        filename=filename,
                        content_type=f.content_type)

        if upload_as == 'image':
            make_crop_set(new_filename)
