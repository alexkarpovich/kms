import keymirror from 'keymirror';

export const CommentConst = keymirror({
    LOAD_COMMENT_LIST_REQUEST: null,
    LOAD_COMMENT_LIST_SUCCESS: null,
    LOAD_COMMENT_LIST_FAILURE: null,

    POST_COMMENT_REQUEST: null,
    POST_COMMENT_SUCCESS: null,
    POST_COMMENT_FAILURE: null
});
