import './UserList.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import UserItem from './UserItem';

class UserList extends Component {
    constructor(props) {
        super(props);

        this.loadUsers = ::this.loadUsers;
        this.searchUsers = ::this.searchUsers;
        this.handleKeyPress = ::this.handleKeyPress;
    }

    componentDidMount() {
        this.loadUsers();
    }

    loadUsers(searchValue) {
        let {actions} = this.props;

        actions.loadUserPage({
            page: 1,
            size: 1000,
            ordering: 'id',
            search: searchValue ? searchValue : ''
        });
    }

    searchUsers() {
        let searchValue = this.refs.search.value;
        this.loadUsers(searchValue);
    }

    handleKeyPress(e) {
        if (e.charCode == 13) {
            this.searchUsers();
        }
    }

    render() {
        let {user: {page: userPage}, actions, routeActions} = this.props;

        return (
            <div className="user-list">
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="user.title.many" />
                    </h1>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="actions">
                                <div className="search">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            ref="search"
                                            className="form-control input-sm"
                                            onKeyPress={this.handleKeyPress}
                                            placeholder="Search..."
                                        />
                                        <span className="input-group-btn">
                                            <button
                                                className="btn btn-sm btn-default"
                                                type="button"
                                                onClick={this.searchUsers}
                                            >
                                                Search
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                { userPage.results.map(user => {
                                    return <UserItem
                                        key={user.id}
                                        user={user}
                                        deleteUser={actions.deleteUser}
                                        loadUsers={this.loadUsers}
                                        routeActions={routeActions}
                                    />
                                }) }
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(UserList);
