from django.contrib import admin

from .models import Lesson, LessonType


class LessonAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', )
    list_filter = ('name', 'type', )
    search_fields = ('name', 'content', )
    fieldsets = (
        (None, {'fields': ('name', 'type', )}),
        (None, {'fields': ('content', )}),
    )


admin.site.register(LessonType)
admin.site.register(Lesson, LessonAdmin)
