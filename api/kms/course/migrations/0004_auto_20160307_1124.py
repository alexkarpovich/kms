# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import markdownx.models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0003_course_author'),
    ]

    operations = [
        migrations.AlterField(
            model_name='course',
            name='description',
            field=markdownx.models.MarkdownxField(blank=True, default=''),
        ),
    ]
