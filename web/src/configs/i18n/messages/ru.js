import flattenMessages from 'utils/flatten-messages'

export const ru = flattenMessages({
    navigation: {
        title: 'Навигация',
        admin: 'Админ',
        home: 'Главная',
        user: 'Пользователь',
        skill: 'Навык',
        skillCategory: 'Категория навыка',
        group: 'Группа',
        permission: 'Права доступа',
        lessonType: 'Тип урока',
        eventType: 'Тип события',
        lesson: 'Урок',
        course: 'Курс',
        event: 'Событие',
        profile: 'Профиль',
        calendar: 'Календарь'
    },
    breadcrumb: {
        update: 'Обновить',
        create: 'Создать',
        signin: 'Войти',
        signup: 'Зарегестрироваться',
        profile: 'Профиль пользователя'
    },
    common: {
        id: {
            title: 'ID'
        },
        name: {
            title: 'Название'
        },
        description: {
            title: 'Описание'
        },
        type: {
            title: 'Тип'
        },
        content: {
            title: 'Контент'
        },
        complexity: {
            title: 'Сложность'
        },
        enabled: {
            title: 'Включен'
        },
        category: {
            title: "Категория"
        },
        parent: {
            title: 'Родитель'
        },
        level: {
            title: 'Уровень'
        },
        action: {
            select: {
                title: 'Выбрать'
            },
            clear: {
                title: 'очистить'
            },
            cancel: {
                title: 'Отменить'
            },
            save: {
                title: 'Сохранить'
            },
            add: {
                title: 'добавить'
            }
        },
        confirmation: {
            ok: {
                title: 'OK'
            }
        },
        total: {
            title: 'Всего'
        },
        to: {
            title: 'до'
        },
        comment: {
            title: {
                many: 'Комментарии',
                one: 'Комментарий'
            },
            reply: 'ответить',
            edit: 'редактировать',
            delete: 'удалить',
            form: {
                placeholder: 'Оставьте свой комментарий'
            }
        }
    },
    admin: {
        title: {
            one: 'Администратор'
        }
    },
    user: {
        title: {
            many: 'Пользователи',
            one: 'Пользователь'
        },
        profile: {
            title: 'Профиль'
        },
        action: {
            login: 'Войти'
        },
        id: {
            title: 'ID'
        },
        username: {
            title: 'Ник'
        },
        email: {
            title: 'Почта'
        },
        password: {
            title: 'Пароль'
        },
        confirmPassword: {
            title: 'Подтвердите пароль'
        },
        firstName: {
            title: 'Имя'
        },
        lastName: {
            title: 'Фамилия'
        },
        phone: {
            title: 'Телефон'
        },
        avatar: {
            title: 'Аватар'
        },
        skype: {
            title: 'Скайп'
        },
        isActive: {
            title: 'Активен'
        },
        isStaff: {
            title: 'Персонал'
        },
        dataJoined: {
            title: 'Дата регистации'
        }
    },
    skill: {
        title: {
            many: 'Навыки'
        }
    },
    skillCategory: {
        title: {
            many: 'Категории навыков'
        }
    },
    group: {
        title: {
            many: 'Группы'
        }
    },
    permission: {
        title: {
            many: 'Права'
        },
        codename: {
            title: 'Кодовое название'
        },
        contentType: {
            title: 'Тип контента'
        }
    },
    lessontype: {
        title: {
            many: 'Типы уроков'
        }
    },
    eventtype: {
        title: {
            many: 'Типы событий'
        }
    },
    lesson: {
        title: {
            many: 'Уроки'
        },
        empty: 'Список созданных уроков пуст'
    },
    course: {
        title: {
            many: 'Курсы'
        },
        empty: 'Список созданных курсов пуст'
    },
    event: {
        title: {
            many: 'События'
        },
        startDate: {
            title: 'Дата начала'
        },
        endDate: {
            title: 'Дата конца'
        },
        startTime: {
            title: 'Время начала'
        },
        endTime: {
            title: 'Время конца'
        },
        dateRange: {
            title: 'Даты проведения'
        },
        timeRange: {
            title: 'Время проведения'
        }
    },
    file: {
        action: {
            upload: 'Загрузить файл',
            choose: 'Выбрать файл'
        },
        uploader: {
            title: 'Загрузчик файлов',
            info: 'Выберите один файл для загрузки и последующей обработки'
        }
    },
    grid: {
        action: {
            create: {
                title: {
                    low: 'создать',
                    up: 'Создать'
                }
            },
            update: {
                title: {
                    low: 'обновить',
                    up: 'Обновить'
                }
            },
            submit: {
                title: {
                    low: 'подтверить',
                    up: 'Подтвердить'
                }
            },
            title: {
                many: 'Действия'
            }
        }
    },
    http: {
        status: {
            400: 'Неправильный запрос',
            401: 'Вы не вошли в систему',
            403: 'Нет прав на получение данного ресурса',
            404: 'Ресурс не найден',
            405: 'Метод запрещен'
        }
    },
    auth: {
        signin: {
            btn: 'Войти',
            form: {
                label: 'Войти',
                username: 'Имя пользователя',
                password: 'Пароль'
            }
        },
        signup: {
            btn: 'Зарегестрироваться',
            form: {
                label: 'Зарегестрироваться',
                username: 'Имя пользователя',
                password: 'Пароль',
                firstName: 'Имя',
                lastName: 'Фамилия',
                emailAddress: 'Почта'
            }
        },
        signout: {
            btn: 'Выйти'
        }
    },
    profile: {
        breadcrumb: 'Профиль пользователя',
        settings: 'Настройки',
        btn: {
            save: 'Сохранить',
            upload: 'Загрузить новую фотографию'
        },
        changesSaved: {
            title: 'Изменения сохранены.',
            message: 'Новые данные будут отражены на Вашей странице..'
        }
    },
    confirmation: {
        delete: 'Вы уверены, что хотете удалить этот элемент?'
    }
});
