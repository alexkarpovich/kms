import './LocaleSwitcher.less'
import React, {Component, PropTypes} from 'react'
import classnames from 'classnames'
import LocaleSwitcherItem from './LocaleSwitcherItem'

export default class LocaleSwitcher extends Component {
    static propTypes = {
        i18n: PropTypes.object.isRequired,
        changeLocale: PropTypes.func.isRequired
    };

    render() {
        let {i18n, changeLocale} = this.props;

        return (
            <div className="locale-switcher">
                {i18n.availableLocales.map(key =>
                    <LocaleSwitcherItem
                        key={key}
                        locale={key}
                        currentLocale={i18n.locale}
                        changeLocale={changeLocale}
                    />)}
            </div>
        );
    }
}
