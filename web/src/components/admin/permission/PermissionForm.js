import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import CommonComponent from 'components/common/CommonComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import PermissionModel from 'models/Permission';

class PermissionForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {permission, params} = props;

        this.state = {
            permission: !!params.permissionId ? permission.item : {...PermissionModel}
        };

        this.handleChange = ::this.handleChange;
        this.createPermission = ::this.createPermission;
        this.updatePermission = ::this.updatePermission;
    }

    static propTypes = {
        params: PropTypes.object.isRequired,
        actions: PropTypes.object.isRequired
    };

    componentDidMount() {
        let {params, actions} = this.props;

        if (params.permissionId) {
            actions.loadPermission(params.permissionId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {params, permission} = newProps;

        if (this.state.permission !== permission.item && !!params.permissionId) {
            this.state.permission = permission.item;
        }
    }

    updatePermission() {
        let {actions, routeActions} = this.props;

        actions.updatePermission(this.state.permission);
        routeActions.push('/admin/permission');
    }

    createPermission() {
        let {actions, routeActions} = this.props;

        actions.createPermission(this.state.permission);
        routeActions.push('/admin/permission');
    }

    handleChange(e) {
        this.state.permission[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    render() {
        let isEdit = !!this.props.params.permissionId;
        let {permission} = this.state;
        let {formatMessage} = this.props.intl;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="permission.title.many"/>
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title"/>
                                            </label>
                                            <input type="text"
                                                   className="form-control input-sm"
                                                   id="name"
                                                   onChange={this.handleChange}
                                                   value={permission.name}
                                                   placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="codename">
                                                <FormattedMessage id="permission.codename.title"/>
                                            </label>
                                            <input type="text"
                                                   className="form-control input-sm"
                                                   id="codename"
                                                   onChange={this.handleChange}
                                                   value={permission.codename}
                                                   placeholder={formatMessage({id: 'permission.codename.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="contentType">
                                                <FormattedMessage id="permission.contentType.title"/>
                                            </label>
                                            <input type="text"
                                                   className="form-control input-sm"
                                                   id="contentType"
                                                   onChange={this.handleChange}
                                                   value={permission.contentType}
                                                   placeholder={formatMessage({id: 'permission.contentType.title'})}
                                            />
                                        </div>

                                        <button type="button"
                                                className={submitClasses}
                                                onClick={isEdit ? this.updatePermission : this.createPermission}>
                                            <FormattedMessage id="grid.action.submit.title.up"/>
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(PermissionForm);
