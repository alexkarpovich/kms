from django.db import models


class SkillCategory(models.Model):
    id = models.AutoField(primary_key=True)
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=255, blank=False, default='')
    description = models.CharField(max_length=1000, blank=True, default='')
    enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Skill(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(SkillCategory, blank=False, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=255, blank=False, default='')
    description = models.CharField(max_length=1000, blank=True, default='')
    enabled = models.BooleanField(default=True)

    def __unicode__(self):
        return '{} (category = {})'.format(self.name, self.category)

    def __str__(self):
        return '{} (category = {})'.format(self.name, self.category)
