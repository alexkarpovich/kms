import {SkillConst} from 'constants';
import api from 'helpers/api';

export function loadSkillPage(params) {
    return async dispatch => {
        dispatch({type: SkillConst.LOAD_SKILL_PAGE_REQUEST});

        try {
            let data = await api({url: 'skill/', query: params});

            dispatch({type: SkillConst.LOAD_SKILL_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: SkillConst.LOAD_SKILL_PAGE_FAILURE, error});
        }
    }
}

export function loadSkill(skillId) {
    return async dispatch => {
        dispatch({type: SkillConst.LOAD_SKILL_REQUEST});

        try {
            let data = await api({url: `skill/${skillId}/`});

            dispatch({type: SkillConst.LOAD_SKILL_SUCCESS, data});
        } catch (error) {
            dispatch({type: SkillConst.LOAD_SKILL_FAILURE, error});
        }
    };
}

export function createSkill(skill) {
    return async dispatch => {
        dispatch({type: SkillConst.CREATE_SKILL_REQUEST});

        try {
            await api({url: `skill/`, method: 'post', data: skill});

            dispatch({type: SkillConst.CREATE_SKILL_SUCCESS});
        } catch (error) {
            dispatch({type: SkillConst.CREATE_SKILL_FAILURE, error});
        }
    };
}

export function updateSkill(skill) {
    return async dispatch => {
        dispatch({type: SkillConst.UPDATE_SKILL_REQUEST});

        try {
            await api({url: `skill/${skill.id}/`, method: 'put', data: skill});

            dispatch({type: SkillConst.UPDATE_SKILL_SUCCESS});
        } catch (error) {
            dispatch({type: SkillConst.UPDATE_SKILL_FAILURE, error});
        }
    };
}

export function deleteSkill(skillId) {
    return async dispatch => {
        dispatch({type: SkillConst.DELETE_SKILL_REQUEST});

        try {
            await api({url: `skill/${skillId}/`, method: 'delete'});

            dispatch({type: SkillConst.DELETE_SKILL_SUCCESS});
        } catch (error) {
            dispatch({type: SkillConst.DELETE_SKILL_FAILURE, error});
        }
    };
}
