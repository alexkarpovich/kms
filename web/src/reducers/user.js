import {combineReducers} from 'redux';
import {UserConst} from 'constants';
import UserModel from 'models/User';
import PageModel from 'models/Page';


function item(state = UserModel, action) {
    switch(action.type) {
        case UserConst.LOAD_USER_REQUEST:
            return {...state, isFetching: false, error: null};

        case UserConst.LOAD_USER_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case UserConst.LOAD_USER_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case UserConst.LOAD_USER_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case UserConst.LOAD_USER_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case UserConst.LOAD_USER_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function lessons(state = {data: []}, action) {
    switch(action.type) {
        case UserConst.LOAD_USER_LESSONS_REQUEST:
            return {...state, isFetching: true, error: null};

        case UserConst.LOAD_USER_LESSONS_SUCCESS:
            return {...state, data: action.data, isFetching: false, error: null};

        case UserConst.LOAD_USER_LESSONS_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function courses(state = {data: []}, action) {
    switch(action.type) {
        case UserConst.LOAD_USER_COURSES_REQUEST:
            return {...state, isFetching: true, error: null};

        case UserConst.LOAD_USER_COURSES_SUCCESS:
            return {...state, data: action.data, isFetching: false, error: null};

        case UserConst.LOAD_USER_COURSES_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function skills(state = {data: []}, action) {
    switch(action.type) {
        case UserConst.LOAD_USER_SKILLS_REQUEST:
            return {...state, isFetching: true, error: null};

        case UserConst.LOAD_USER_SKILLS_SUCCESS:
            return {...state, data: action.data, isFetching: false, error: null};

        case UserConst.LOAD_USER_SKILLS_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page, lessons, courses, skills
});
