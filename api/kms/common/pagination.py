from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination


class CustomPagination(PageNumberPagination):
    def get_paginated_response(self, data):
        return Response({
            'has_next': bool(self.get_next_link()),
            'has_prev': bool(self.get_previous_link()),
            'count': self.page.paginator.count,
            'results': data
        })
