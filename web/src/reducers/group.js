import {combineReducers} from 'redux';
import {GroupConst} from 'constants';
import GroupModel from 'models/Group';
import PageModel from 'models/Page';


function item(state = GroupModel, action) {
    switch(action.type) {
        case GroupConst.LOAD_GROUP_REQUEST:
            return {...state, isFetching: false, error: null};

        case GroupConst.LOAD_GROUP_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case GroupConst.LOAD_GROUP_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case GroupConst.LOAD_GROUP_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case GroupConst.LOAD_GROUP_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case GroupConst.LOAD_GROUP_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
