import './CourseItem.less';
import React, {Component, PropTypes} from 'react';
import {injectIntl} from 'react-intl';

class CourseItem extends Component {
    constructor(props) {
        super(props);

        this.viewCourse = ::this.viewCourse;
        this.updateCourse = ::this.updateCourse;
        this.removeCourse = ::this.removeCourse;
    }

    static propTypes = {
        course: PropTypes.object.isRequired,
        deleteCourse: PropTypes.func.isRequired,
        loadCourses: PropTypes.func.isRequired,
        routeActions: PropTypes.object.isRequired,
        authUser: PropTypes.object.isRequired
    };

    updateCourse() {
        let {routeActions, course} = this.props;

        routeActions.push(`/course/${course.id}/edit`);
    }

    removeCourse() {
        let {course, deleteCourse, loadCourses, intl: {formatMessage}} = this.props;

        alertify.confirm(formatMessage({id: 'confirmation.delete'}), isConfirmed => {
            isConfirmed && deleteCourse(course.id).then(() => loadCourses());
        });

    }

    viewCourse() {
        let {routeActions, course} = this.props;

        routeActions.push(`/course/${course.id}`);
    }

    render() {
        let {course: {name, description, skills, lessons, author}, authUser} = this.props;

        return (
            <div className="course-item info-box">
                {authUser && author.id === authUser.id ?
                <div className="actions box-tools pull-right">
                    <button className="btn btn-xs btn-default" onClick={this.updateCourse}>
                        <i className="fa fa-edit"/>
                    </button>
                    <button className="btn btn-xs btn-default" onClick={this.removeCourse}>
                        <i className="fa fa-close"/>
                    </button>
                </div>
                    : null }
                <span className="icon info-box-icon bg-yellow" onClick={this.viewCourse}>
                    <i className="fa fa-graduation-cap"/>
                </span>
                <div className="info-box-content">
                    <div className="title">
                        {`${name}`}
                    </div>
                    <div className="skills">
                    {skills.map(skill => <span key={skill.id} className="label label-success">{skill.name}</span>)}
                    </div>
                    <div className="description">
                        <p>{description}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default injectIntl(CourseItem);
