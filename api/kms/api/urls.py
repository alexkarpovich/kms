from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
from file.views import FileViewSet
from kmsuser.views import UserViewSet, GroupViewSet, PermissionViewSet
from lesson.views import LessonViewSet, LessonTypeViewSet
from event.views import EventViewSet, EventTypeViewSet
from course.views import CourseViewSet
from skill.views import SkillViewSet, SkillCategoryViewSet
from common.rating.views import RatingViewSet
from common.comment.views import CommentViewSet
from auth.views import AuthView


router = DefaultRouter()
router.register(r'file', FileViewSet)
router.register(r'user', UserViewSet)
router.register(r'group', GroupViewSet)
router.register(r'permission', PermissionViewSet)
router.register(r'lesson', LessonViewSet)
router.register(r'lesson-type', LessonTypeViewSet)
router.register(r'event', EventViewSet)
router.register(r'event-type', EventTypeViewSet)
router.register(r'course', CourseViewSet)
router.register(r'skill-category', SkillCategoryViewSet)
router.register(r'skill', SkillViewSet)
router.register(r'rating', RatingViewSet)
router.register(r'comment', CommentViewSet)

auth_urls = [
    url(r'signin/$', obtain_jwt_token),
    url(r'token-refresh/$', 'rest_framework_jwt.views.refresh_jwt_token'),
]

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^auth/', include(auth_urls)),
    url(r'^register/', AuthView.as_view())
]
