'use strict';

const DefaultConfig = {
    gridControls: {
        paginateBy: true,
        createNew: true,
        searchField: true,
        pagination: true
    },
    paginateBy: {
        default: 10,
        items: [10, 15, 30, 50, 75, 100]
    }
};

export default class GridConfig {
    constructor(params = {}) {
        this.gridControls = {...DefaultConfig.gridControls, ...params.gridControls};
        this.paginateBy = {...DefaultConfig.paginateBy, ...params.paginateBy};
    }
}
