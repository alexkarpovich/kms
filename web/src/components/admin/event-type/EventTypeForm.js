import React, {Component, PropTypes} from 'react';
import classnames from 'classnames';
import {FormattedMessage, injectIntl} from 'react-intl';
import CommonComponent from 'components/common/CommonComponent';
import {Breadcrumb} from 'components/common/breadcrumb/Breadcrumb';
import EventTypeModel from 'models/EventType';


class EventTypeForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {params, eventType} = props;

        this.state = {
            eventType: !!params.eventTypeId ? eventType.item : {...EventTypeModel}
        };

        this.handleChange = ::this.handleChange;
        this.createEventType = ::this.createEventType;
        this.updateEventType = ::this.updateEventType;
    }

    static propTypes = {
        params: PropTypes.object.isRequired,
        actions: PropTypes.object.isRequired
    };

    componentDidMount() {
        let {params, actions} = this.props;

        if (params.eventTypeId) {
            actions.loadEventType(params.eventTypeId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {params, eventType} = newProps;

        console.log(this.state.eventType);

        if (this.state.eventType !== eventType.item && !!params.eventTypeId) {
            this.state.eventType = eventType.item;
        }
    }

    updateEventType() {
        let {actions, routeActions} = this.props;

        actions.updateEventType(this.state.eventType).then(() => {
            routeActions.push('/admin/event-type');
        });
    }

    createEventType() {
        let {actions, routeActions} = this.props;

        actions.createEventType(this.state.eventType).then(() => {
            routeActions.push('/admin/event-type');
        });
    }

    handleChange(e) {
        this.state.eventType[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    render() {
        let isEdit = !!this.props.params.eventTypeId;
        let {eventType} = this.state;
        let {formatMessage} = this.props.intl;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="eventtype.title.many" />
                    </h1>
                    { /*<Breadcrumb />*/ }
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title" />
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="name"
                                                onChange={this.handleChange}
                                                value={eventType.name}
                                                placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="description">
                                                <FormattedMessage id="common.description.title" />
                                            </label>
                                            <textarea
                                                className="form-control input-sm"
                                                id="description"
                                                onChange={this.handleChange}
                                                value={eventType.description}
                                                placeholder={formatMessage({id: 'common.description.title'})} >
                                            </textarea>
                                        </div>

                                        <button type="button" className={submitClasses} onClick={isEdit ? this.updateEventType : this.createEventType}>
                                            <FormattedMessage id="grid.action.submit.title.up" />
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(EventTypeForm);
