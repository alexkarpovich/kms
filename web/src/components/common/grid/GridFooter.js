'use strict';

import React, {Component} from 'react';
import classnames from 'classnames';
import {FormattedMessage, formatMessage} from 'react-intl'

export default class GridFooter extends Component {
    constructor(props, context) {
        super(props, context);

        this.toFirstPage = ::this.toFirstPage;
        this.toLastPage = ::this.toLastPage;
        this.toNextPage = ::this.toNextPage;
        this.toPrevPage = ::this.toPrevPage;
        this.toPage = ::this.toPage;
    }

    static contextTypes = {
        setPageNumber: React.PropTypes.func
    };

    componentWillReceiveProps() {
        this.forceUpdate();
    }

    toNextPage() {
        this.toPage(this.props.pageNumber + 1);
    }

    toLastPage() {
        let {pageSize, count} = this.props;
        let lastPageNumber = Math.ceil(count / pageSize);

        this.toPage(lastPageNumber);
    }

    toPrevPage() {
        this.toPage(this.props.pageNumber - 1);
    }

    toFirstPage() {
        this.toPage(1);
    }

    toPage(newPageNumber) {
        let {pageSize, count, pageNumber} = this.props;
        let lastPageNumber = Math.ceil(count / pageSize);

        if (newPageNumber > 0 &&
            newPageNumber <= lastPageNumber &&
            newPageNumber !== pageNumber) {
            this.context.setPageNumber(newPageNumber);
        }
    }

    render() {
        let {pageNumber, pageSize, count, hasNext, hasPrev} = this.props;
        let prevClasses = classnames({
            'disabled': !hasPrev
        });
        let nextClasses = classnames({
            'disabled': !hasNext
        });

        return (
            <div className="box-footer clearfix">
                <div className="pull-left">
                    <span className="total">
                        <FormattedMessage id="common.total.title" />{': ' + count}</span>
                </div>
                <ul className="pagination pagination-sm no-margin pull-right">
                    <li className={prevClasses}>
                        <a href="javascript:void(0)" onClick={this.toFirstPage}>
                            <i className="fa fa-fast-backward"></i>
                        </a>
                    </li>
                    <li className={prevClasses}>
                        <a href="javascript:void(0)" onClick={this.toPrevPage}>
                            <i className="fa fa-backward"></i>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            {pageNumber }
                        </a>
                    </li>
                    <li className={nextClasses}>
                        <a href="javascript:void(0)" onClick={this.toNextPage}>
                            <i className="fa fa-forward"></i>
                        </a>
                    </li>
                    <li className={nextClasses}>
                        <a href="javascript:void(0)" onClick={this.toLastPage}>
                            <i className="fa fa-fast-forward"></i>
                        </a>
                    </li>
                </ul>
            </div>
        );
    }
}
