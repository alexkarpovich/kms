const SkillCategory = {
    id: null,
    parent: {},
    name: '',
    description: '',
    enabled: 'true'
};

export default SkillCategory;
