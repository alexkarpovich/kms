'use strict';

import React, {Component} from 'react';
import HeaderItem from './body/HeaderItem';
import BodyItem from './body/BodyItem';

export default class GridBody extends Component {
    static contextTypes = {
        columns: React.PropTypes.array.isRequired
    };

    render() {
        let {columns} = this.context;
        let {entities, ordering} = this.props;
        let actionsColumn = {
            key: 'actions',
            name: 'grid.action.title.many'
        };

        let headerItems = columns.map(column => {
            return <HeaderItem key={column.key} ordering={ordering} column={column}/>
        });

        headerItems.push(<HeaderItem key={actionsColumn.key} ordering={ordering} column={actionsColumn}/>);

        return (
            <div className="box-body table-responsive no-padding">
                <table className="table table-striped">
                    <thead>
                        <tr className="no-wrapping">
                            {headerItems}
                        </tr>
                    </thead>

                    <tbody>
                    { entities && entities.map(entity => <BodyItem key={entity.id} entity={entity}/>) }
                    </tbody>
                </table>
            </div>
        );
    }
}
