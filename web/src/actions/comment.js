import {CommentConst} from 'constants';
import api from 'helpers/api';

function prepareComment(data) {
    let comments = [];
    let nestedComments = [];
    let commentIdsHaveChildren = [];

    /* filter comments parentId=null */
    data.forEach(comment => {
        if (!comment.parentId) {
            comments.push(comment)
        } else {
            nestedComments.push(comment);
            commentIdsHaveChildren.push(comment.parentId);
        }
    });

    /* insert into children array nested comments */
    return comments.map(comment => {
        if (commentIdsHaveChildren.indexOf(comment.id) !== -1) {
            comment.children = [];

            nestedComments.forEach(nestedComment => {
                if (+nestedComment.parentId === +comment.id) {
                    comment.children.push(nestedComment);
                }
            });
        }

        return comment;
    });
}

export function loadCommentList(entityName, entityId) {
    return async dispatch => {
        dispatch({type: CommentConst.LOAD_COMMENT_LIST_REQUEST});

        try {
            let data = await api({url: `${entityName}/${entityId}/comments/`});

            data = prepareComment(data);
            console.log(data);

            dispatch({type: CommentConst.LOAD_COMMENT_LIST_SUCCESS, data});
        } catch (error) {
            dispatch({type: CommentConst.LOAD_COMMENT_LIST_FAILURE, error});
        }
    };
}

export function postComment(entityName, entityId, comment) {
    return async dispatch => {
        dispatch({type: CommentConst.POST_COMMENT_REQUEST});

        try {
            await api({url: `${entityName}/${entityId}/comments/`, method: 'post', data: comment});

            dispatch({type: CommentConst.POST_COMMENT_SUCCESS});
        } catch (error) {
            dispatch({type: CommentConst.POST_COMMENT_FAILURE, error});
        }
    };
}
