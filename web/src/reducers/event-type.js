import {combineReducers} from 'redux';
import {EventTypeConst} from 'constants';
import PageModel from 'models/Page';
import EventTypeModel from 'models/EventType';


function item(state = EventTypeModel, action) {
     switch(action.type) {
         case EventTypeConst.LOAD_EVENT_TYPE_REQUEST:
             return {...state, isFetching: false, error: null};

         case EventTypeConst.LOAD_EVENT_TYPE_SUCCESS:
             return {...state, ...action.data, isFetching: false};

         case EventTypeConst.LOAD_EVENT_TYPE_FAILURE:
             return {...state, error: action.error, isFetching: false};

         default:
             return state;
     }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case EventTypeConst.LOAD_EVENT_TYPE_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case EventTypeConst.LOAD_EVENT_TYPE_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case EventTypeConst.LOAD_EVENT_TYPE_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
