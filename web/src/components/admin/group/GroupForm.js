import React, {Component, PropType} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import CommonComponent from 'components/common/CommonComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import Selector from 'components/common/selector/Selector';
import GroupModel from 'models/Group';

class GroupForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {group, permission, params} = props;

        this.state = {
            group: !!params.groupId ? group.item : {...GroupModel},
            permissionPage: permission.page
        };

        this.handleChange = ::this.handleChange;
        this.createGroup = ::this.createGroup;
        this.updateGroup = ::this.updateGroup;
        this.permissionChange = ::this.permissionChange;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        actions.loadPermissionPage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        if (params.groupId) {
            actions.loadGroup(params.groupId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {params, group, permission} = newProps;

        if (this.state.group !== group.item && !!params.groupId) {
            this.state.group = group.item;
        }

        if (this.state.permissionPage !== permission.page) {
            this.state.permissionPage = permission.page;
        }
    }

    updateGroup() {
        let {actions, routeActions} = this.props;

        actions.updateGroup(this.state.group).then(() => {
            routeActions.push('/admin/group');
        });
    }

    createGroup() {
        let {actions, routeActions} = this.props;

        actions.createGroup(this.state.group).then(() => {
            routeActions.push('/admin/group');
        });
    }

    handleChange(e) {
        this.state.group[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    permissionChange(permissionIds) {
        this.state.group.permissions = permissionIds;
        this.setState(this.state);
    }

    render() {
        let {params, intl: {formatMessage}} = this.props;
        let isEdit = !!params.groupId;
        let {group, permissionPage} = this.state;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        let permissions = permissionPage.results.map(item => ({
            value: item.id,
            label: item.name
        }));

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="group.title.many"/>
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title"/>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="name"
                                                onChange={this.handleChange}
                                                value={group.name}
                                                placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="permissions">
                                                <FormattedMessage id="permission.title.many"/>
                                            </label>
                                            <Selector
                                                id="permissions"
                                                items={permissions}
                                                selected={group.permissions}
                                                onChange={this.permissionChange}
                                            />
                                        </div>

                                        <button type="button" className={submitClasses}
                                                onClick={isEdit ? this.updateGroup : this.createGroup}>
                                            <FormattedMessage id="grid.action.submit.title.up"/>
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(GroupForm);
