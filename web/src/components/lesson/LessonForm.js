import React, {Component, PropType} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import CommonComponent from 'components/common/CommonComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import MarkdownEditor from 'components/common/markdown-editor/MarkdownEditor';
import LessonModel from 'models/Lesson';

class LessonForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {lesson, params} = props;

        this.state = {
            lesson: !!params.lessonId ? {...lesson.item, type_id: lesson.item.type.id} : {...LessonModel}
        };

        this.handleChange = ::this.handleChange;
        this.contentChange = ::this.contentChange;
        this.createLesson = ::this.createLesson;
        this.updateLesson = ::this.updateLesson;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        /* TODO: Load all lessonType pages */
        actions.loadLessonTypePage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        if (params.lessonId) {
            actions.loadLesson(params.lessonId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {params, lesson} = newProps;

        if (this.state.lesson !== lesson.item && !!params.lessonId) {
            this.state.lesson = {...lesson.item, type_id: lesson.item.type.id};
        }
    }

    updateLesson() {
        let {actions, routeActions} = this.props;

        actions.updateLesson(this.state.lesson).then(() => {
            routeActions.push('/lesson');
        });
    }

    createLesson() {
        let {actions, routeActions} = this.props;
        let lesson = this.state.lesson;
        delete lesson.type;
        delete lesson.author;

        actions.createLesson(lesson).then(() => {
            routeActions.push('/lesson');
        });
    }

    contentChange(content) {
        this.state.lesson.content = content;
        this.setState(this.state);
    }

    handleChange(e) {
        this.state.lesson[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    render() {
        let {intl: {formatMessage}, params, lessonType} = this.props;
        let isEdit = !!params.lessonId;
        let {lesson} = this.state;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="lesson.title.many" />
                    </h1>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title" />
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="name"
                                                onChange={this.handleChange}
                                                value={lesson.name}
                                                placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="type">
                                                <FormattedMessage id="common.type.title" />
                                            </label>
                                            <select
                                                id="type_id"
                                                className="form-control input-sm"
                                                value={lesson.type_id}
                                                onChange={this.handleChange}>
                                                <option disabled>
                                                    { formatMessage({id: 'common.action.select.title'}) }
                                                </option>
                                                { lessonType.page.results.map(item => {
                                                    return <option key={item.id} value={item.id}>{item.name}</option>;
                                                }) }
                                            </select>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="content">
                                                <FormattedMessage id="common.content.title" />
                                            </label>
                                            <MarkdownEditor
                                                id="content"
                                                content={lesson.content}
                                                onChange={this.contentChange}
                                            />
                                        </div>

                                        <button type="button" className={submitClasses} onClick={isEdit ? this.updateLesson : this.createLesson}>
                                            <FormattedMessage id="grid.action.submit.title.up" />
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(LessonForm);
