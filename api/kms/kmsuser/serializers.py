from rest_framework import serializers
from django.contrib.auth.models import Group, Permission
from skill.serializers import SkillSerializer
from file.serializers import FileSerializer
from skill.models import Skill
from .models import User, UserSkill


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group


class PermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permission


class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(read_only=True, many=True)
    group_ids = serializers.ListField(write_only=True, child=serializers.IntegerField(), required=False)
    skills = SkillSerializer(read_only=True, many=True)
    skill_ids = serializers.ListField(write_only=True, child=serializers.IntegerField(), required=False)
    avatar = FileSerializer(read_only=True)
    avatar_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = User
        read_only_fields = ('id', )
        write_only_fields = ('password', )
        fields = ('id', 'username', 'email', 'password', 'first_name', 'last_name', 'is_superuser', 'phone',
                  'skype', 'groups', 'group_ids', 'skills', 'skill_ids', 'avatar', 'avatar_id')

    def create(self, validated_data):
        # Exclude confirm_password field during the creation User
        validated_data.pop('confirm_password', None)

        password = validated_data.pop('password', None)
        group_ids = validated_data.pop('group_ids', None)
        skill_ids = validated_data.pop('skill_ids', None)
        logged_in_user = self.context['request'].user
        user = self.Meta.model(**validated_data)

        if password is not None:
            user.set_password(password)
        user.save()

        if logged_in_user.pk is None:
            employee_group = Group.objects.get(pk=2)
            user.groups = [employee_group]
        elif group_ids is not None:
            groups = Group.objects.filter(pk__in=group_ids)
            user.groups = groups
        if skill_ids is not None:
            for skill_id in skill_ids:
                skill = Skill.objects.get(pk=skill_id)
                UserSkill.objects.create(user=user, skill=skill)

        return user

    def update(self, user, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                user.set_password(value)
            elif attr == 'group_ids':
                groups = Group.objects.filter(pk__in=value)
                user.groups = groups
            elif attr == 'skill_ids':
                for skill_id in value:
                    skill = Skill.objects.get(pk=skill_id)
                    UserSkill.objects.create(user=user, skill=skill)
            else:
                setattr(user, attr, value)

        user.save()

        return user


class UserSkillSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    skill = SkillSerializer()

    class Meta:
        model = UserSkill