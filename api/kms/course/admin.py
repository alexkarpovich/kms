from django.contrib import admin
from django.utils.translation import ugettext, ugettext_lazy as _

from .models import Course


class SkillsInlineAdmin(admin.TabularInline):
    model = Course.skills.through
    extra = 0


class LessonsInlineAdmin(admin.TabularInline):
    model = Course.lessons.through
    extra = 0


class CourseAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {'fields': ('name', 'description', 'complexity')}),
        (_('Other fields'), {'fields': ('rating', )}),
    )
    list_display = ('name', 'complexity', 'rating', )
    list_filter = ('name', 'complexity', 'rating', )
    search_fields = ('name', 'description', )
    inlines = (SkillsInlineAdmin, LessonsInlineAdmin, )


admin.site.register(Course, CourseAdmin)
