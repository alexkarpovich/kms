import React from 'react';
import CommonComponent from 'components/common/CommonComponent';

export default class UserSkillComponent extends CommonComponent {
    constructor(props) {
        super(props);

        this.showSkillForm = ::this.showSkillForm;
        this.cancelSkill = ::this.cancelSkill;
        this.saveSkill = ::this.saveSkill;
        this.deleteSkill = ::this.deleteSkill;
    }

    cancelSkill() {
        this.showSkillForm(false);
    }

    saveSkill(userSkill) {
        let {actions, params, auth: {authUser}} = this.props;
        let userId = params.userId ? params.userId : authUser.id;

        actions.addUserSkill(userId, userSkill).then(() => {
            this.state.showSkillForm = false;
            actions.loadUserSkills(userId);
        });
    }

    deleteSkill(userSkillId) {
        let {actions, params, auth: {authUser}} = this.props;
        let userId = params.userId ? params.userId : authUser.id;

        actions.deleteUserSkill(userId, userSkillId).then(() => {
            actions.loadUserSkills(userId);
        });
    }

    showSkillForm(showSkillForm) {
        this.state.showSkillForm = showSkillForm;
        this.setState(this.state);
    }
}
