# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('file', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='content_type',
            field=models.CharField(max_length=30, default='', blank=True),
        ),
        migrations.AlterField(
            model_name='file',
            name='filename',
            field=models.CharField(max_length=255, default='', blank=True),
        ),
        migrations.AlterField(
            model_name='file',
            name='path',
            field=models.FileField(upload_to=''),
        ),
    ]
