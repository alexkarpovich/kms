import {combineReducers} from 'redux';
import {SkillCategoryConst} from 'constants';
import SkillCategoryModel from 'models/SkillCategory';
import PageModel from 'models/Page';


function item(state = SkillCategoryModel, action) {
    switch(action.type) {
        case SkillCategoryConst.LOAD_SKILL_CATEGORY_REQUEST:
            return {...state, isFetching: false, error: null};

        case SkillCategoryConst.LOAD_SKILL_CATEGORY_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case SkillCategoryConst.LOAD_SKILL_CATEGORY_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case SkillCategoryConst.LOAD_SKILL_CATEGORY_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case SkillCategoryConst.LOAD_SKILL_CATEGORY_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case SkillCategoryConst.LOAD_SKILL_CATEGORY_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
