from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.core.validators import MaxValueValidator, MinValueValidator
from skill.models import Skill
from file.models import File


class User(AbstractUser):
    phone = models.CharField(max_length=100, blank=True, default='')
    mobile = models.CharField(max_length=100, blank=True, default='')
    skype = models.CharField(max_length=100, blank=True, default='')
    about = models.TextField(max_length=300, blank=True, default='')
    skills = models.ManyToManyField(Skill, through='UserSkill', related_name='users', blank=True)
    avatar = models.ForeignKey(File, related_name='user', blank=True, null=True)

    objects = UserManager()

    def get_skills(self):
        return UserSkill.objects.filter(user_id=self.id)

    def add_skill(self, request):
        skill_id = request.data.get('skill_id')
        level = request.data.get('level')

        return UserSkill.objects.update_or_create(user_id=self.id, skill_id=skill_id, defaults={'level': level})

    def delete_skill(self, user_skill_id):
        return UserSkill.objects.get(pk=user_skill_id).delete()

    def __str__(self):
        return self.username

    def __unicode__(self):
        return self.username


class UserSkill(models.Model):
    user = models.ForeignKey(User, related_name='membership')
    skill = models.ForeignKey(Skill, related_name='membership')
    level = models.IntegerField(blank=False, default=15, validators=[
        MinValueValidator(0),
        MaxValueValidator(100)
    ])

    class Meta:
        unique_together = ('user', 'skill')
