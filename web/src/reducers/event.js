import {combineReducers} from 'redux';
import {EventConst} from 'constants';
import EventModel from 'models/Event';
import PageModel from 'models/Page';


function item(state = EventModel, action) {
    switch(action.type) {
        case EventConst.LOAD_EVENT_REQUEST:
            return {...state, isFetching: false, error: null};

        case EventConst.LOAD_EVENT_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case EventConst.LOAD_EVENT_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case EventConst.LOAD_EVENT_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case EventConst.LOAD_EVENT_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case EventConst.LOAD_EVENT_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
