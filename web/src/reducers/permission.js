import {combineReducers} from 'redux';
import {PermissionConst} from 'constants';
import PageModel from 'models/Page';
import PermissionModel from 'models/Permission';


function item(state = PermissionModel, action) {
     switch(action.type) {
         case PermissionConst.LOAD_PERMISSION_REQUEST:
             return {...state, isFetching: false, error: null};

         case PermissionConst.LOAD_PERMISSION_SUCCESS:
             return {...state, ...action.data, isFetching: false};

         case PermissionConst.LOAD_PERMISSION_FAILURE:
             return {...state, error: action.error, isFetching: false};

         default:
             return state;
     }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case PermissionConst.LOAD_PERMISSION_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case PermissionConst.LOAD_PERMISSION_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case PermissionConst.LOAD_PERMISSION_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
