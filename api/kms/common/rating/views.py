from common.mixins import FilterModelViewSetMixin
from .models import Rating
from .serializers import RatingSerializer


class RatingViewSet(FilterModelViewSetMixin):
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer
    search_fields = ('id', 'rating', 'user__username')
    ordering_fields = ('id', 'rating', 'user')
