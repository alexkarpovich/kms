import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {FormattedMessages} from 'react-intl';

export default class LessonItem extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        lesson: PropTypes.object.isRequired,
        routeActions: PropTypes.object.isRequired
    };

    render() {
        let {lesson} = this.props;

        return (
            <Link to={`/lesson/${lesson.id}`} className="list-group-item">
                <h4 className="list-group-item-heading">{lesson.name}</h4>
            </Link>
        );
    }
}
