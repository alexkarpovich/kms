import 'Jcrop/js/jquery.Jcrop';
import './ImageCropper.less';
import React, {Component, PropTypes} from 'react';

const MAX_BOX_WIDTH = 400;
const MAX_BOX_HEIGHT = 400;

var cropper = null;
var naturalWidth = 0;
var naturalHeight = 0;

export default class Cropper extends Component {
    constructor(props, context) {
        super(props, context);

        this.onAreaChange = ::this.onAreaChange;
        this.onAreaSelect = ::this.onAreaSelect;
        this.showPreview = ::this.showPreview;
    }

    static contextTypes = {
        setCoords: PropTypes.func,
        getFileData: PropTypes.func
    };

    componentDidMount() {
        var origin = $('#origin');
        var fileData = this.context.getFileData();
        cropper = $.Jcrop(origin, {});
        cropper.setOptions({
            aspectRatio: 1,
            boxWidth: MAX_BOX_WIDTH,
            boxHeight: MAX_BOX_HEIGHT,
            onChange: this.onAreaChange,
            onSelect: this.onAreaSelect
        });

        cropper.setImage(fileData.path);

        var tmp = new Image();
        tmp.src = origin.attr("src");
        $(tmp).on('load', function () {
            naturalWidth = tmp.width;
            naturalHeight = tmp.height;
        });
    }

    onAreaChange(e) {
        this.showPreview(e);
    }

    onAreaSelect(e) {
        this.context.setCoords(e);
        this.showPreview(e);
    }

    showPreview(coords) {
        var rx = 100 / coords.w;
        var ry = 100 / coords.h;

        $('#preview').css({
            width: Math.round(rx * naturalWidth) + 'px',
            height: Math.round(ry * naturalHeight) + 'px',
            marginLeft: '-' + Math.round(rx * coords.x) + 'px',
            marginTop: '-' + Math.round(ry * coords.y) + 'px'
        });
    }

    render() {
        var fileData = this.context.getFileData();

        return (
            <div className="image-cropper row">
                <div className="original-view col-xs-9 col-sm-9 col-md-9 col-lg-9">
                    <img id="origin" src={fileData.path} />
                </div>
                <div className="image-cropped-view col-xs-3 col-sm-3 col-md-3 col-lg-3">
                    <img id="preview" src={fileData.path} />
                </div>
            </div>
        );
    }
}
