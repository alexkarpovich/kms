from django.contrib import admin

from .models import Skill, SkillCategory


class SkillAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', )
    list_filter = ('name', 'category', )
    search_fields = ('name', )


admin.site.register(SkillCategory)
admin.site.register(Skill, SkillAdmin)
