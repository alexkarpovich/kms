import React, {Component} from 'react';
import {Link} from 'react-router';
import PathHelper from 'helpers/path';

export default class Header extends Component {
    constructor(props, context) {
        super(props, context);

        this.logout = ::this.logout;
    }

    logout() {
        this.props.logout();
        this.props.routeActions.push('/');
    }

    render() {
        let {isAuthenticated, authUser} = this.props.authData;
        let avatarSrc = authUser && authUser.avatar && authUser.avatar.path ?
            PathHelper.image(authUser.avatar.path, 32, 32) : '/images/avatar32x32.png';

        return (
            <header className="main-header">
                <a href="/" className="logo">
                    <span className="logo-mini"><b>KMS</b></span>
                    <span className="logo-lg"><b>KMS</b></span>
                </a>

                <nav className="navbar navbar-static-top" role="navigation">
                    <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"/>
                        <span className="icon-bar"/>
                        <span className="icon-bar"/>
                    </a>

                    <div className="navbar-custom-menu">
                        { isAuthenticated && authUser ? (
                            <ul className="nav navbar-nav">
                                <li className="dropdown user user-menu">
                                    <a href="#" className="dropdown-toggle" data-toggle="dropdown"
                                       aria-expanded="false">
                                        <img src={avatarSrc} className="user-image"/>
                                        <span className="hidden-xs">
                                            {authUser.firstName && authUser.lastName ? authUser.firstName + ' ' + authUser.lastName : authUser.username}
                                        </span>&nbsp;
                                        <span className="caret"/>
                                    </a>
                                    <ul className="dropdown-menu" role="menu">
                                        <li>
                                            <Link to="/profile">
                                                Profile
                                            </Link>
                                        </li>
                                        <li className="divider"/>
                                        <li>
                                            <a href="#" onClick={this.logout}>
                                                Logout
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        ) : (
                            <ul className="nav navbar-nav">
                                <li>
                                    <Link to="/register">
                                        Register
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/login">
                                        Login
                                    </Link>
                                </li>
                            </ul>
                        ) }

                    </div>
                </nav>
            </header>
        );
    }
}
