import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import Markdown from 'react-markdown';
import CommentBox from 'components/common/comment-box/CommentBox';

class LessonView extends Component {
    constructor(props) {
        super(props);

        this.postLessonComment = ::this.postLessonComment;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        actions.loadLesson(params.lessonId);
        actions.loadCommentList('lesson', params.lessonId);
    }

    postLessonComment(comment) {
        let {actions, params} = this.props;

        return actions.postComment('lesson', params.lessonId, comment).then(() => {
            actions.loadCommentList('lesson', params.lessonId);
        });
    }

    render() {
        let {lesson:{item: lesson}, auth: {authUser}, comment: {list: commentList}} = this.props;

        return (
            <div className="lesson-view">
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="lesson.title.many" />
                    </h1>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <Markdown source={lesson.content ? lesson.content : ''} />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <CommentBox
                                authUser={authUser}
                                conversation={commentList}
                                postComment={this.postLessonComment}
                            />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(LessonView);
