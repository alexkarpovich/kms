from django.db import models
from datetime import datetime


class EventType(models.Model):
    name = models.CharField(max_length=255, blank=False, default='')
    description = models.TextField(blank=True, default='')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length=255, blank=False, default='')
    description = models.TextField(blank=True, default='')
    type = models.ForeignKey(EventType, blank=False)
    start = models.DateTimeField(default=datetime.now, blank=False)
    end = models.DateTimeField(default=datetime.now, blank=False)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name
