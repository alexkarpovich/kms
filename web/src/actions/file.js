import {FileConst} from 'constants';
import api from 'helpers/api';

export function loadFile(fileId) {
    return async dispatch => {
        dispatch({type: FileConst.LOAD_FILE_REQUEST});

        try {
            let data = await api({url: `file/${fileId}/`});

            dispatch({type: FileConst.LOAD_FILE_SUCCESS, data});
        } catch (error) {
            dispatch({type: FileConst.LOAD_FILE_FAILURE, error});
        }
    };
}

export function uploadFile(filename, file, uploadAs) {
    return async dispatch => {
        dispatch({type: FileConst.UPLOAD_FILE_REQUEST});

        let formData = new FormData();
        formData.append('filename', filename);
        formData.append('file', file);
        formData.append('uploadAs', uploadAs);

        try {
            let data = await api({url: `file/`, method: 'post', data: formData, isUpload: true});

            dispatch({type: FileConst.UPLOAD_FILE_SUCCESS, data});
        } catch (error) {
            dispatch({type: FileConst.UPLOAD_FILE_FAILURE, error});
        }
    };
}

export function cropImage(fileId, params) {
    return async dispatch => {
        dispatch({type: FileConst.CROP_IMAGE_REQUEST});

        try {
            await api({url: `file/${fileId}/crop/`, method: 'post', data: params});

            dispatch({type: FileConst.CROP_IMAGE_SUCCESS});
        } catch (error) {
            dispatch({type: FileConst.CROP_IMAGE_FAILURE, error});
        }
    };
}
