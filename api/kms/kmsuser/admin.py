from django.contrib import admin
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import User


class SkillsInlineAdmin(admin.TabularInline):
    model = User.skills.through
    extra = 0


class UserAdmin(BaseUserAdmin):

    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Other fields'), {'fields': ('mobile', 'skype', 'about', 'avatar', )}),
    )
    inlines = (SkillsInlineAdmin,)


admin.site.register(User, UserAdmin)
