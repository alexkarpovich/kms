import keymirror from 'keymirror';

export const FileConst = keymirror({
    LOAD_FILE_REQUEST: null,
    LOAD_FILE_SUCCESS: null,
    LOAD_FILE_FAILURE: null,

    UPLOAD_FILE_REQUEST: null,
    UPLOAD_FILE_SUCCESS: null,
    UPLOAD_FILE_FAILURE: null,

    CROP_IMAGE_REQUEST: null,
    CROP_IMAGE_SUCCESS: null,
    CROP_IMAGE_FAILURE: null
});
