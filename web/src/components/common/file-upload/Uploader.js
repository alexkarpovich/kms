import './Uploader.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';

export default class Uploader extends Component {
    constructor(props, context) {
        super(props, context);

        this.fileChange = ::this.fileChange;
    }

    static contextTypes = {
        setFileData: PropTypes.func,
        uploadFile: PropTypes.func.isRequired
    };

    fileChange(e) {
        let file = e.target.files[0];
        let filename = file.name;

        this.context.uploadFile(filename, file);
    }

    render() {
        return (
            <div className="uploader">
                <div className="info">
                    <FormattedMessage id="file.uploader.info" />
                </div>
                <div className="choose-file">
                    <button type="button" className="btn btn-default btn-sm">
                        <input
                            type="file"
                            name="file"
                            id="file"
                            onChange={this.fileChange}
                        />
                        <i className="fa fa-upload fa-md"></i>
                        <FormattedMessage id="file.action.choose" />
                    </button>
                </div>
            </div>
        );
    }
}
