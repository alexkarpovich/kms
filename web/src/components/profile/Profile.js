import './Profile.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import Select from 'react-select';
import PathHelper from 'helpers/path';
import UserSkillComponent from 'components/common/UserSkillComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import FileUpload from 'components/common/file-upload/FileUpload';
import SkillLevelForm from 'components/common/skill-level/SkillLevelForm';
import SkillLevelView from 'components/common/skill-level/SkillLevelView';
import UserModel from 'models/User';

class Profile extends UserSkillComponent {
    constructor(props) {
        super(props);

        let {user, params} = props;
        let groupIds = user.item.groups.map(group => group.id);
        let skillIds = user.item.skills.map(skill => skill.id);
        let avatarId = user.item.avatar ? user.item.avatar.id : null;

        this.state = {
            user: !!params.userId ? {
                ...user.item,
                group_ids: groupIds,
                skill_ids: skillIds,
                avatar_id: avatarId
            } : {...UserModel},
            avatar: user.item.avatar,
            changesSaved: false,
            showSkillForm: false
        };

        this.updateUser = ::this.updateUser;
        this.handleChange = ::this.handleChange;
        this.arrayChange = ::this.arrayChange;
        this.avatarChange = ::this.avatarChange;
    }

    componentDidMount() {
        let {actions, auth} = this.props;

        /* TODO: temp solution */
        actions.loadGroupPage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        actions.loadSkillPage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        actions.loadUser(auth.authUser.id);
        actions.loadUserSkills(auth.authUser.id);
    }

    componentWillReceiveProps(newProps) {
        let {user: {item}} = newProps;
        let {user} = this.state;
        let groupIds = item.groups.map(group => group.id);
        let skillIds = item.skills.map(skill => skill.id);
        let avatarId = item.avatar ? item.avatar.id : null;

        if (user !== item) {
            this.state.user = {
                ...item,
                group_ids: groupIds,
                skill_ids: skillIds,
                avatar_id: avatarId
            };
            this.state.avatar = item.avatar;
        }
    }

    updateUser() {
        let {actions} = this.props;
        let user = {...this.state.user};
        delete user.groups;
        delete user.skills;
        delete user.avatar;

        if (user.password === user.confirmPassword) {
            actions.updateUser(user);

            this.state.changesSaved = true;
            this.setState(this.state);
        }
    }

    handleChange(e) {
        this.state.user[e.target.id] =e.target.value;
        this.setState(this.state);
    }

    arrayChange(items, name) {
        this.state.user[name] = items.map(item => item.value);
        this.setState(this.state);
    }

    avatarChange(fileData) {
        this.state.user.avatar_id = fileData.id;
        this.state.avatar = fileData;
        this.setState(this.state);
    }

    render() {
        let {user, avatar, changesSaved, showSkillForm} = this.state;
        let {intl: {formatMessage}, group:{page: groupPage}, skill: {page: skillPage}, actions,
            user: {skills: userSkills}} = this.props;

        let groups = groupPage.results.map(group => ({
            label: group.name,
            value: group.id
        }));

        let avatarSrc = !!avatar && avatar.path ?
            PathHelper.image(avatar.path, 256, 256) : '/images/avatar256x256.png';

        return (
            <div className="profile">
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="profile.breadcrumb"/>
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="avatar col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <div className="box box-primary">
                                <div className="box-body box-profile">
                                    <img src={avatarSrc} />
                                    <h3 className="profile-username text-center">
                                        {user.firstName + ' ' + user.lastName}
                                    </h3>
                                    <FileUpload
                                        {...this.props}
                                        uploadAs="image"
                                        onChange={this.avatarChange}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                            <div className="nav-tabs-custom">
                                <ul className="nav nav-tabs">
                                    <li className="active">
                                        <a href="#settings" data-toggle="tab">
                                            <FormattedMessage id="profile.settings"/>
                                        </a>
                                    </li>
                                </ul>
                                <div className="tab-content">
                                    <div className="tab-pane active" id="settings">
                                        { changesSaved ?
                                            <div className="alert alert-success alert-dismissible">
                                                <button
                                                    type="button"
                                                    className="close"
                                                    data-dismiss="alert"
                                                    aria-hidden="true">×
                                                </button>
                                                <h4>
                                                    <FormattedMessage id="profile.changesSaved.title"/>
                                                </h4>
                                                <FormattedMessage id="profile.changesSaved.message"/>
                                            </div>
                                            : null
                                        }

                                        <form className="form-horizontal">
                                            <div className="form-group">
                                                <label htmlFor="username" className="col-sm-3 control-label">
                                                    <FormattedMessage id="user.username.title"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <input
                                                        type="text"
                                                        className="form-control input-sm"
                                                        id="username"
                                                        onChange={this.handleChange}
                                                        value={user.username}
                                                        placeholder={formatMessage({id: 'user.username.title'})}/>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="email" className="col-sm-3 control-label">
                                                    <FormattedMessage id="user.email.title"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <input
                                                        type="email"
                                                        className="form-control input-sm"
                                                        id="email"
                                                        onChange={this.handleChange}
                                                        value={user.email}
                                                        placeholder={formatMessage({id: 'user.email.title'})}
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="groups" className="col-sm-3 control-label">
                                                    <FormattedMessage id="group.title.many"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <Select
                                                        name="group_ids"
                                                        options={groups}
                                                        value={user.group_ids}
                                                        onChange={(groups) => this.arrayChange(groups, 'group_ids')}
                                                        multi={true}
                                                        placeholder={formatMessage({id: 'common.action.select.title'})}
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="skills" className="col-sm-3 control-label">
                                                    <FormattedMessage id="skill.title.many" />
                                                </label>
                                                <div className="col-sm-9">
                                                    <button type="button" className="btn btn-xs btn-link" onClick={() => this.showSkillForm(true)}>
                                                        <i className="fa fa-plus" />
                                                        <FormattedMessage id="common.action.add.title" />
                                                    </button>
                                                    <br />
                                                    {userSkills.data.map(item =>
                                                        <SkillLevelView
                                                            key={item.id}
                                                            userSkill={item}
                                                            onDelete={this.deleteSkill}
                                                            onSave={this.saveSkill}
                                                        />)}
                                                    {showSkillForm ?
                                                        <SkillLevelForm
                                                            userSkills={userSkills.data}
                                                            allSkills={skillPage.results}
                                                            onCancel={this.cancelSkill}
                                                            onSave={this.saveSkill}
                                                        />
                                                    : null}
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="password" className="col-sm-3 control-label">
                                                    <FormattedMessage id="user.password.title"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <input
                                                        type="password"
                                                        className="form-control input-sm"
                                                        id="password"
                                                        onChange={this.handleChange}
                                                        placeholder={formatMessage({id: 'user.password.title'})}
                                                    />
                                                </div>
                                            </div>

                                            <div className="form-group">
                                                <label htmlFor="confirmPassword" className="col-sm-3 control-label">
                                                    <FormattedMessage id="user.confirmPassword.title"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <input
                                                        type="password"
                                                        className="form-control input-sm"
                                                        id="confirmPassword"
                                                        onChange={this.handleChange}
                                                        placeholder={formatMessage({id: 'user.confirmPassword.title'})}
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="firstName" className="col-sm-3 control-label">
                                                    <FormattedMessage id="user.firstName.title"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <input
                                                        type="text"
                                                        className="form-control input-sm"
                                                        id="firstName"
                                                        onChange={this.handleChange}
                                                        value={user.firstName}
                                                        placeholder={formatMessage({id: 'user.firstName.title'})}
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="lastName" className="col-sm-3 control-label">
                                                    <FormattedMessage id="user.lastName.title"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <input
                                                        type="text"
                                                        className="form-control input-sm"
                                                        id="lastName"
                                                        onChange={this.handleChange}
                                                        value={user.lastName}
                                                        placeholder={formatMessage({id: 'user.lastName.title'})}
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="phone" className="col-sm-3 control-label">
                                                    <FormattedMessage id="user.phone.title"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <input
                                                        type="text"
                                                        className="form-control input-sm"
                                                        id="phone"
                                                        onChange={this.handleChange}
                                                        value={user.phone}
                                                        placeholder={formatMessage({id: 'user.phone.title'})}
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="skype" className="col-sm-3 control-label">
                                                    <FormattedMessage id="user.skype.title"/>
                                                </label>
                                                <div className="col-sm-9">
                                                    <input
                                                        type="text"
                                                        className="form-control input-sm"
                                                        id="skype"
                                                        onChange={this.handleChange}
                                                        value={user.skype}
                                                        placeholder={formatMessage({id: 'user.skype.title'})}
                                                    />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-sm-offset-3 col-sm-9">
                                                    <button
                                                        type="button"
                                                        className="btn btn-sm btn-danger"
                                                        onClick={this.updateUser}>
                                                        <FormattedMessage id="profile.btn.save"/>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(Profile);
