import {combineReducers} from 'redux';
import {CommentConst} from 'constants';

let commentListState = {
    data: []
};

function list(state = commentListState, action) {
    switch(action.type) {
        case CommentConst.LOAD_COMMENT_LIST_REQUEST:
            return {...state, isFetching: true, error: null};

        case CommentConst.LOAD_COMMENT_LIST_SUCCESS:
            return {...state, data: action.data, isFetching: false, error: null};

        case CommentConst.LOAD_COMMENT_LIST_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    list
});
