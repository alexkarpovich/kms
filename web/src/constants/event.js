import keymirror from 'keymirror';

export const EventConst = keymirror({
    LOAD_EVENT_REQUEST: null,
    LOAD_EVENT_SUCCESS: null,
    LOAD_EVENT_FAILURE: null,

    LOAD_EVENT_PAGE_REQUEST: null,
    LOAD_EVENT_PAGE_SUCCESS: null,
    LOAD_EVENT_PAGE_FAILURE: null,

    CREATE_EVENT_REQUEST: null,
    CREATE_EVENT_SUCCESS: null,
    CREATE_EVENT_FAILURE: null,

    UPDATE_EVENT_REQUEST: null,
    UPDATE_EVENT_SUCCESS: null,
    UPDATE_EVENT_FAILURE: null,

    DELETE_EVENT_REQUEST: null,
    DELETE_EVENT_SUCCESS: null,
    DELETE_EVENT_FAILURE: null,
});
