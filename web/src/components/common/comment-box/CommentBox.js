import './CommentBox.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import CommentForm from './CommentForm';
import Conversation from './Conversation';

class CommentBox extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        authUser: PropTypes.object.isRequired,
        conversation: PropTypes.object.isRequired,
        postComment: PropTypes.func.isRequired
    };

    static childContextTypes = {
        authUser: PropTypes.object.isRequired,
        postComment: PropTypes.func.isRequired
    };

    getChildContext() {
        let {authUser, postComment} = this.props;

        return {
            authUser: authUser,
            postComment: postComment
        };
    }

    render() {
        let {conversation, authUser} = this.props;

        return (
            <div className="comment-box box">
                <h4>
                    <FormattedMessage id="common.comment.title.many" />
                </h4>
                {authUser ?
                <div>
                    <Conversation conversation={conversation}/>
                    <CommentForm
                        parentId={null}
                        focus={false}
                    />
                </div>
                : null }
            </div>
        );
    }
}

export default injectIntl(CommentBox);
