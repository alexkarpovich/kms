import React, {Component, PropType} from 'react';
import {FormattedMessage} from 'react-intl';
import {Grid, GridConfig, GridMapping} from 'components/common/grid/Grid';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';


export default class EventList extends Component {
    render() {
        let {event, actions} = this.props;
        let gridConfig = new GridConfig();
        let gridMapping = new GridMapping({
            actions: {
                loadPage: actions.loadEventPage,
                deleteEntity: actions.deleteEvent
            }
        });

        let columns = [
            {key: 'id', name: 'common.id.title'},
            {key: 'name', name: 'common.name.title'},
            {key: 'description', name: 'common.description.title'},
            {key: 'type', name: 'common.type.title'},
            {key: 'start', name: 'event.startDate.title'},
            {key: 'end', name: 'event.endDate.title'}
        ];

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="event.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <Grid
                                entity={event.page}
                                config={gridConfig}
                                mapping={gridMapping}
                                columns={columns}
                                rootUrl={'/admin/event/'}
                            />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
