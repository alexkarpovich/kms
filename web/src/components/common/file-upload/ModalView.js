import './ModalView.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import Uploader from './Uploader';
import ImageCropper from './ImageCropper';

const BOX_WIDTH = 400;
const BOX_HEIGHT = 400;

let cropper = null;

export default class ModalView extends Component {
    constructor(props, context) {
        super(props, context);

        this.apply = ::this.apply;
    }

    static propTypes = {
        id: PropTypes.string.isRequired
    };

    static contextTypes = {
        uploadAs: PropTypes.oneOf(['image', 'file']),
        getFileData: PropTypes.func,
        cropImage: PropTypes.func
    };

    apply() {
        this.context.cropImage();
    }

    render() {
        let {id} = this.props;
        let {uploadAs, getFileData} = this.context;

        let body = getFileData().path && uploadAs == 'image' ?
            <ImageCropper /> :
            <Uploader />;

        return (
            <div className="modal fade" id={id} tabIndex="-1" role="dialog" aria-labelledby="viewModal">
              <div className="modal-dialog" role="document">
                <div className="modal-content modal-min-width">
                  <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 className="modal-title" id="viewModal">
                        <FormattedMessage id="file.uploader.title" />
                    </h4>
                  </div>
                  <div className="modal-body">
                      {body}
                  </div>
                  <div className="modal-footer">
                    <button type="button" className="btn btn-default" data-dismiss="modal">
                        <FormattedMessage id="common.action.cancel.title" />
                    </button>
                    <button type="button" className="btn btn-primary" onClick={this.apply}>
                        <FormattedMessage id="common.confirmation.ok.title" />
                    </button>
                  </div>
                </div>
              </div>
            </div>
        );
    }
}
