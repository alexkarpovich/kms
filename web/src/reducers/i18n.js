import {I18nConst} from 'constants'
import * as locales from  '../configs/i18n/messages'

const initialState = {
    locale: 'en',
    availableLocales: Object.keys(locales),
    messages: locales.en
};

export default function i18n(state = initialState, action) {
    switch(action.type) {
        case I18nConst.CHANGE_LOCALE:
            return {...state, locale: action.locale, messages: locales[action.locale]};
        default:
            return state;
    }
}
