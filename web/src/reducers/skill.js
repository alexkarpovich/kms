import {combineReducers} from 'redux';
import {SkillConst} from 'constants';
import SkillModel from 'models/Skill';
import PageModel from 'models/Page';


function item(state = SkillModel, action) {
    switch(action.type) {
        case SkillConst.LOAD_SKILL_REQUEST:
            return {...state, isFetching: false, error: null};

        case SkillConst.LOAD_SKILL_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case SkillConst.LOAD_SKILL_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function page(state = PageModel, action) {
    switch(action.type) {
        case SkillConst.LOAD_SKILL_PAGE_REQUEST:
            return {...state, isFetching: true, error: null};

        case SkillConst.LOAD_SKILL_PAGE_SUCCESS:
            return {...state, ...action.data, isFetching: false, error: null};

        case SkillConst.LOAD_SKILL_PAGE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
