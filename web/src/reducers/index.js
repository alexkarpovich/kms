import {routeReducer} from 'react-router-redux';
import {combineReducers} from 'redux';
import auth from './auth';
import event from './event';
import eventType from './event-type';
import lesson from './lesson';
import lessonType from './lesson-type';
import permission from './permission';
import file from './file';
import user from './user';
import group from './group';
import course from './course';
import skill from './skill';
import skillCategory from './skill-category';
import comment from './comment';
import i18n from './i18n';

const reducer = combineReducers({
    routing: routeReducer,
    auth,
    event,
    eventType,
    lesson,
    lessonType,
    permission,
    file,
    user,
    group,
    course,
    skill,
    skillCategory,
    comment,
    i18n
});

export default reducer;
