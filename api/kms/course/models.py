from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from markdownx.models import MarkdownxField
from common.mixins import CommentModelMixin
from skill.models import Skill
from lesson.models import Lesson
from kmsuser.models import User


class Course(models.Model, CommentModelMixin):
    name = models.CharField(max_length=255, blank=False, default='')
    description = MarkdownxField(blank=True, default='')
    complexity = models.IntegerField(blank=False, default=30, validators=[
        MinValueValidator(0),
        MaxValueValidator(100)
    ])
    skills = models.ManyToManyField(Skill, related_name='courses', blank=True)
    lessons = models.ManyToManyField(Lesson, related_name='courses', blank=True)
    rating = models.FloatField(blank=True, default=2.5, validators=[
        MinValueValidator(0),
        MaxValueValidator(5)
    ])
    author = models.ForeignKey(User, related_name='course', blank=False, null=False, default=1)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name
