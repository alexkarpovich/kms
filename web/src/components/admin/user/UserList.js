import React, {Component, PropType} from 'react';
import {FormattedMessage} from 'react-intl';
import {Grid, GridConfig, GridMapping} from 'components/common/grid/Grid';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';


export default class UserList extends Component {
    render() {
        let {user, actions} = this.props;
        let gridConfig = new GridConfig();
        let gridMapping = new GridMapping({
            actions: {
                loadPage: actions.loadUserPage,
                deleteEntity: actions.deleteUser
            }
        });

        let columns = [
            {key: 'id', name: 'common.id.title'},
            {key: 'username', name: 'user.username.title'},
            {key: 'email', name: 'user.email.title'},
            {key: 'firstName', name: 'user.firstName.title'},
            {key: 'lastName', name: 'user.lastName.title'},
            {key: 'phone', name: 'user.phone.title'},
            {key: 'skype', name: 'user.skype.title'}
        ];

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="user.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <Grid
                                entity={user.page}
                                config={gridConfig}
                                mapping={gridMapping}
                                columns={columns}
                                rootUrl={'/admin/user/'}
                            />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
