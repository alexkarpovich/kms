import {GroupConst} from 'constants';
import api from 'helpers/api';

export function loadGroupPage(params) {
    return async dispatch => {
        dispatch({type: GroupConst.LOAD_GROUP_PAGE_REQUEST});

        try {
            let data = await api({url: 'group/', query: params});

            dispatch({type: GroupConst.LOAD_GROUP_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: GroupConst.LOAD_GROUP_PAGE_FAILURE, error});
        }
    }
}

export function loadGroup(groupId) {
    return async dispatch => {
        dispatch({type: GroupConst.LOAD_GROUP_REQUEST});

        try {
            let data = await api({url: `group/${groupId}/`});

            dispatch({type: GroupConst.LOAD_GROUP_SUCCESS, data});
        } catch (error) {
            dispatch({type: GroupConst.LOAD_GROUP_FAILURE, error});
        }
    };
}

export function createGroup(group) {
    return async dispatch => {
        dispatch({type: GroupConst.CREATE_GROUP_REQUEST});

        try {
            await api({url: `group/`, method: 'post', data: group});

            dispatch({type: GroupConst.CREATE_GROUP_SUCCESS});
        } catch (error) {
            dispatch({type: GroupConst.CREATE_GROUP_FAILURE, error});
        }
    };
}

export function updateGroup(group) {
    return async dispatch => {
        dispatch({type: GroupConst.UPDATE_GROUP_REQUEST});

        try {
            await api({url: `group/${group.id}/`, method: 'put', data: group});

            dispatch({type: GroupConst.UPDATE_GROUP_SUCCESS});
        } catch (error) {
            dispatch({type: GroupConst.UPDATE_GROUP_FAILURE, error});
        }
    };
}

export function deleteGroup(groupId) {
    return async dispatch => {
        dispatch({type: GroupConst.DELETE_GROUP_REQUEST});

        try {
            await api({url: `group/${groupId}/`, method: 'delete'});

            dispatch({type: GroupConst.DELETE_GROUP_SUCCESS});
        } catch (error) {
            dispatch({type: GroupConst.DELETE_GROUP_FAILURE, error});
        }
    };
}
