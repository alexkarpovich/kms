import './Conversation.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import CommentItem from './CommentItem';

class Conversation extends Component {
    constructor(props) {
        super(props);


    }

    static propTypes = {
        conversation: PropTypes.object.isRequired
    };

    render() {
        let {conversation} = this.props;

        return (
            <div className="conversation">
                { conversation.isFetching ?
                    <div className="overlay">
                        <i className="fa fa-spinner fa-spin"></i>
                    </div>
                : null }
                {conversation.data.map(item => <CommentItem key={item.id} nesting={0} comment={item} />)}
            </div>
        );
    }
}

export default injectIntl(Conversation);
