import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {routeActions} from 'react-router-redux';
import * as CourseActions from 'actions/course';
import * as LessonActions from 'actions/lesson';
import * as SkillActions from 'actions/skill';
import * as UserActions from 'actions/user';

class Course extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className="course-container">
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...CourseActions,
            ...LessonActions,
            ...SkillActions,
            ...UserActions
        }, dispatch)
    })
)(Course);
