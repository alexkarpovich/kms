import React, {Component, PropTypes} from 'react'

export default class Admin extends Component {
    render() {
        return (
            <div className="admin-container">
                {this.props.children}
            </div>
        );
    }
}
