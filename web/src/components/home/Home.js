import React, {Component} from 'react'
import {Link} from 'react-router'
import Breadcrumb from '../common/breadcrumb/Breadcrumb'

export default class Home extends Component {
    render() {
        return (
            <div>
                <section className="content-header">
                    <h1>
                        Home
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <div className="info-box">
                                <span className="info-box-icon bg-aqua">
                                    <i className="ion ion-ios-gear-outline"/>
                                </span>
                                <div className="info-box-content">
                                    <span className="info-box-text">
                                        CPU Traffic
                                    </span>
                                    <span className="info-box-number">
                                        90<small>%</small>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3 col-sm-6 col-xs-12">
                            <div className="info-box">
                                <span className="info-box-icon bg-red">
                                    <i className="fa fa-star-o"/>
                                </span>
                                <div className="info-box-content">
                                    <span className="info-box-text">Likes</span>
                                    <span className="info-box-number">93,139</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
