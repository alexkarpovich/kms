const PageModel = {
    isFetching: false,
    error: null,
    count: 0,
    hasNext: false,
    hasPrev: false,
    results: []
};

export default PageModel;
