from common.mixins import FilterModelViewSetMixin, CommentViewMixin
from .models import Course
from .serializers import CourseSerializer


class CourseViewSet(FilterModelViewSetMixin, CommentViewMixin):
    model = Course
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    search_fields = ('id', 'name', 'description', 'complexity', 'lessons__name', 'skills__name')
    ordering_fields = ('id', 'name', 'description', 'complexity', 'lessons', 'skills')
