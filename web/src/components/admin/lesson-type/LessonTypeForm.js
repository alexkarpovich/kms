import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import CommonComponent from 'components/common/CommonComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import LessonTypeModel from 'models/LessonType';

class LessonTypeForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {params, lessonType} = props;

        this.state = {
            lessonType: !!params.lessonTypeId ? lessonType.item : {...LessonTypeModel}
        };

        this.handleChange = ::this.handleChange;
        this.createLessonType = ::this.createLessonType;
        this.updateLessonType = ::this.updateLessonType;
    }

    static propTypes = {
        params: PropTypes.object.isRequired,
        actions: PropTypes.object.isRequired
    };

    componentDidMount() {
        let {params, actions} = this.props;

        if (params.lessonTypeId) {
            actions.loadLessonType(params.lessonTypeId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {params, lessonType} = newProps;

        if (this.state.lessonType !== lessonType.item && !!params.lessonTypeId) {
            this.state.lessonType = lessonType.item;
        }
    }

    updateLessonType() {
        let {actions, routeActions} = this.props;

        actions.updateLessonType(this.state.lessonType).then(() => {
            routeActions.push('/admin/lesson-type');
        });
    }

    createLessonType() {
        let {actions, routeActions} = this.props;

        actions.createLessonType(this.state.lessonType).then(() => {
            routeActions.push('/admin/lesson-type');
        });
    }

    handleChange(e) {
        this.state.lessonType[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    render() {
        let isEdit = !!this.props.params.lessonTypeId;
        let {lessonType} = this.state;
        let {formatMessage} = this.props.intl;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="lessontype.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title" />
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="name"
                                                onChange={this.handleChange}
                                                value={lessonType.name}
                                                placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="description">
                                                <FormattedMessage id="common.description.title" />
                                            </label>
                                            <textarea
                                                className="form-control input-sm"
                                                id="description"
                                                onChange={this.handleChange}
                                                value={lessonType.description}
                                                placeholder={formatMessage({id: 'common.description.title'})} >
                                            </textarea>
                                        </div>

                                        <button type="button" className={submitClasses} onClick={isEdit ? this.updateLessonType : this.createLessonType}>
                                            <FormattedMessage id="grid.action.submit.title.up" />
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(LessonTypeForm);
