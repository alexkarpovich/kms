const LessonModel = {
    id: null,
    name: '',
    author: {},
    type: {},
    content: ''
};

export default LessonModel;
