from rest_framework import serializers
from .models import Rating
from kmsuser.serializers import UserSerializer


class RatingSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Rating
        fields = ('id', 'rating', 'user')
