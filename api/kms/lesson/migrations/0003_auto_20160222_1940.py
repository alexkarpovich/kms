# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lesson', '0002_auto_20160209_1826'),
    ]

    operations = [
        migrations.AddField(
            model_name='lesson',
            name='author',
            field=models.ForeignKey(related_name='lesson', to=settings.AUTH_USER_MODEL, default=1),
        ),
    ]
