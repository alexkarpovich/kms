# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('path', models.FileField(upload_to=b'')),
                ('filename', models.CharField(default=b'', max_length=255, blank=True)),
                ('content_type', models.CharField(default=b'', max_length=30, blank=True)),
            ],
        ),
    ]
