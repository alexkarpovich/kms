import React, {Component} from 'react';
import { Link, History } from 'react-router';
import {injectIntl} from 'react-intl';

class BodyItem extends Component {
    constructor(props, context) {
        super(props, context);

        this.deleteEntity = ::this.deleteEntity;
    }

    static contextTypes = {
        columns: React.PropTypes.array.isRequired,
        rootUrl: React.PropTypes.string,
        deleteEntity: React.PropTypes.func
    };

    deleteEntity() {
        let {entity, intl: {formatMessage}} = this.props;

        alertify.confirm(formatMessage({id: 'confirmation.delete'}), isConfirmed => {
            isConfirmed && this.context.deleteEntity(entity.id)
        });
    }

    render() {

        let {columns, rootUrl} = this.context;
        let {entity} = this.props;

        return (

            <tr className="no-wrapping">
                { columns.map(column => {
                    let value = entity[column.key];

                    if (value === null) {
                        value = '-';
                    }
                    if (typeof value === 'boolean') {
                        value = value ? 'true' : 'false';
                    }

                    if (typeof value === 'object') {
                        value = value.name ? value.name : value.id;
                    }

                    return <td key={column.key}> {value} </td>;
                }) }
                <td className="actions">
                    <Link to={rootUrl + entity.id}
                          className="action-btn btn btn-default btn-xs" title="Edit">
                        <span className="fa fa-edit fa-lg"></span>
                    </Link>
                    <button onClick={this.deleteEntity}
                            className="action-btn btn btn-default btn-xs" title="Delete">
                        <span className="fa fa-trash fa-lg"></span>
                    </button>
                </td>
            </tr>
        );
    }
}

export default injectIntl(BodyItem);
