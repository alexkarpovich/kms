import keymirror from 'keymirror';

export const SkillConst = keymirror({
    LOAD_SKILL_REQUEST: null,
    LOAD_SKILL_SUCCESS: null,
    LOAD_SKILL_FAILURE: null,

    LOAD_SKILL_PAGE_REQUEST: null,
    LOAD_SKILL_PAGE_SUCCESS: null,
    LOAD_SKILL_PAGE_FAILURE: null,

    CREATE_SKILL_REQUEST: null,
    CREATE_SKILL_SUCCESS: null,
    CREATE_SKILL_FAILURE: null,

    UPDATE_SKILL_REQUEST: null,
    UPDATE_SKILL_SUCCESS: null,
    UPDATE_SKILL_FAILURE: null,

    DELETE_SKILL_REQUEST: null,
    DELETE_SKILL_SUCCESS: null,
    DELETE_SKILL_FAILURE: null,
});
