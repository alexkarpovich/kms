import './Selector.less';
import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import _remove from 'lodash/remove';


export default class Selector extends Component {
    constructor(props) {
        super(props);

        this.state = {
            from: 'rest',
            items: props.items,
            selected: props.selected
        };

        this.onChange = ::this.onChange;
        this.onFocus = ::this.onFocus;
    }

    static propTypes = {
        items: React.PropTypes.array,
        selected: React.PropTypes.array,
        onChange: React.PropTypes.func
    };

    componentWillReceiveProps(props) {
        this.state.items = props.items;
        this.state.selected = props.selected;
        this.setState(this.state);
    }

    onChange(e) {
        var restBox = ReactDOM.findDOMNode(this.refs.rest);
        var selectedBox = ReactDOM.findDOMNode(this.refs.selected);
        var isFromRest = this.state.from === 'rest';

        if (isFromRest) {
            let count = restBox.selectedOptions.length;

            for (let i = 0; i < count; i++) {
                this.state.selected.push(+restBox.selectedOptions[i].value);
            }
        } else {
            let count = selectedBox.selectedOptions.length;

            for (let i = 0; i < count; i++) {
                _remove(this.state.selected, item => {
                    return item === +selectedBox.selectedOptions[i].value
                });
            }
        }

        this.props.onChange(this.state.selected);
    }

    onFocus(e) {
        this.state.from = e.target.name;
        this.setState(this.state);
    }

    render() {
        var rest = [];
        var selected = [];
        var selectedIds = this.state.selected;

        this.state.items.forEach(item => {
            var current = selectedIds.indexOf(+item.value) !== -1 ? selected : rest;
            current.push(<option key={item.value} value={item.value}>{ item.label }</option>);
        });

        return (
            <div id={this.props.id} className="selector">
                <div className="rest-box">
                    <select
                        multiple={true}
                        name="rest"
                        ref="rest"
                        className="form-control"
                        onFocus={this.onFocus}
                        title="Rest Items">
                        {rest}
                    </select>
                </div>
                <div className="action">
                    <div onClick={this.onChange}>
                        <i className="fa fa-exchange fa-2x"></i>
                    </div>
                </div>
                <div className="selected-box">
                    <select
                        multiple={true}
                        name="selected"
                        ref="selected"
                        className="form-control"
                        onFocus={this.onFocus}
                        title="Selected Items">
                        {selected}
                    </select>
                </div>
            </div>
        );
    }
}
