'use strict';

export default class GridMapping {
    constructor(params = {}) {
        this.actions = params.actions || {};
        this.actionTypes = params.actionTypes || {};
        this.store = params.store || {};
    }
}
