from rest_framework import generics
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from common.mixins import FilterModelViewSetMixin
from django.contrib.auth.models import Group, Permission
from .models import User
from .serializers import UserSerializer, GroupSerializer, PermissionSerializer
from common.mixins import AllowAnyMixin
from lesson.models import Lesson
from course.models import Course
from lesson.serializers import LessonSerializer
from course.serializers import CourseSerializer
from skill.serializers import SkillSerializer


class CreateUserView(AllowAnyMixin, generics.CreateAPIView):
    model = User
    serializer_class = UserSerializer


class UserViewSet(AllowAnyMixin, FilterModelViewSetMixin):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    search_fields = ('id', 'username', 'email', 'first_name', 'last_name', 'phone', 'skype', 'skills__name',
                     'skills__description', 'groups__name',)
    ordering_fields = ('id', 'username', 'email', 'first_name', 'last_name', 'phone', 'skype')

    @detail_route(methods=['get'])
    def lessons(self, request, pk):
        lessons = Lesson.objects.filter(author_id=pk)

        return Response(LessonSerializer(lessons, many=True, context={'request': request}).data)

    @detail_route(methods=['get'])
    def courses(self, request, pk):
        courses = Course.objects.filter(author_id=pk)

        return Response(CourseSerializer(courses, many=True, context={'request': request}).data)

    @detail_route(methods=['get', 'post', 'delete'], url_path='skills(?:/(?P<user_skill_id>[0-9]+))?')
    def skills(self, request, pk=None, user_skill_id=None):
        user = User.objects.get(pk=pk)

        if request.method == 'GET':
            user_skills = user.get_skills()
            response = []
            for user_skill in user_skills:
                response.append({
                    'id': user_skill.pk,
                    'skill': SkillSerializer(user_skill.skill, context={'request': request}).data,
                    'level': user_skill.level
                })
        elif request.method == 'POST':
            response = SkillSerializer(user.add_skill(request), context={'request': request}).data
        elif request.method == 'DELETE':
            response = user.delete_skill(user_skill_id)

        return Response(response)


class GroupViewSet(FilterModelViewSetMixin):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    search_fields = ('id', 'name')
    ordering_fields = ('id', 'name')


class PermissionViewSet(FilterModelViewSetMixin):
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer
    search_fields = ('id', 'name', 'codename', 'content_type')
    ordering_fields = ('id', 'name', 'codename', 'content_type')
