import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {routeActions} from 'react-router-redux';
import * as SkillCategoryActions from 'actions/skill-category';

class SkillCategory extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className="skill-category-container">
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...SkillCategoryActions
        }, dispatch)
    })
)(SkillCategory);
