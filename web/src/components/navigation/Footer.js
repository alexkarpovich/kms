import React, {Component, PropTypes} from 'react'
import LocaleSwitcher from 'components/locale-switcher/LocaleSwitcher'

export default class Footer extends Component {
    render() {
        let currentYear = new Date().getFullYear();
        let {i18n, actions} = this.props;

        return (
            <footer className="main-footer">
                <div className="pull-right hidden-xs">
                    <LocaleSwitcher i18n={i18n} changeLocale={actions.changeLocale}/>
                </div>
                Copyright © <strong>iTechArt </strong> {currentYear}
            </footer>
        );
    }
}
