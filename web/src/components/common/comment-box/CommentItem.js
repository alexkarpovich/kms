import './CommentItem.less';
import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {FormattedMessage, injectIntl} from 'react-intl';
import PathHelper from 'helpers/path';
import Markdown from 'react-markdown';
import CommentForm from './CommentForm';
import Conversation from './Conversation';

class CommentItem extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            showReplyForm: false
        };

        this.openReplyForm = ::this.openReplyForm;
        this.cancelReply = ::this.cancelReply;
    }

    static propTypes = {
        nesting: PropTypes.number.isRequired,
        comment: PropTypes.object.isRequired
    };

    static contextTypes = {
        authUser: PropTypes.object.isRequired
    };

    openReplyForm() {
        this.state.showReplyForm = true;
        this.setState(this.state);
    }

    cancelReply() {
        this.state.showReplyForm = false;
        this.setState(this.state);
    }

    render() {
        let {nesting, comment:{author, content, children, id}} = this.props;
        let {showReplyForm} = this.state;

        let avatarSrc = author.avatar && author.avatar.path ?
            PathHelper.image(author.avatar.path, 64, 64) : '/images/avatar64x64.png';

        return (
            <li className="comment-item">
                <span className="user-avatar">
                    <img src={avatarSrc} />
                </span>
                <div className="main">
                    <div className="user-name">
                        <Link to={`/user/${author.id}`}>{`${author.firstName} ${author.lastName}`}</Link>
                    </div>
                    <div className="text">
                        <Markdown source={content} />
                    </div>
                    <div className="actions">
                        {nesting === 0 ?
                            <span onClick={this.openReplyForm}>
                                <FormattedMessage id="common.comment.reply" />
                            </span>
                        : null}
                    </div>
                    <ul className="replies" ref="replies">
                        { children && children.length ?
                            children.map(child => <CommentItem key={child.id} comment={child} nesting={1} />)
                        : null }
                        {showReplyForm ?
                            <CommentForm
                                parentId={id}
                                onCancel={this.cancelReply}
                                focus={true}
                            />
                        : null}
                    </ul>

                </div>
            </li>
        );
    }
}

export default injectIntl(CommentItem);
