const EventModel = {
    id: null,
    name: '',
    description: '',
    type: {},
    start: '',
    end: ''
};

export default EventModel;
