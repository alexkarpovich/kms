import jwtDecode from 'jwt-decode'
import {AuthConst} from 'constants/index'

const initialAuthState = {
    authUser: null,
    isAuthenticated: false
};

export default function auth(state = initialAuthState, action) {
    switch (action.type) {
        case AuthConst.LOGIN_USER_SUCCESS:
            return Object.assign({}, state, {
                isAuthenticated: true
            });

        case AuthConst.LOGIN_USER_FAILURE:
            return Object.assign({}, state, {
                authUser: null,
                isAuthenticated: false
            });

        case AuthConst.REGISTER_USER_FAILURE:
            return Object.assign({}, state, {
                authUser: null,
                isAuthenticated: false,
                errors: action.errors
            });

        case AuthConst.GET_AUTH_USER_SUCCESS:
            return Object.assign({}, state, {
                authUser: action.authUser,
                isAuthenticated: true
            });

        case AuthConst.GET_AUTH_USER_FAILURE:
            return Object.assign({}, state, {
                authUser: null,
                isAuthenticated: false
            });

        case AuthConst.SET_AUTH_USER:
            return Object.assign({}, state, {
                authUser: action.authUser,
                isAuthenticated: true
            });

        case AuthConst.LOGOUT_USER:
            return Object.assign({}, state, {
                authUser: null,
                isAuthenticated: false
            });

        default:
            return state
    }
}
