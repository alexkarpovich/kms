const UserModel = {
    id: null,
    username: '',
    email: '',
    groups: [],
    password: '',
    confirmPassword: '',
    firstName: '',
    lastName: '',
    phone: '',
    skype: '',
    skills: [],
    avatar: undefined
};

export default UserModel;
