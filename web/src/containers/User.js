import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {routeActions} from 'react-router-redux';
import * as UserActions from 'actions/user';
import * as GroupActions from 'actions/group';
import * as SkillActions from 'actions/skill';

class User extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className="user-container">
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...UserActions,
            ...GroupActions,
            ...SkillActions
        }, dispatch)
    })
)(User)
