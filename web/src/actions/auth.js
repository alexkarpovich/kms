import jwtDecode from 'jwt-decode'
import {AuthConst} from 'constants/auth'
import api from '../helpers/api'

// LOGIN_USER action creators
export function loginUser(credentials) {
    return async dispatch => {
        dispatch({type: AuthConst.LOGIN_USER_REQUEST});

        try {
            let data = await api({url: 'auth/signin/', method: 'post', data: credentials});

            setAuthToken(data.token);

            dispatch({type: AuthConst.LOGIN_USER_SUCCESS});
            dispatch(getAuthUser(data.token));
        } catch (error) {
            removeAuthToken();

            dispatch(requestError(error, AuthConst.LOGIN_USER));
        }
    }
}

// GET_AUTH_USER action creators
export function getAuthUser(token) {
    return async dispatch => {
        let authUser = jwtDecode(token);

        dispatch({type: AuthConst.GET_AUTH_USER_REQUEST});

        try {
            let data = await api({url: `user/${authUser.user_id}/`});

            dispatch({type: AuthConst.GET_AUTH_USER_SUCCESS, authUser: data});
        } catch (error) {
            dispatch(requestError(error, AuthConst.GET_AUTH_USER));
        }
    }
}

// REGISTER_USER action creators
export function registerUser(credentials) {
    return async dispatch => {
        dispatch({type: AuthConst.REGISTER_USER_REQUEST});

        try {
            let data = await api({url: 'register/', method: 'post', data: credentials});

            dispatch({type: AuthConst.REGISTER_USER_SUCCESS});

            if (data.user && data.token) {
                setAuthToken(data.token);

                dispatch({type: AuthConst.SET_AUTH_USER, authUser: data.user});
            }
        } catch (error) {
            dispatch(requestError(error.errors, AuthConst.REGISTER_USER));
        }
    }
}

export function logoutUser() {
    removeAuthToken();

    return {
        type: AuthConst.LOGOUT_USER
    }
}

function setAuthToken(token) {
    localStorage.setItem('token', token);
}

function removeAuthToken() {
    localStorage.removeItem('token');
}

function requestError(errors, type) {
    return {
        type: type + '_FAILURE',
        errors
    }
}
