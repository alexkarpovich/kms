import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {routeActions} from 'react-router-redux';
import * as LessonActions from 'actions/lesson';
import * as LessonTypeActions from 'actions/lesson-type';
import * as UserActions from 'actions/user';

class Lesson extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className="lesson-container">
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...LessonActions,
            ...LessonTypeActions,
            ...UserActions
        }, dispatch)
    })
)(Lesson);
