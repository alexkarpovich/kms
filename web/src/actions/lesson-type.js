import {LessonTypeConst} from 'constants';
import api from 'helpers/api';

export function loadLessonTypePage(params) {
    return async dispatch => {
        dispatch({type: LessonTypeConst.LOAD_LESSON_TYPE_PAGE_REQUEST});

        try {
            let data = await api({url: 'lesson-type/', query: params});

            dispatch({type: LessonTypeConst.LOAD_LESSON_TYPE_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: LessonTypeConst.LOAD_LESSON_TYPE_PAGE_FAILURE, error});
        }
    }
}

export function loadLessonType(lessonTypeId) {
    return async dispatch => {
        dispatch({type: LessonTypeConst.LOAD_LESSON_TYPE_REQUEST});

        try {
            let data = await api({url: `lesson-type/${lessonTypeId}/`});

            dispatch({type: LessonTypeConst.LOAD_LESSON_TYPE_SUCCESS, data});
        } catch (error) {
            dispatch({type: LessonTypeConst.LOAD_LESSON_TYPE_FAILURE, error});
        }
    };
}

export function createLessonType(lessonType) {
    return async dispatch => {
        dispatch({type: LessonTypeConst.CREATE_LESSON_TYPE_REQUEST});

        try {
            await api({url: `lesson-type/`, method: 'post', data: lessonType});

            dispatch({type: LessonTypeConst.CREATE_LESSON_TYPE_SUCCESS});
        } catch (error) {
            dispatch({type: LessonTypeConst.CREATE_LESSON_TYPE_FAILURE, error});
        }
    };
}

export function updateLessonType(lessonType) {
    return async dispatch => {
        dispatch({type: LessonTypeConst.UPDATE_LESSON_TYPE_REQUEST});

        try {
            await api({url: `lesson-type/${lessonType.id}/`, method: 'put', data: lessonType});

            dispatch({type: LessonTypeConst.UPDATE_LESSON_TYPE_SUCCESS});
        } catch (error) {
            dispatch({type: LessonTypeConst.UPDATE_LESSON_TYPE_FAILURE, error});
        }
    };
}

export function deleteLessonType(lessonTypeId) {
    return async dispatch => {
        dispatch({type: LessonTypeConst.DELETE_LESSON_TYPE_REQUEST});

        try {
            await api({url: `lesson-type/${lessonTypeId}/`, method: 'delete'});

            dispatch({type: LessonTypeConst.DELETE_LESSON_TYPE_SUCCESS});
        } catch (error) {
            dispatch({type: LessonTypeConst.DELETE_LESSON_TYPE_FAILURE, error});
        }
    };
}
