from rest_framework import filters
from common.mixins import AllowAnyModelViewSetMixin
from .models import Skill, SkillCategory
from .serializers import SkillSerializer, SkillCategorySerializer


class SkillCategoryViewSet(AllowAnyModelViewSetMixin):
    queryset = SkillCategory.objects.all()
    serializer_class = SkillCategorySerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('id', 'name', 'description', 'parent__id', 'enabled')
    ordering_fields = ('id', 'name', 'description', 'parent', 'enabled')


class SkillViewSet(AllowAnyModelViewSetMixin):
    queryset = Skill.objects.all()
    serializer_class = SkillSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )
    search_fields = ('id', 'name', 'description', 'category__name', 'category__id', 'enabled')
    ordering_fields = ('id', 'name', 'description', 'category', 'enabled')
