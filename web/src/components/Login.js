import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {routeActions} from 'react-router-redux'
import * as AuthActions from 'actions/auth'

export default class Login extends Component {
    constructor(props, context) {
        super(props, context);

        this.actions = this.props.actions;

        this.state = {
            username: '',
            password: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.login = this.login.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.authUser) {
            this.props.routeActions.push('/');
        }
    }

    handleChange(e) {
        this.state[e.target.name] = e.target.value;

        this.setState(this.state);
    }

    login(event) {
        event.preventDefault();

        this.actions.loginUser(this.state);
    }

    render() {
        return (
            <div>
                <section className="content">
                    <div className="row">
                        <div
                            className="login col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    Login
                                </div>
                                <div className="panel-body">
                                    <form>
                                        <div className="form-group">
                                            <label>Username</label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                name="username"
                                                onChange={this.handleChange}
                                                value={this.state.username}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label>Password</label>
                                            <input
                                                type="password"
                                                className="form-control input-sm"
                                                name="password"
                                                onChange={this.handleChange}
                                                value={this.state.password}
                                            />
                                        </div>

                                        <button
                                            type="submit"
                                            className="btn btn-sm btn-default"
                                            onClick={this.login}
                                        >
                                            Login
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...AuthActions
        }, dispatch)
    })
)(Login)
