import {EventTypeConst} from 'constants';
import api from 'helpers/api';

export function loadEventTypePage(params) {
    return async dispatch => {
        dispatch({type: EventTypeConst.LOAD_EVENT_TYPE_PAGE_REQUEST});

        try {
            let data = await api({url: 'event-type/', query: params});

            dispatch({type: EventTypeConst.LOAD_EVENT_TYPE_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: EventTypeConst.LOAD_EVENT_TYPE_PAGE_FAILURE, error});
        }
    }
}

export function loadEventType(eventTypeId) {
    return async dispatch => {
        dispatch({type: EventTypeConst.LOAD_EVENT_TYPE_REQUEST});

        try {
            let data = await api({url: `event-type/${eventTypeId}/`});

            dispatch({type: EventTypeConst.LOAD_EVENT_TYPE_SUCCESS, data});
        } catch (error) {
            dispatch({type: EventTypeConst.LOAD_EVENT_TYPE_FAILURE, error});
        }
    };
}

export function createEventType(eventType) {
    return async dispatch => {
        dispatch({type: EventTypeConst.CREATE_EVENT_TYPE_REQUEST});

        try {
            await api({url: `event-type/`, method: 'post', data: eventType});

            dispatch({type: EventTypeConst.CREATE_EVENT_TYPE_SUCCESS});
        } catch (error) {
            dispatch({type: EventTypeConst.CREATE_EVENT_TYPE_FAILURE, error});
        }
    };
}

export function updateEventType(eventType) {
    return async dispatch => {
        dispatch({type: EventTypeConst.UPDATE_EVENT_TYPE_REQUEST});

        try {
            await api({url: `event-type/${eventType.id}/`, method: 'put', data: eventType});

            dispatch({type: EventTypeConst.UPDATE_EVENT_TYPE_SUCCESS});
        } catch (error) {
            dispatch({type: EventTypeConst.UPDATE_EVENT_TYPE_FAILURE, error});
        }
    };
}

export function deleteEventType(eventTypeId) {
    return async dispatch => {
        dispatch({type: EventTypeConst.DELETE_EVENT_TYPE_REQUEST});

        try {
            await api({url: `event-type/${eventTypeId}/`, method: 'delete'});

            dispatch({type: EventTypeConst.DELETE_EVENT_TYPE_SUCCESS});
        } catch (error) {
            dispatch({type: EventTypeConst.DELETE_EVENT_TYPE_FAILURE, error});
        }
    };
}
