import './styles.less';
import 'adminlte/dist/js/app';
import 'bootstrap/dist/js/bootstrap';
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import Root from 'containers/Root';
import configureStore from './store/store';
import * as AuthActions from 'actions/auth';

const store = configureStore();

/* TODO: Make it better */
function layoutFixes() {
    ReactDOM.render(<Root store={store} />, document.getElementById('root'));
    window.dispatchEvent(new Event('resize'));
    setTimeout(() => $.AdminLTE.tree('.sidebar'), 1000);
}

/* TODO: Make it better */
let token = localStorage.getItem('token') || null;
if (token) {
    store.dispatch(AuthActions.getAuthUser(token)).then(() => layoutFixes());
} else {
    layoutFixes();
}

