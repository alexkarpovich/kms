export default class PathHelper {
    static image(url, width, height) {
        return url ? url.replace('images/', `images/${width}x${height}/`) /*+ '?' + new Date().getSeconds()*/ : null;
    }
}
