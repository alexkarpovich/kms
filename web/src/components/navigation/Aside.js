import React, {Component} from 'react';
import {Link} from 'react-router'
import {FormattedMessage} from 'react-intl';
import menuItems from 'configs/menu';
import MenuItem from './aside/MenuItem';

export default class Aside extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            menuItems: [...menuItems]
        };
    }

    render() {
        let {menuItems} = this.state;

        menuItems = menuItems.map((item, key) => {
            return <MenuItem key={key} item={item} {...this.props} />;
        });

        return (
            <aside className="main-sidebar">
                <section className="sidebar">
                    <ul className="sidebar-menu">
                        { menuItems }
                    </ul>
                </section>
            </aside>
        );
    }
}
