import React from 'react';
import {Route, IndexRoute} from 'react-router';
// Containers
import AppContainer from 'containers/App';
import AdminContainer from 'containers/admin/Admin';
import AdminEventContainer from 'containers/admin/Event';
import AdminEventTypeContainer from 'containers/admin/EventType';
import AdminLessonContainer from 'containers/admin/Lesson';
import AdminLessonTypeContainer from 'containers/admin/LessonType';
import AdminPermissionContainer from 'containers/admin/Permission';
import AdminUserContainer from 'containers/admin/User';
import AdminGroupContainer from 'containers/admin/Group';
import AdminCourseContainer from 'containers/admin/Course';
import AdminSkillContainer from 'containers/admin/Skill';
import AdminSkillCategoryContainer from 'containers/admin/SkillCategory';
import ProfileContainer from 'containers/Profile';
import CalendarContainer from 'containers/Calendar';
import CourseContainer from 'containers/Course';
import LessonContainer from 'containers/Lesson';
import UserContainer from 'containers/User';

// Components
import Login from 'components/Login';
import Register from 'components/Register';
import Home from 'components/home/Home';
import NotFound from 'components/not-found/NotFound';
import AdminEventForm from 'components/admin/event/EventForm';
import AdminEventList from 'components/admin/event/EventList';
import AdminEventTypeForm from 'components/admin/event-type/EventTypeForm';
import AdminEventTypeList from 'components/admin/event-type/EventTypeList';
import AdminLessonForm from 'components/admin/lesson/LessonForm';
import AdminLessonList from 'components/admin/lesson/LessonList';
import AdminLessonTypeForm from 'components/admin/lesson-type/LessonTypeForm';
import AdminLessonTypeList from 'components/admin/lesson-type/LessonTypeList';
import AdminPermissionForm from 'components/admin/permission/PermissionForm';
import AdminPermissionList from 'components/admin/permission/PermissionList';
import AdminUserForm from 'components/admin/user/UserForm';
import AdminUserList from 'components/admin/user/UserList';
import AdminGroupForm from 'components/admin/group/GroupForm';
import AdminGroupList from 'components/admin/group/GroupList';
import AdminCourseForm from 'components/admin/course/CourseForm';
import AdminCourseList from 'components/admin/course/CourseList';
import AdminSkillForm from 'components/admin/skill/SkillForm';
import AdminSkillList from 'components/admin/skill/SkillList';
import AdminSkillCategoryForm from 'components/admin/skill-category/SkillCategoryForm';
import AdminSkillCategoryList from 'components/admin/skill-category/SkillCategoryList';
import Profile from 'components/profile/Profile';
import Calendar from 'components/calendar/Calendar';
import CourseList from 'components/course/CourseList';
import CourseView from 'components/course/CourseView';
import CourseForm from 'components/course/CourseForm';
import LessonList from 'components/lesson/LessonList';
import LessonForm from 'components/lesson/LessonForm';
import LessonView from 'components/lesson/LessonView';
import UserList from 'components/user/UserList';
import UserView from 'components/user/UserView';


export default (store) => {
    function isAuthenticated(nextState, replaceState) {
        let {auth} = store.getState();

        if (!auth.isAuthenticated) {
            replaceState({ nextPathname: nextState.location.pathname }, '/signin')
        }
    }

    return (
        <Route path="/" component={AppContainer}>
            <IndexRoute component={Home} />
            <Route path="login" component={Login} />
            <Route path="register" component={Register} />

            <Route path="admin" component={AdminContainer} onEnter={isAuthenticated}>
                <Route path="event" component={AdminEventContainer}>
                    <IndexRoute component={AdminEventList} />
                    <Route path="create" component={AdminEventForm} />
                    <Route path=":eventId" component={AdminEventForm} />
                </Route>

                <Route path="event-type" component={AdminEventTypeContainer}>
                    <IndexRoute component={AdminEventTypeList} />
                    <Route path="create" component={AdminEventTypeForm} />
                    <Route path=":eventTypeId" component={AdminEventTypeForm} />
                </Route>

                <Route path="lesson" component={AdminLessonContainer}>
                    <IndexRoute component={AdminLessonList} />
                    <Route path="create" component={AdminLessonForm} />
                    <Route path=":lessonId" component={AdminLessonForm} />
                </Route>

                <Route path="lesson-type" component={AdminLessonTypeContainer}>
                    <IndexRoute component={AdminLessonTypeList} />
                    <Route path="create" component={AdminLessonTypeForm} />
                    <Route path=":lessonTypeId" component={AdminLessonTypeForm} />
                </Route>

                <Route path="permission" component={AdminPermissionContainer}>
                    <IndexRoute component={AdminPermissionList} />
                    <Route path="create" component={AdminPermissionForm} />
                    <Route path=":permissionId" component={AdminPermissionForm} />
                </Route>

                <Route path="user" component={AdminUserContainer}>
                    <IndexRoute component={AdminUserList} />
                    <Route path="create" component={AdminUserForm} />
                    <Route path=":userId" component={AdminUserForm} />
                </Route>

                <Route path="group" component={AdminGroupContainer}>
                    <IndexRoute component={AdminGroupList} />
                    <Route path="create" component={AdminGroupForm} />
                    <Route path=":groupId" component={AdminGroupForm} />
                </Route>

                <Route path="course" component={AdminCourseContainer}>
                    <IndexRoute component={AdminCourseList} />
                    <Route path="create" component={AdminCourseForm} />
                    <Route path=":courseId" component={AdminCourseForm} />
                </Route>

                <Route path="skill" component={AdminSkillContainer}>
                    <IndexRoute component={AdminSkillList} />
                    <Route path="create" component={AdminSkillForm} />
                    <Route path=":skillId" component={AdminSkillForm} />
                </Route>

                <Route path="skill-category" component={AdminSkillCategoryContainer}>
                    <IndexRoute component={AdminSkillCategoryList} />
                    <Route path="create" component={AdminSkillCategoryForm} />
                    <Route path=":skillCategoryId" component={AdminSkillCategoryForm} />
                </Route>
            </Route>

            <Route path="profile" component={ProfileContainer} onEnter={isAuthenticated}>
                <IndexRoute component={Profile} />
            </Route>

            <Route path="calendar" component={CalendarContainer} onEnter={isAuthenticated}>
                <IndexRoute component={Calendar} />
            </Route>

            <Route path="course" component={CourseContainer} onEnter={isAuthenticated}>
                <IndexRoute component={CourseList} />
                <Route path="create" component={CourseForm} />
                <Route path=":courseId" component={CourseView} />
                <Route path=":courseId/edit" component={CourseForm} />
            </Route>

            <Route path="lesson" component={LessonContainer} onEnter={isAuthenticated}>
                <IndexRoute component={LessonList} />
                <Route path="create" component={LessonForm} />
                <Route path=":lessonId" component={LessonView} />
                <Route path=":lessonId/edit" component={LessonForm} />
            </Route>

            <Route path="user" component={UserContainer} onEnter={isAuthenticated}>
                <IndexRoute component={UserList} />
                <Route path=":userId" component={UserView} />
            </Route>

            <Route path="*" component={NotFound} />
        </Route>
    )
}
