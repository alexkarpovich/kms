from aiohttp import web
from user.routes import UserRouterConnector

app = web.Application()
app = UserRouterConnector().add_routes(app)
web.run_app(app)
