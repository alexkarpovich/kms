import functools
from rest_framework.permissions import AllowAny
from rest_framework import viewsets
from rest_framework import filters
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from django.contrib.contenttypes.models import ContentType
from common.rating.models import Rating
from common.comment.models import Comment
from common.comment.serializers import CommentSerializer
from kmsuser.serializers import UserSerializer
from kmsuser.models import User


class AllowAnyMixin(object):
    permission_classes = (AllowAny, )
    authentication_classes = ()


class AllowAnyModelViewSetMixin(AllowAnyMixin, viewsets.ModelViewSet):
    pass


class FilterMixin(object):
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, )


class FilterModelViewSetMixin(FilterMixin, viewsets.ModelViewSet):
    pass


class ContentTypeMixin(object):
    def get_content_type(self):
        return ContentType.objects.get_for_model(self)


class RatingModelMixin(ContentTypeMixin):
    def get_rating(self):
        ratings = Rating.objects.filter(content_type=self.get_content_type(), object_id=self.id)
        rating_count = len(ratings)
        rating_sum = functools.reduce(lambda res, item: res + item.rating, ratings, 0)

        return round(rating_sum / rating_count) if rating_count > 0 else 0

    def get_voted_users(self):
        ratings = Rating.objects.filter(content_type=self.get_content_type(), object_id=self.id)
        voted_users = map(lambda x: UserSerializer(x.user).data, ratings)

        return voted_users


class CommentModelMixin(ContentTypeMixin):
    def get_comments(self, request):
        comments = Comment.objects.filter(content_type=self.get_content_type(), object_id=self.id)
        return CommentSerializer(comments, many=True, context={'request': request}).data

    def add_comment(self, request):
        parent_id = request.data.get('parent_id', None)
        author_id = request.data.get('author_id', None)
        content = request.data.get('content', None)

        if author_id is not None:
            author = User.objects.get(pk=author_id)
        else:
            raise Exception('author_id is required')

        comment = Comment.objects.create(
            parent_id=parent_id,
            author=author,
            content=content,
            content_type=self.get_content_type(),
            object_id=self.id
        )

        return CommentSerializer(comment, context={'request': request}).data


class CommentViewMixin(object):
    @detail_route(methods=['get', 'post'])
    def comments(self, request, pk=None):
        if request.method == 'GET':
            response = self.model.objects.get(pk=pk).get_comments(request)

        elif request.method == 'POST':
            response = self.model.objects.get(pk=pk).add_comment(request)

        return Response(response)
