from common.mixins import FilterModelViewSetMixin
from .models import Comment
from .serializers import CommentSerializer


class CommentViewSet(FilterModelViewSetMixin):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    search_fields = ('id', 'parent__content', 'user__username', 'user__first_name', 'user__last_name', 'content')
    ordering_fields = ('id', 'content')
