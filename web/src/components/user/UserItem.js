import './UserItem.less';
import React, {Component, PropTypes} from 'react';
import PathHelper from 'helpers/path';

export default class UserItem extends Component {
    constructor(props) {
        super(props);

        this.viewUser = ::this.viewUser;
    }

    static propTypes = {
        user: PropTypes.object.isRequired,
        loadUsers: PropTypes.func.isRequired,
        routeActions: PropTypes.object.isRequired
    };

    viewUser() {
        let {routeActions, user} = this.props;

        routeActions.push(`/user/${user.id}`);
    }

    render() {
        let {user} = this.props;
        let avatarSrc = !!user.avatar && user.avatar.path ?
            PathHelper.image(user.avatar.path, 64, 64) : '/images/avatar64x64.png';

        return (
            <div className="user-item">
                <div className="avatar" onClick={this.viewUser}>
                    <img src={avatarSrc} />
                </div>
                <div className="">
                    <span className="full-name">
                        {(user.firstName || user.lastName) ?
                            `${user.firstName} ${user.lastName}` : user.username}
                    </span>
                </div>
            </div>
        );
    }
}
