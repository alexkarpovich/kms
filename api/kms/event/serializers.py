from rest_framework import serializers
from .models import Event,EventType


class EventTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventType
        fields = ('id', 'name', 'description')


class EventSerializer(serializers.ModelSerializer):
    type = EventTypeSerializer(read_only=True)
    type_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = Event
        fields = ('id', 'name', 'description', 'type', 'type_id', 'start', 'end')
