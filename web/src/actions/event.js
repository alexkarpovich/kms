import {EventConst} from 'constants';
import api from 'helpers/api';

export function loadEventPage(params) {
    return async dispatch => {
        dispatch({type: EventConst.LOAD_EVENT_PAGE_REQUEST});

        try {
            let data = await api({url: 'event/', query: params});

            dispatch({type: EventConst.LOAD_EVENT_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: EventConst.LOAD_EVENT_PAGE_FAILURE, error});
        }
    }
}

export function loadEvent(eventId) {
    return async dispatch => {
        dispatch({type: EventConst.LOAD_EVENT_REQUEST});

        try {
            let data = await api({url: `event/${eventId}/`});

            dispatch({type: EventConst.LOAD_EVENT_SUCCESS, data});
        } catch (error) {
            dispatch({type: EventConst.LOAD_EVENT_FAILURE, error});
        }
    };
}

export function createEvent(event) {
    return async dispatch => {
        dispatch({type: EventConst.CREATE_EVENT_REQUEST});

        try {
            await api({url: `event/`, method: 'post', data: event});

            dispatch({type: EventConst.CREATE_EVENT_SUCCESS});
        } catch (error) {
            dispatch({type: EventConst.CREATE_EVENT_FAILURE, error});
        }
    };
}

export function updateEvent(event) {
    return async dispatch => {
        dispatch({type: EventConst.UPDATE_EVENT_REQUEST});

        try {
            await api({url: `event/${event.id}/`, method: 'put', data: event});

            dispatch({type: EventConst.UPDATE_EVENT_SUCCESS});
        } catch (error) {
            dispatch({type: EventConst.UPDATE_EVENT_FAILURE, error});
        }
    };
}

export function deleteEvent(eventId) {
    return async dispatch => {
        dispatch({type: EventConst.DELETE_EVENT_REQUEST});

        try {
            await api({url: `event/${eventId}/`, method: 'delete'});

            dispatch({type: EventConst.DELETE_EVENT_SUCCESS});
        } catch (error) {
            dispatch({type: EventConst.DELETE_EVENT_FAILURE, error});
        }
    };
}
