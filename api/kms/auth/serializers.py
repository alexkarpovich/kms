from rest_framework import serializers
from kmsuser.models import User
from kmsuser.serializers import UserSerializer


class RegisterSerializer(UserSerializer):
    confirm_password = serializers.CharField(allow_blank=False, required=False)
    email = serializers.EmailField(allow_blank=False)

    class Meta:
        model = User

    def validate(self, data):
        if len(data['password']) < 8:
            raise serializers.ValidationError({'password' : 'Password is too short (minimum is 8 characters).'})
        elif data['password'] != data['confirm_password']:
            raise serializers.ValidationError({'confirmPassword' : 'Password confirmation doesn\'t match Password.'})

        return data
