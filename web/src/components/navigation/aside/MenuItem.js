import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router'
import {FormattedMessage} from 'react-intl';
import classnames from 'classnames';

function isAvailableItem(item, auth) {
    return item.isAdmin && auth.isAuthenticated && auth.authUser && auth.authUser.isSuperuser ||
        !item.isAdmin &&!item.isPublic && auth.isAuthenticated ||
        item.isPublic;
}

export default class MenuItem extends Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        parent: ''
    };

    static propTypes = {
        auth: PropTypes.object.isRequired,
        item: PropTypes.object.isRequired,
        parent: PropTypes.string.isRequired
    };

    render() {
        let {auth, item, parent} = this.props;
        let isActive = location.pathname.startsWith(item.link);
        let itemClasses = classnames({
            'treeview': true,
            'active': isActive
        });

        if (!isAvailableItem(item, auth)) {
            return null;
        }

        if (item.header) {
            return (
                <li className="treeview header">
                    <FormattedMessage id={item.title} />
                </li>
            );
        }

        if (!item.children) {
            return (
                <li className={itemClasses}>
                    <Link to={parent + item.link}>
                        <i className={item.icon}/>
                        <FormattedMessage id={item.title} />
                    </Link>
                </li>
            );
        }

        return (
            <li className={itemClasses}>
                <Link to={parent + item.link}>
                    <i className={item.icon}/>
                    <FormattedMessage id={item.title} />
                </Link>
                <ul className="treeview-menu">
                    {item.children.map((child, key) => {
                        return <MenuItem key={key} item={child} parent={item.link} auth={auth}/>;
                    })}
                </ul>
            </li>
        );
    }
}
