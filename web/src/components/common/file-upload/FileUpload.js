import React, {Component, PropTypes} from 'react';
import ModalView from './ModalView';
import {FormattedMessage} from 'react-intl';
import FileModel from 'models/File';

export default class FileUpload extends Component {
    constructor(props, context) {
        super(props, context);

        let {file} = props;

        this.state = {
            fileData: file.item,
            coords: {}
        };

        this.getFileData = ::this.getFileData;
        this.setFileData = ::this.setFileData;
        this.getCoords = ::this.getCoords;
        this.setCoords = ::this.setCoords;
        this.clear = ::this.clear;
        this.cropImage = ::this.cropImage;
        this.uploadFile = ::this.uploadFile;
    }

    static propTypes = {
        id: PropTypes.string,
        file: PropTypes.object.isRequired,
        uploadAs: PropTypes.oneOf(['file', 'image']).isRequired,
        onChange: PropTypes.func
    };

    static defaultProps = {
        id: 'file-upload',
        uploadAs: 'file'
    };

    static childContextTypes = {
        uploadAs: PropTypes.oneOf(['file', 'image']),
        setFileData: PropTypes.func,
        setCoords: PropTypes.func,
        getFileData: PropTypes.func,
        getCoords: PropTypes.func,
        cropImage: PropTypes.func,
        uploadFile: PropTypes.func
    };

    getChildContext() {
        let {uploadAs} = this.props;

        return {
            uploadAs: uploadAs,
            setFileData: this.setFileData,
            setCoords: this.setCoords,
            getFileData: this.getFileData,
            cropImage: this.cropImage,
            uploadFile: this.uploadFile
        };
    }

    componentWillReceiveProps(newProps) {
        if (this.state.fileData !== newProps.file.item) {
            this.state.fileData = newProps.file.item;
        }
    }

    getFileData() {
        return this.state.fileData;
    }

    setFileData(fileData) {
        this.state.fileData = fileData;
        this.setState(this.state);
        this.props.onChange(fileData);
    }

    getCoords() {
        return this.state.coords;
    }

    setCoords(coords) {
        this.state.coords = coords;
        this.setState(this.state);
    }

    clear() {
        let {onChange} = this.props;

        this.state.fileData = FileModel;
        this.state.coords = {};
        this.setState(this.state);
        onChange && onChange(this.state.fileData);
    }

    cropImage() {
        let {fileData, coords} = this.state;
        let {actions} = this.props;

        actions.cropImage(fileData.id, {coords}).then(() => {
            this.imageCropped();
        });
    }

    uploadFile(filename, file) {
        let {actions, uploadAs} = this.props;

        actions.uploadFile(filename, file, uploadAs);
    }

    imageCropped() {
        var {onChange} = this.props;

        $('#modal-view').modal('toggle');
        onChange && onChange(this.state.fileData);
    }

    render() {
        var {id} = this.props;

        return (
            <div id={id} className="file-upload">
                <div className="actions">
                    <button
                        type="button"
                        className="preview btn btn-default btn-sm"
                        data-toggle="modal"
                        data-target="#modal-view">
                        <i className="fa fa-upload fa-md"></i>
                        <FormattedMessage id="file.action.upload" />
                    </button>
                    <a href="#" className="btn btn-link" onClick={this.clear}>
                        <FormattedMessage id="common.action.clear.title" />
                    </a>
                </div>

                <ModalView id="modal-view" />
            </div>
        );
    }
}
