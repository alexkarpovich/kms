import './CourseList.less';
import React, {Component, PropTypes} from 'react';
import {Link} from 'react-router';
import {FormattedMessage, injectIntl} from 'react-intl';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import CourseItem from './CourseItem';


export default class CourseList extends Component {
    constructor(props) {
        super(props);

        this.loadCourses = ::this.loadCourses;
        this.searchCourses = ::this.searchCourses;
        this.handleKeyPress = ::this.handleKeyPress;
    }

    componentDidMount() {
        this.loadCourses();
    }

    loadCourses(searchValue) {
        let {actions} = this.props;

        actions.loadCoursePage({
            page: 1,
            size: 1000,
            ordering: 'id',
            search: searchValue ? searchValue : ''
        });
    }

    searchCourses() {
        let searchValue = this.refs.search.value;
        this.loadCourses(searchValue);
    }

    handleKeyPress(e) {
        if (e.charCode == 13) {
            this.searchCourses();
        }
    }

    render() {
        let {course: {page: coursePage}, routeActions, actions, auth: {authUser}} = this.props;

        return (
            <div className="course-list">
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="course.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="actions">
                                <div className="new-course">
                                    <div className="form-group">
                                        <Link to={'/course/create'} className="btn btn-sm btn-primary">
                                            <span className="fa fa-plus fa-lg"></span>
                                            {' Course'}
                                        </Link>
                                    </div>
                                </div>
                                <div className="search">
                                    <div className="input-group">
                                        <input
                                            type="text"
                                            ref="search"
                                            className="form-control input-sm"
                                            onKeyPress={this.handleKeyPress}
                                            placeholder="Search..."
                                        />
                                        <span className="input-group-btn">
                                            <button
                                                className="btn btn-sm btn-default"
                                                type="button"
                                                onClick={this.searchCourses}
                                            >
                                                Search
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div>
                                { coursePage.results.map(course => {
                                    return <CourseItem
                                        key={course.id}
                                        course={course}
                                        deleteCourse={actions.deleteCourse}
                                        loadCourses={this.loadCourses}
                                        routeActions={routeActions}
                                        authUser={authUser}
                                    />
                                }) }
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(CourseList);
