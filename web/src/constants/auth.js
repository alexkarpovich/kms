import keymirror from 'keymirror';

export var AuthConst = keymirror({
    LOGIN_USER: null,
    LOGIN_USER_REQUEST: null,
    LOGIN_USER_SUCCESS: null,
    LOGIN_USER_FAILURE: null,

    REGISTER_USER: null,
    REGISTER_USER_REQUEST: null,
    REGISTER_USER_SUCCESS: null,
    REGISTER_USER_FAILURE: null,

    GET_AUTH_USER: null,
    GET_AUTH_USER_REQUEST: null,
    GET_AUTH_USER_SUCCESS: null,
    GET_AUTH_USER_FAILURE: null,
    SET_AUTH_USER: null,

    LOGOUT_USER: null
});
