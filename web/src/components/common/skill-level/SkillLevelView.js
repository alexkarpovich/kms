import './SkillLevelView.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';

class SkillLevelView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            level: this.props.userSkill.level
        };

        this.deleteUserSkill = ::this.deleteUserSkill;
        this.handleChange = ::this.handleChange;
        this.handleSave = ::this.handleSave;
        this.handleCancel = ::this.handleCancel;
        this.resetValue = ::this.resetValue;
    }

    static propTypes = {
        userSkill: PropTypes.object.isRequired,
        onDelete: PropTypes.func.isRequired,
        onSave: PropTypes.func.isRequired
    };

    componentWillReceiveProps(newProps) {
        if (this.state.level !== newProps.userSkill.level) {
            this.state.level = newProps.userSkill.level;
            this.setState(this.state);
        }
    }

    deleteUserSkill() {
        let {onDelete, intl: {formatMessage}, userSkill} = this.props;

        alertify.confirm(formatMessage({id: 'confirmation.delete'}), isConfirmed => {
            isConfirmed && onDelete && onDelete(userSkill.id);
        })
    }

    resetValue() {
        this.state.level = this.props.userSkill.level;
        this.setState(this.state);
    }

    handleSave() {
        let {userSkill, onSave} = this.props;
        let {level} = this.state;
        let newUserSkill = {
            skill_id: userSkill.skill.id,
            level: level
        };

        onSave && onSave(newUserSkill);
        this.resetValue();
    }

    handleCancel() {
        this.resetValue();
    }

    handleChange(e) {
        this.state[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    render() {
        let {userSkill} = this.props;
        let {level} = this.state;

        return (
            <div className="btn-group btn-group-xs skill-level-view" role="group">
                  <button type="button" className="btn btn-xs btn-danger" onClick={this.deleteUserSkill}>
                        <i className="fa fa-trash"></i>
                  </button>
                  <button type="button" className="btn btn-xs btn-success">
                      {userSkill.skill.name}
                  </button>
                  <button
                      type="button"
                      className="btn btn-xs btn-success dropdown-toggle"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                  >
                      {userSkill.level + ' '}
                      <span className="caret"></span>
                      <span className="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul className="level-popover dropdown-menu">
                      <div className="level-picker">
                          <input
                              type="range"
                              id="level"
                              className="form-control"
                              min="0"
                              max="100"
                              value={level}
                              onChange={this.handleChange}
                          />
                          <span className="value">
                              {level}
                          </span>
                      </div>
                      <button
                          type="button"
                          className="btn btn-sm btn-link"
                          onClick={this.handleSave} >
                          <FormattedMessage id="common.action.save.title" />
                      </button>
                      <button
                          type="button"
                          className="btn btn-sm btn-link"
                          onClick={this.handleCancel} >
                          <FormattedMessage id="common.action.cancel.title" />
                      </button>
                  </ul>
            </div>
        );
    }
}

export default injectIntl(SkillLevelView);
