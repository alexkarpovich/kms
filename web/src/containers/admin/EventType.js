import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {routeActions} from 'react-router-redux';
import * as EventTypeActions from 'actions/event-type';

class EventType extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className="event-type-container">
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...EventTypeActions
        }, dispatch)
    })
)(EventType);
