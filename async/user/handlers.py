import sqlalchemy as sa
from aiohttp import web
from aiopg.sa import create_engine

from .models import User
from .schemes import UserSchema
from settings.base import settings


class UserHandler(web.View):

    async def get(self):
        data = []
        username = self.request.match_info['username']
        schema = UserSchema()
        query = (sa.select([User]).where(User.c.username == username))
        async with create_engine(**settings['database']) as engine:
            async with engine.acquire() as conn:
                async for row in conn.execute(query):
                    result = schema.dump(row)
                    data.append(result.data)
        return web.json_response(data)


class UserListHandler(web.View):

    async def get(self):
        data = []
        query = (sa.select([User]))
        schema = UserSchema()
        async with create_engine(**settings['database']) as engine:
            async with engine.acquire() as conn:
                async for row in conn.execute(query):
                    result = schema.dump(row)
                    data.append(result.data)
        return web.json_response(data)
