import './Breadcrumb.less'
import React, {Component} from 'react'
import {Link} from 'react-router'
import {FormattedMessage} from 'react-intl'

export default class Breadcrumb extends Component {
    render() {
        var pathname = window.location.pathname,
            pathContainAdmin = pathname.search('admin') !== -1,
            routes = [
                {
                    url: '/',
                    title: 'navigation.home',
                    icon: 'fa fa-home'
                }
            ];

        if (pathContainAdmin) {
            pathname = pathname.replace('/admin/', '');
            pathname = pathname.split('/');

            pathname.forEach((item, index) => {
                let title,
                    url = index === 0
                        ? routes[index].url + ( pathContainAdmin ? 'admin/' : '/' ) + item
                        : routes[index].url + '/' + item;

                if (index === 0 && item.search('-') !== -1) {
                    let position = item.search('-'),
                        upperCase = item[position + 1].toUpperCase(),
                        re = /(-\w)/;

                    title = 'navigation.' + item.replace(re, upperCase);
                } else {
                    title = index === 0 ? 'navigation.' + item :
                        ( Number.isInteger(+item) ? 'breadcrumb.update' : 'breadcrumb.create' );
                }

                routes.push({
                    url: url,
                    title: title
                });
            });
        } else {
            pathname = pathname.replace('/', '');

            if (pathname !== '') {
                routes.push({
                    url: '/' + pathname,
                    title: 'breadcrumb.' + pathname
                });
            }
        }

        routes[routes.length - 1].active = true;

        routes = routes.map((route, index) => {
            if (route.active) {
                return (
                    <li key={ index } className="active">
                        <i className={ route.icon }/>
                        <FormattedMessage id={route.title} />
                    </li>
                );
            }

            return (
                <li key={ index }>
                    <Link to={ route.url }>
                        <i className={ route.icon }/>
                        <FormattedMessage id={route.title} />
                    </Link>
                </li>
            );
        });

        return (
            <ol className="breadcrumb">
                { routes }
            </ol>
        );
    }
}
