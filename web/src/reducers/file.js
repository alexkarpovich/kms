import {combineReducers} from 'redux';
import {FileConst} from 'constants';
import FileModel from 'models/File';
import PageModel from 'models/Page';


function item(state = FileModel, action) {
    switch(action.type) {
        case FileConst.LOAD_FILE_REQUEST:
            return {...state, isFetching: false, error: null};

        case FileConst.LOAD_FILE_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case FileConst.LOAD_FILE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        case FileConst.UPLOAD_FILE_REQUEST:
            return {...state, isFetching: false, error: null};

        case FileConst.UPLOAD_FILE_SUCCESS:
            return {...state, ...action.data, isFetching: false};

        case FileConst.UPLOAD_FILE_FAILURE:
            return {...state, error: action.error, isFetching: false};

        default:
            return state;
    }
}

function page(state = PageModel, action) {
    switch(action.type) {
        default:
            return state;
    }
}

export default combineReducers({
    item, page
});
