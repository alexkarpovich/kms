# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=255)),
                ('content', models.TextField(default=b'', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='LessonType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=255)),
                ('description', models.TextField(default=b'', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='lesson',
            name='type',
            field=models.ForeignKey(to='lesson.LessonType'),
        ),
    ]
