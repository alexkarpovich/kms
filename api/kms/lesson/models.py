from django.db import models
from markdownx.models import MarkdownxField
from common.mixins import RatingModelMixin, CommentModelMixin
from kmsuser.models import User


class LessonType(models.Model):
    name = models.CharField(max_length=255, blank=False, default='')
    description = models.TextField(blank=True, default='')

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name


class Lesson(RatingModelMixin, CommentModelMixin, models.Model):
    name = models.CharField(max_length=255, blank=False, default='')
    type = models.ForeignKey(LessonType, blank=False)
    content = MarkdownxField(blank=True, default='')
    author = models.ForeignKey(User, related_name='lesson', blank=False, null=False, default=1)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name
