import './LessonItem.less';
import React, {Component, PropTypes} from 'react';
import {injectIntl} from 'react-intl';

class LessonItem extends Component {
    constructor(props) {
        super(props);

        this.viewLesson = ::this.viewLesson;
        this.updateLesson = ::this.updateLesson;
        this.removeLesson = ::this.removeLesson;
    }

    static propTypes = {
        lesson: PropTypes.object.isRequired,
        deleteLesson: PropTypes.func.isRequired,
        loadLessons: PropTypes.func.isRequired,
        routeActions: PropTypes.object.isRequired,
        authUser: PropTypes.object.isRequired
    };

    updateLesson() {
        let {routeActions, lesson} = this.props;

        routeActions.push(`/lesson/${lesson.id}/edit`);
    }

    removeLesson() {
        let {lesson, deleteLesson, loadLessons, intl: {formatMessage}} = this.props;

        alertify.confirm(formatMessage({id: 'confirmation.delete'}), isConfirmed => {
            isConfirmed && deleteLesson(lesson.id).then(() => loadLessons());
        });
    }

    viewLesson() {
        let {routeActions, lesson} = this.props;

        routeActions.push(`/lesson/${lesson.id}`);
    }

    render() {
        let {lesson: {name, author}, authUser} = this.props;

        return (
            <div className="lesson-item info-box">
                {authUser && author.id === authUser.id ?
                <div className="actions box-tools pull-right">
                    <button className="btn btn-xs btn-default" onClick={this.updateLesson}>
                        <i className="fa fa-edit"/>
                    </button>
                    <button className="btn btn-xs btn-default" onClick={this.removeLesson}>
                        <i className="fa fa-close"/>
                    </button>
                </div>
                    : null }
                <span className="icon info-box-icon bg-green" onClick={this.viewLesson}>
                    <i className="fa fa-book"/>
                </span>
                <div className="info-box-content">
                    <div className="title">
                        {`${name}`}
                    </div>
                </div>
            </div>
        );
    }
}

export default injectIntl(LessonItem);
