# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('lesson', '0001_initial'),
        ('skill', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=255)),
                ('description', models.TextField(default=b'', blank=True)),
                ('complexity', models.IntegerField(default=30, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(100)])),
                ('rating', models.FloatField(default=2.5, blank=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(5)])),
                ('lessons', models.ManyToManyField(related_name='courses', to='lesson.Lesson', blank=True)),
                ('skills', models.ManyToManyField(related_name='courses', to='skill.Skill', blank=True)),
            ],
        ),
    ]
