export default [
    {
        title: 'navigation.title',
        header: true,
        isPublic: true
    },
    {
        icon: 'fa fa-home',
        title: 'navigation.home',
        link: '/',
        isPublic: true
    },
    {
        icon: 'fa fa-adn',
        title: 'navigation.admin',
        link: '/admin',
        isAdmin: true,
        children: [
            {
                icon: 'fa fa-users',
                title: 'navigation.user',
                link: '/user'
            },
            {
                icon: 'fa fa-circle',
                title: 'navigation.skill',
                link: '/skill'
            },
            {
                icon: 'fa fa-circle',
                title: 'navigation.skillCategory',
                link: '/skill-category'
            },
            {
                icon: 'fa fa-group',
                title: 'navigation.group',
                link: '/group'
            },
            {
                icon: 'fa fa-lock',
                title: 'navigation.permission',
                link: '/permission'
            },
            {
                icon: 'fa fa-circle',
                title: 'navigation.lessonType',
                link: '/lesson-type'
            },

            {
                icon: 'fa fa-circle',
                title: 'navigation.eventType',
                link: '/event-type'
            },
            {
                icon: 'fa fa-book',
                title: 'navigation.lesson',
                link: '/lesson'
            },
            {
                icon: 'fa fa-graduation-cap',
                title: 'navigation.course',
                link: '/course'
            },
            {
                icon: 'fa fa-circle',
                title: 'navigation.event',
                link: '/event'
            }
        ]
    },
    {
        icon: 'fa fa-calendar',
        title: 'navigation.calendar',
        link: '/calendar'
    },
    {
        icon: 'fa fa-graduation-cap',
        title: 'navigation.course',
        link: '/course'
    },
    {
        icon: 'fa fa-book',
        title: 'navigation.lesson',
        link: '/lesson'
    },
    {
        icon: 'fa fa-user',
        title: 'navigation.user',
        link: '/user'
    }
];
