import React, {Component, PropTypes} from 'react'
import classnames from 'classnames'

export default class LocaleSwitcherItem extends Component {
    static propTypes = {
        locale: PropTypes.string.isRequired,
        currentLocale: PropTypes.string.isRequired,
        changeLocale: PropTypes.func.isRequired
    };

    switchLocale() {
        let {locale, changeLocale} = this.props;

        changeLocale(locale);
    }

    render() {
        let {locale, currentLocale} = this.props;

        var classes = classnames({
            'btn btn-link btn-xs': true,
            'active': locale === currentLocale
        });

        return (
            <span className={classes} onClick={::this.switchLocale}>{ locale }</span>
        );
    }
}
