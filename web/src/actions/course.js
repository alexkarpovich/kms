import {CourseConst} from 'constants';
import api from 'helpers/api';

export function loadCoursePage(params) {
    return async dispatch => {
        dispatch({type: CourseConst.LOAD_COURSE_PAGE_REQUEST});

        try {
            let data = await api({url: 'course/', query: params});

            dispatch({type: CourseConst.LOAD_COURSE_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: CourseConst.LOAD_COURSE_PAGE_FAILURE, error});
        }
    }
}

export function loadCourse(courseId) {
    return async dispatch => {
        dispatch({type: CourseConst.LOAD_COURSE_REQUEST});

        try {
            let data = await api({url: `course/${courseId}/`});
            dispatch({type: CourseConst.LOAD_COURSE_SUCCESS, data});
        } catch (error) {
            dispatch({type: CourseConst.LOAD_COURSE_FAILURE, error});
        }
    };
}

export function createCourse(course) {
    return async dispatch => {
        dispatch({type: CourseConst.CREATE_COURSE_REQUEST});

        try {
            await api({url: `course/`, method: 'post', data: course});

            dispatch({type: CourseConst.CREATE_COURSE_SUCCESS});
        } catch (error) {
            dispatch({type: CourseConst.CREATE_COURSE_FAILURE, error});
        }
    };
}

export function updateCourse(course) {
    return async dispatch => {
        dispatch({type: CourseConst.UPDATE_COURSE_REQUEST});

        try {
            await api({url: `course/${course.id}/`, method: 'put', data: course});

            dispatch({type: CourseConst.UPDATE_COURSE_SUCCESS});
        } catch (error) {
            dispatch({type: CourseConst.UPDATE_COURSE_FAILURE, error});
        }
    };
}

export function deleteCourse(courseId) {
    return async dispatch => {
        dispatch({type: CourseConst.DELETE_COURSE_REQUEST});

        try {
            await api({url: `course/${courseId}/`, method: 'delete'});

            dispatch({type: CourseConst.DELETE_COURSE_SUCCESS});
        } catch (error) {
            dispatch({type: CourseConst.DELETE_COURSE_FAILURE, error});
        }
    };
}
