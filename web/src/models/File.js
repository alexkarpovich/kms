const File = {
    id: null,
    path: '',
    filename: '',
    contentType: ''
};

export default File;
