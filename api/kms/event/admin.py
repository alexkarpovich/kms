from django.contrib import admin

from .models import Event, EventType


class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'start', 'end', )
    list_filter = ('name', 'start', 'end', 'type', )
    search_fields = ('name', 'description', 'start', 'end', 'type', )


admin.site.register(EventType)
admin.site.register(Event, EventAdmin)
