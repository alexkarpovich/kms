import {UserConst} from 'constants';
import api from 'helpers/api';

export function loadUserPage(params) {
    return async dispatch => {
        dispatch({type: UserConst.LOAD_USER_PAGE_REQUEST});

        try {
            let data = await api({url: 'user/', query: params});

            dispatch({type: UserConst.LOAD_USER_PAGE_SUCCESS, data});
        } catch (error) {
            dispatch({type: UserConst.LOAD_USER_PAGE_FAILURE, error});
        }
    }
}

export function loadUser(userId) {
    return async dispatch => {
        dispatch({type: UserConst.LOAD_USER_REQUEST});

        try {
            let data = await api({url: `user/${userId}/`});

            dispatch({type: UserConst.LOAD_USER_SUCCESS, data});
        } catch (error) {
            dispatch({type: UserConst.LOAD_USER_FAILURE, error});
        }
    };
}

export function createUser(user) {
    return async dispatch => {
        dispatch({type: UserConst.CREATE_USER_REQUEST});

        try {
            await api({url: `user/`, method: 'post', data: user});

            dispatch({type: UserConst.CREATE_USER_SUCCESS});
        } catch (error) {
            dispatch({type: UserConst.CREATE_USER_FAILURE, error});
        }
    };
}

export function updateUser(user) {
    return async dispatch => {
        dispatch({type: UserConst.UPDATE_USER_REQUEST});

        try {
            await api({url: `user/${user.id}/`, method: 'put', data: user});

            dispatch({type: UserConst.UPDATE_USER_SUCCESS});
        } catch (error) {
            dispatch({type: UserConst.UPDATE_USER_FAILURE, error});
        }
    };
}

export function deleteUser(userId) {
    return async dispatch => {
        dispatch({type: UserConst.DELETE_USER_REQUEST});

        try {
            await api({url: `user/${userId}/`, method: 'delete'});

            dispatch({type: UserConst.DELETE_USER_SUCCESS});
        } catch (error) {
            dispatch({type: UserConst.DELETE_USER_FAILURE, error});
        }
    };
}

export function loadUserLessons(userId) {
    return async dispatch => {
        dispatch({type: UserConst.LOAD_USER_LESSONS_REQUEST});

        try {
            let data = await api({url: `user/${userId}/lessons/`});

            dispatch({type: UserConst.LOAD_USER_LESSONS_SUCCESS, data});
        } catch (error) {
            dispatch({type: UserConst.LOAD_USER_LESSONS_FAILURE, error});
        }
    }
}

export function loadUserCourses(userId) {
    return async dispatch => {
        dispatch({type: UserConst.LOAD_USER_COURSES_REQUEST});

        try {
            let data = await api({url: `user/${userId}/courses/`});

            dispatch({type: UserConst.LOAD_USER_COURSES_SUCCESS, data});
        } catch (error) {
            dispatch({type: UserConst.LOAD_USER_COURSES_FAILURE, error});
        }
    }
}

export function loadUserSkills(userId) {
    return async dispatch => {
        dispatch({type: UserConst.LOAD_USER_SKILLS_REQUEST});

        try {
            let data = await api({url: `user/${userId}/skills/`});

            dispatch({type: UserConst.LOAD_USER_SKILLS_SUCCESS, data});
        } catch (error) {
            dispatch({type: UserConst.LOAD_USER_SKILLS_FAILURE, error});
        }
    }
}

export function addUserSkill(userId, userSkill) {
    return async dispatch => {
        dispatch({type: UserConst.ADD_USER_SKILL_REQUEST});

        try {
            await api({url: `user/${userId}/skills/`, method: 'post', data: userSkill});

            dispatch({type: UserConst.ADD_USER_SKILL_SUCCESS, data});
        } catch (error) {
            dispatch({type: UserConst.ADD_USER_SKILL_FAILURE, error});
        }
    }
}

export function deleteUserSkill(userId, userSkillId) {
    return async dispatch => {
        dispatch({type: UserConst.DELETE_USER_SKILL_REQUEST});

        try {
            await api({url: `user/${userId}/skills/${userSkillId}/`, method: 'delete'});

            dispatch({type: UserConst.DELETE_USER_SKILL_SUCCESS, data});
        } catch (error) {
            dispatch({type: UserConst.DELETE_USER_SKILL_FAILURE, error});
        }
    }
}
