'use strict';

import React, {Component} from 'react';
import classnames from 'classnames';
import {FormattedMessage, formatMessage} from 'react-intl'

export default class HeaderItem extends Component {
    constructor(props, context) {
        super(props, context);

        this.activate = ::this.activate;
    }

    static contextTypes = {
        setOrdering: React.PropTypes.func
    };

    activate() {
        let {column, ordering} = this.props;
        let {by, direction} = ordering;

        if (column.key == ordering.by) {
            direction = direction === '+' ? '-' : '+';
        } else {
            by = column.key;
            direction = '+';
        }

        this.context.setOrdering({
            by: by,
            direction: direction
        });
    }

    render() {
        let {column, ordering} = this.props;
        let isActive = column.key == ordering.by;
        let isAscDirection = ordering.direction === '+';

        let styles = classnames({
            'column-header': true,
            'active': isActive
        });

        let iconStyles = classnames({
            'pull-right': true,
            'fa fa-sort-asc': isAscDirection,
            'fa fa-sort-desc': !isAscDirection,
            'grid-header-direction-hide': !isActive
        });

        return (
            <th className={styles} onClick={this.activate}>
                <div className="kms-table-header clearfix">
                    <span className="pull-left"><FormattedMessage id={column.name} /></span>
                    <i className={iconStyles}></i>
                </div>
            </th>
        );
    }
}
