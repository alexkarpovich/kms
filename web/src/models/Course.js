const CourseModel = {
    id: null,
    name: '',
    author: {},
    description: '',
    complexity: null,
    skills: [],
    lessons: []
};

export default CourseModel;
