import React, {Component, PropType} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import Select from 'react-select';
import UserSkillComponent from 'components/common/UserSkillComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import FileUpload from 'components/common/file-upload/FileUpload';
import SkillLevelForm from 'components/common/skill-level/SkillLevelForm';
import SkillLevelView from 'components/common/skill-level/SkillLevelView';
import PathHelper from 'helpers/path';
import UserModel from 'models/User';

class UserForm extends UserSkillComponent {
    constructor(props) {
        super(props);

        let {user, params} = props;
        let groupIds = user.item.groups.map(group => group.id);
        let skillIds = user.item.skills.map(skill => skill.id);
        let avatarId = user.item.avatar ? user.item.avatar.id : null;

        this.state = {
            user: !!params.userId ? {
                ...user.item,
                group_ids: groupIds,
                skill_ids: skillIds,
                avatar_id: avatarId
            } : {...UserModel},
            avatar: user.item.avatar,
            showSkillForm: false
        };

        this.createUser = ::this.createUser;
        this.updateUser = ::this.updateUser;
        this.handleChange = ::this.handleChange;
        this.arrayChange = ::this.arrayChange;
        this.avatarChange = ::this.avatarChange;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        actions.loadGroupPage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        actions.loadSkillPage({
            page: 1,
            size: 1000,
            ordering: 'id'
        });

        if (!!params.userId) {
            actions.loadUser(params.userId);
            actions.loadUserSkills(params.userId);
        }
    }

    componentWillReceiveProps(newProps) {
        let {user: {item}, params} = newProps;
        let {user} = this.state;
        let groupIds = item.groups.map(group => group.id);
        let skillIds = item.skills.map(skill => skill.id);
        let avatarId = item.avatar ? item.avatar.id : null;

        if (user !== item && !!params.userId) {
            this.state.user = {
                ...item,
                group_ids: groupIds,
                skill_ids: skillIds,
                avatar_id: avatarId
            };
            this.state.avatar = item.avatar;
        }
    }

    createUser() {
        let {routeActions, actions} = this.props;
        let user = {...this.state.user};
        delete user.groups;
        delete user.skills;
        delete user.avatar;

        if (user.password === user.confirmPassword) {
            actions.createUser(user).then(() => {
                routeActions.push('/admin/user');
            });
        }
    }

    updateUser() {
        let {routeActions, actions} = this.props;
        let user = {...this.state.user};
        delete user.groups;
        delete user.skills;
        delete user.avatar;

        actions.updateUser(user).then(() => {
            routeActions.push('/admin/user');
        });
    }

    handleChange(e) {
        this.state.user[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    arrayChange(items, name) {
        this.state.user[name] = items.map(item => item.value);
        this.setState(this.state);
    }

    avatarChange(fileData) {
        this.state.user.avatar_id = fileData.id;
        this.state.avatar = fileData;
        this.setState(this.state);
    }

    render() {
        let {params, intl: {formatMessage}, group: {page: groupPage}, skill: {page: skillPage}, actions,
            user: {skills: userSkills}} = this.props;
        let isEdit = !!params.userId;
        let {user, avatar, showSkillForm} = this.state;

        let submitClasses = classnames({
            'btn': true,
            'btn-sm': true,
            'btn-primary': !isEdit,
            'btn-warning': isEdit
        });

        let groups = groupPage.results.map(group => ({
            label: group.name,
            value: group.id
        }));

        let avatarSrc = !!avatar && avatar.path ?
            PathHelper.image(avatar.path, 256, 256) : '/images/avatar256x256.png';

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="user.title.many"/>
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="avatar">
                                                <FormattedMessage id="user.avatar.title"/>
                                            </label>
                                            <div className="avatar">
                                                <img src={avatarSrc}/>
                                            </div>
                                            <FileUpload
                                                {...this.props}
                                                uploadAs="image"
                                                onChange={this.avatarChange}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="username">
                                                <FormattedMessage id="user.username.title"/>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="username"
                                                onChange={this.handleChange}
                                                value={user.username}
                                                placeholder={formatMessage({id: 'user.username.title'})}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="email">
                                                <FormattedMessage id="user.email.title"/>
                                            </label>
                                            <input
                                                type="email"
                                                className="form-control input-sm"
                                                id="email"
                                                onChange={this.handleChange}
                                                value={user.email}
                                                placeholder={formatMessage({id: 'user.email.title'})}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="groups">
                                                <FormattedMessage id="group.title.many"/>
                                            </label>
                                            <Select
                                                name="groups"
                                                options={groups}
                                                value={user.group_ids}
                                                onChange={(groups) => this.arrayChange(groups, 'group_ids')}
                                                multi={true}
                                                placeholder={formatMessage({id: 'common.action.select.title'})}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="skills">
                                                <FormattedMessage id="skill.title.many"/>
                                            </label>
                                            <button type="button" className="btn btn-xs btn-link" onClick={() => this.showSkillForm(true)}>
                                                <i className="fa fa-plus"></i>
                                                <FormattedMessage id="common.action.add.title" />
                                            </button>
                                            <br />
                                            {userSkills.data.map(item =>
                                                <SkillLevelView
                                                    key={item.id}
                                                    userSkill={item}
                                                    onDelete={this.deleteSkill}
                                                    onSave={this.saveSkill}
                                                />)}
                                            {showSkillForm ?
                                                <SkillLevelForm
                                                    userSkills={userSkills.data}
                                                    allSkills={skillPage.results}
                                                    onCancel={this.cancelSkill}
                                                    onSave={this.saveSkill}
                                                />
                                            : null}
                                        </div>
                                        { !isEdit ?
                                            <div className="form-group">
                                                <label htmlFor="password">
                                                    <FormattedMessage id="user.password.title"/>
                                                </label>
                                                <input
                                                    type="password"
                                                    className="form-control input-sm"
                                                    id="password"
                                                    onChange={this.handleChange}
                                                    placeholder={formatMessage({id: 'user.password.title'})}
                                                />
                                            </div> : null }
                                        { !isEdit ?
                                            <div className="form-group">
                                                <label htmlFor="confirmPassword">
                                                    <FormattedMessage id="user.confirmPassword.title"/>
                                                </label>
                                                <input
                                                    type="password"
                                                    className="form-control input-sm"
                                                    id="confirmPassword"
                                                    onChange={this.handleChange}
                                                    placeholder={formatMessage({id: 'user.confirmPassword.title'})}
                                                />
                                            </div> : null }
                                        <div className="form-group">
                                            <label htmlFor="firstName">
                                                <FormattedMessage id="user.firstName.title"/>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="firstName"
                                                onChange={this.handleChange}
                                                value={user.firstName}
                                                placeholder={formatMessage({id: 'user.firstName.title'})}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="lastName">
                                                <FormattedMessage id="user.lastName.title"/>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="lastName"
                                                onChange={this.handleChange}
                                                value={user.lastName}
                                                placeholder={formatMessage({id: 'user.lastName.title'})}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="phone">
                                                <FormattedMessage id="user.phone.title"/>
                                            </label>
                                            <input
                                                type="tel"
                                                className="form-control input-sm"
                                                id="phone"
                                                onChange={this.handleChange}
                                                value={user.phone}
                                                placeholder={formatMessage({id: 'user.phone.title'})}
                                            />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="skype">
                                                <FormattedMessage id="user.skype.title"/>
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="skype"
                                                onChange={this.handleChange}
                                                value={user.skype}
                                                placeholder={formatMessage({id: 'user.skype.title'})}
                                            />
                                        </div>

                                        <button type="button" className={submitClasses}
                                                onClick={isEdit ? this.updateUser : this.createUser}>
                                            <FormattedMessage id="grid.action.submit.title.up"/>
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(UserForm);
