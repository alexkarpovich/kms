import {I18nConst} from 'constants'

export function changeLocale(locale) {
    return {
        type: I18nConst.CHANGE_LOCALE,
        locale
    };
}
