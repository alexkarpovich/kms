import './SkillLevelForm.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';

class SkillLevelForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            skill_id: null,
            level: 0
        };

        this.resetValues = ::this.resetValues;
        this.handleSave = ::this.handleSave;
        this.handleCancel = ::this.handleCancel;
        this.handleChange = ::this.handleChange;
    }

    static propTypes = {
        userSkills: PropTypes.array.isRequired,
        allSkills: PropTypes.array.isRequired,
        onCancel: PropTypes.func,
        onSave: PropTypes.func
    };

    resetValues() {
        this.state.skill_id = null;
        this.state.level = 0;
    }

    handleCancel() {
        let {onCancel} = this.props;

        onCancel && onCancel();
        this.resetValues();
    }

    handleSave() {
        let {onSave} = this.props;

        if (this.state.skill_id !== null) {
            onSave && onSave(this.state);
            this.resetValues();
        }
    }

    handleChange(e) {
        this.state[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    render() {
        let {allSkills, userSkills, intl: {formatMessage}} = this.props;
        let {id: skillId} = this.state;
        let userSkillIds = userSkills.map(item => item.skill.id);

        return (
            <div className="skill-level-form form-horizontal">
                <div className="control-group">
                    <label htmlFor="skill_id">
                        <FormattedMessage id="common.name.title" />
                    </label>
                    <select
                        id="skill_id"
                        className="form-control input-sm"
                        value={skillId}
                        defaultValue={0}
                        onChange={this.handleChange}
                    >
                        <option value={0} disabled>
                            {formatMessage({id: 'common.action.select.title'})}
                        </option>
                        { allSkills.map(item => {
                            if (userSkillIds.indexOf(item.id) === -1) {
                                return <option key={item.id} value={item.id}>{item.name}</option>;
                            }
                        }) }
                    </select>
                </div>
                <div className="control-group">
                    <label htmlFor="level">
                        <FormattedMessage id="common.level.title" />
                    </label>
                    <input
                        type="number"
                        className="form-control input-sm"
                        id="level"
                        min="1"
                        max="100"
                        onChange={this.handleChange}
                        placeholder={formatMessage({id: 'common.level.title'})}
                    />
                </div>
                <button type="button" className="btn btn-sm btn-link" onClick={this.handleSave}>
                    <FormattedMessage id="common.action.save.title" />
                </button>
                <button type="button" className="btn btn-sm btn-link" onClick={this.handleCancel}>
                    <FormattedMessage id="common.action.cancel.title" />
                </button>
            </div>
        );
    }
}

export default injectIntl(SkillLevelForm);
