import './CommentForm.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import PathHelper from 'helpers/path';
import MarkdownEditor from 'components/common/markdown-editor/MarkdownEditor';

class CommentForm extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            isOpened: false,
            content: ''
        };

        this.handleChange = ::this.handleChange;
        this.changeView = ::this.changeView;
        this.postComment = ::this.postComment;
    }

    static propTypes = {
        parentId: PropTypes.number,
        focus: PropTypes.bool.isRequired,
        onCancel: PropTypes.func
    };

    static defaultProps = {
        parentId: null,
        focus: false
    };

    static contextTypes = {
        authUser: PropTypes.object.isRequired,
        postComment: PropTypes.func.isRequired
    };

    componentDidMount() {
        let {focus} = this.props;

        focus && this.refs.content.focus();
    }

    handleChange(e) {
        this.state[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    changeView(isOpened) {
        let {onCancel} = this.props;

        this.state.isOpened = isOpened;
        if (!isOpened) {
            this.state.content = '';
            onCancel && onCancel();
        }

        this.setState(this.state);
    }

    postComment() {
        let {postComment, authUser} = this.context;
        let {parentId} = this.props;
        let {content} = this.state;

        let comment = {
            parentId: parentId,
            authorId: authUser.id,
            content: content
        };

        postComment(comment).then(() => this.changeView(false));
    }

    render() {
        let {intl: {formatMessage}} = this.props;
        let {isOpened, content} = this.state;
        let {authUser} = this.context;

        let avatarSrc = authUser && authUser.avatar && authUser.avatar.path ?
            PathHelper.image(authUser.avatar.path, 64, 64) : '/images/avatar64x64.png';

        return (
            <div className="comment-form">
                <div className="new-comment closed">
                    <span className="user-avatar">
                        <img src={avatarSrc} />
                    </span>
                    <div className="comment-placeholder">
                        <div className="form-group">
                            <textarea
                                id="content"
                                ref="content"
                                className="form-control"
                                value={content}
                                onFocus={() => this.changeView(true)}
                                onChange={this.handleChange}
                                placeholder={formatMessage({id: 'common.comment.form.placeholder'})}
                            />
                        </div>
                        { isOpened ?
                        <div className="form-group">
                            <button
                                type="button"
                                className="btn btn-primary btn-sm"
                                onClick={this.postComment}
                                disabled={content===''} >
                                <FormattedMessage id="grid.action.submit.title.up" />
                            </button>

                            <a href="##" className="btn btn-link btn-sm" onClick={() => this.changeView(false)}>
                                <FormattedMessage id="common.action.cancel.title" />
                            </a>
                        </div>
                        : null }
                    </div>
                </div>
            </div>
        );
    }
}

export default injectIntl(CommentForm);
