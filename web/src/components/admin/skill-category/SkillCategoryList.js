import React, {Component, PropType} from 'react';
import {FormattedMessage} from 'react-intl';
import {Grid, GridConfig, GridMapping} from 'components/common/grid/Grid';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';


export default class SkillCategoryList extends Component {
    render() {
        let {skillCategory, actions} = this.props;
        let gridConfig = new GridConfig();
        let gridMapping = new GridMapping({
            actions: {
                loadPage: actions.loadSkillCategoryPage,
                deleteEntity: actions.deleteSkillCategory
            }
        });

        let columns = [
            {key: 'id', name: 'common.id.title'},
            {key: 'name', name: 'common.name.title'},
            {key: 'description', name: 'common.description.title'},
            {key: 'enabled', name: 'common.enabled.title'},
            {key: 'parent', name: 'common.parent.title'}
        ];

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="skillCategory.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <Grid
                                entity={skillCategory.page}
                                config={gridConfig}
                                mapping={gridMapping}
                                columns={columns}
                                rootUrl={'/admin/skill-category/'}
                            />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
