import flattenMessages from 'utils/flatten-messages'

export const en = flattenMessages({
    navigation: {
        title: 'Navigation',
        admin: 'Admin',
        home: 'Home',
        user: 'User',
        skill: 'Skill',
        skillCategory: 'Skill Category',
        group: 'Group',
        permission: 'Permission',
        lessonType: 'Lesson type',
        eventType: 'Event type',
        lesson: 'Lesson',
        course: 'Course',
        event: 'Event',
        profile: 'Profile',
        calendar: 'Calendar'
    },
    breadcrumb: {
        update: 'Update',
        create: 'Create',
        signin: 'Sign in',
        signup: 'Sign up',
        profile: 'User profile',
        course: 'Course'
    },
    common: {
        id: {
            title: 'ID'
        },
        name: {
            title: 'Name'
        },
        description: {
            title: 'Description'
        },
        type: {
            title: 'Type'
        },
        content: {
            title: 'Content'
        },
        complexity: {
            title: 'Complexity'
        },
        enabled: {
            title: 'Enabled'
        },
        category: {
            title: "Category"
        },
        parent: {
            title: 'Parent'
        },
        level: {
            title: 'Level'
        },
        action: {
            select: {
                title: 'Select'
            },
            clear: {
                title: 'clear'
            },
            cancel: {
                title: 'Cancel'
            },
            save: {
                title: 'Save'
            },
            add: {
                title: 'add'
            }
        },
        confirmation: {
            ok: {
                title: 'OK'
            }
        },
        total: {
            title: 'Total'
        },
        to: {
            title: 'to'
        },
        comment: {
            title: {
                many: 'Comments',
                one: 'Comment'
            },
            reply: 'reply',
            edit: 'edit',
            delete: 'delete',
            form: {
                placeholder: 'Leave your comment here'
            }
        }
    },
    admin: {
        title: {
            one: 'Admin'
        }
    },
    user: {
        title: {
            many: 'Users',
            one: 'User'
        },
        profile: {
            title: 'Profile'
        },
        action: {
            login: 'Log in'
        },
        username: {
            title: 'Username'
        },
        email: {
            title: 'Email'
        },
        password: {
            title: 'Password'
        },
        confirmPassword: {
            title: 'Confirm Password'
        },
        firstName: {
            title: 'First Name'
        },
        lastName: {
            title: 'Last Name'
        },
        phone: {
            title: 'Phone'
        },
        avatar: {
            title: 'Avatar'
        },
        skype: {
            title: 'Skype'
        },
        isActive: {
            title: 'Is Active'
        },
        isStaff: {
            title: 'Is Staff'
        },
        dataJoined: {
            title: 'Date Joined'
        }
    },
    skill: {
        title: {
            many: 'Skills'
        }
    },
    skillCategory: {
        title: {
            many: 'Skill Categories'
        }
    },
    group: {
        title: {
            many: 'Groups'
        }
    },
    permission: {
        title: {
            many: 'Permissions'
        },
        codename: {
            title: 'Code Name'
        },
        contentType: {
            title: 'Content Type'
        }
    },
    lessontype: {
        title: {
            many: 'Lesson Types'
        }
    },
    eventtype: {
        title: {
            many: 'Event Types'
        }
    },
    lesson: {
        title: {
            many: 'Lessons'
        },
        empty: 'List of created lessons is empty'
    },
    course: {
        title: {
            many: 'Courses'
        },
        empty: 'List of created courses is empty'
    },
    event: {
        title: {
            many: 'Event'
        },
        startDate: {
            title: 'Start Date'
        },
        endDate: {
            title: 'End Date'
        },
        startTime: {
            title: 'Start Time'
        },
        endTime: {
            title: 'End Time'
        },
        dateRange: {
            title: 'Date Range'
        },
        timeRange: {
            title: 'Time Range'
        }
    },
    file: {
        action: {
            upload: 'Upload file',
            choose: 'Choose file'
        },
        uploader: {
            title: 'File Uploader',
            info: 'Choose a single file to upload it to the server'
        }
    },
    grid: {
        action: {
            create: {
                title: {
                    low: 'create',
                    up: 'Create'
                }
            },
            update: {
                title: {
                    low: 'update',
                    up: 'Update'
                }
            },
            submit: {
                title: {
                    low: 'submit',
                    up: 'Submit'
                }
            },
            title: {
                many: 'Actions'
            }
        }
    },
    http: {
        status: {
            400: 'Bad request',
            401: 'You are not logged in',
            403: 'Forbidden resource',
            404: 'Resource is not found',
            405: 'Method is not allowed'
        }
    },
    auth: {
        signin: {
            btn: 'Sign in',
            form: {
                label: 'Sign in',
                username: 'Username',
                password: 'Password'
            }
        },
        signup: {
            btn: 'Sign up',
            form: {
                label: 'Sign up',
                username: 'Username',
                password: 'Password',
                firstName: 'First name',
                lastName: 'Last name',
                emailAddress: 'Email Address'
            }
        },
        signout: {
            btn: 'Sign out'
        }
    },
    profile: {
        breadcrumb: 'User profile',
        settings: 'Settings',
        btn: {
            save: 'Save',
            upload: 'Upload a new photo'
        },
        changesSaved: {
            title: 'Changes saved.',
            message: 'Your profile has been successfully updated.'
        }
    },
    confirmation: {
        delete: 'Are you sure that you want to remove this item?'
    }
});
