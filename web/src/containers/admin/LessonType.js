import React, {Component, PropTypes} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {routeActions} from 'react-router-redux';
import * as LessonTypeActions from 'actions/lesson-type';

class LessonType extends Component {
    static propTypes = {
        children: PropTypes.node
    };

    render() {
        return (
            <div className="lesson-type-container">
                { React.cloneElement(this.props.children, this.props) }
            </div>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...LessonTypeActions
        }, dispatch)
    })
)(LessonType);
