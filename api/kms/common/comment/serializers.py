from rest_framework import serializers
from .models import Comment
from kmsuser.serializers import UserSerializer


class SubCommentSerializer(serializers.ModelSerializer):
    def to_representation(self, value):
        serializer = self.parent.__class__(value, context=self.context)
        return serializer.data


class CommentSerializer(serializers.ModelSerializer):
    parent_id = serializers.IntegerField(required=False)
    author = UserSerializer(read_only=True)
    author_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = Comment
        fields = ('id', 'parent_id', 'author', 'author_id', 'content', 'created')
