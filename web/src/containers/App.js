import React, {Component, PropTypes} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {routeActions} from 'react-router-redux'
import * as AuthActions from 'actions/auth'
import * as I18nActions from 'actions/i18n'
import {IntlProvider} from 'react-intl'
import Header from 'components/navigation/Header'
import Aside from 'components/navigation/Aside'
import Footer from 'components/navigation/Footer'


export default class App extends Component {
    render() {
        return (
            <IntlProvider
                locale={this.props.i18n.locale}
                messages={this.props.i18n.messages}
                key={this.props.i18n.locale}>
                <div className="wrapper">
                    <Header
                        authData={this.props.auth}
                        logout={this.props.actions.logoutUser}
                        routeActions={this.props.routeActions}
                    />
                    <Aside {...this.props} />
                    <div className="content-wrapper">
                        {this.props.children}
                    </div>
                    <Footer {...this.props} />
                </div>
            </IntlProvider>
        );
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...AuthActions,
            ...I18nActions
        }, dispatch)
    })
)(App)
