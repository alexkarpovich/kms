from rest_framework import serializers
from kmsuser.serializers import UserSerializer
from .models import Lesson, LessonType


class LessonTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LessonType
        fields = ('id', 'name', 'description')


class LessonSerializer(serializers.ModelSerializer):
    type = LessonTypeSerializer(read_only=True)
    type_id = serializers.IntegerField(write_only=True, required=False)
    author = UserSerializer(read_only=True)
    author_id = serializers.IntegerField(write_only=True, required=False)
    rating = serializers.IntegerField(read_only=True, source='get_rating')

    class Meta:
        model = Lesson
        fields = ('id', 'name', 'type', 'type_id', 'content', 'rating', 'author', 'author_id')

    def create(self, validated_data):
        author_id = validated_data.pop('author_id', self.context['request'].user.id)
        lesson = self.Meta.model(**validated_data)
        lesson.author_id = author_id
        lesson.save()

        return lesson

    def update(self, lesson, validated_data):
        author_id = validated_data.pop('author_id', lesson.author_id)
        lesson.author_id = author_id

        for attr, value in validated_data.items():
            setattr(lesson, attr, value)

        lesson.save()

        return lesson
