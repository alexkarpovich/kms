import keymirror from 'keymirror';

export const GroupConst = keymirror({
    LOAD_GROUP_REQUEST: null,
    LOAD_GROUP_SUCCESS: null,
    LOAD_GROUP_FAILURE: null,

    LOAD_GROUP_PAGE_REQUEST: null,
    LOAD_GROUP_PAGE_SUCCESS: null,
    LOAD_GROUP_PAGE_FAILURE: null,

    CREATE_GROUP_REQUEST: null,
    CREATE_GROUP_SUCCESS: null,
    CREATE_GROUP_FAILURE: null,

    UPDATE_GROUP_REQUEST: null,
    UPDATE_GROUP_SUCCESS: null,
    UPDATE_GROUP_FAILURE: null,

    DELETE_GROUP_REQUEST: null,
    DELETE_GROUP_SUCCESS: null,
    DELETE_GROUP_FAILURE: null,
});
