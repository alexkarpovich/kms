import $ from 'jquery';
import 'eonasdan-bootstrap-datetimepicker';
import React, {Component, PropType} from 'react';
import {FormattedMessage, injectIntl} from 'react-intl';
import classnames from 'classnames';
import CommonComponent from 'components/common/CommonComponent';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import EventModel from 'models/Event';

class EventForm extends CommonComponent {
    constructor(props) {
        super(props);

        let {event, params} = props;

        this.state = {
            event: !!params.eventId ? {...event.item, type_id: event.item.type.id} : {...EventModel}
        };

        this.handleChange = ::this.handleChange;
        this.createEvent = ::this.createEvent;
        this.updateEvent = ::this.updateEvent;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        /* TODO: Load all eventType pages */
        actions.loadEventTypePage({
            page: 1,
            size: 10000,
            ordering: 'id'
        });

        if (params.eventId) {
            actions.loadEvent(params.eventId);
        }

        var eventForm = this;

        $('.input-daterange input').each(function() {
            $(this).datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss'
                })
                .on('dp.change', eventForm.handleChange);
        });
    }

    componentWillReceiveProps(newProps) {
        let {params, event} = newProps;

        if (this.state.event !== event.item && !!params.eventId) {
            this.state.event = {...event.item, type_id: event.item.type.id};
        }
    }

    updateEvent() {
        let {actions, routeActions} = this.props;
        let event = {...this.state.event};
        delete event.type;

        actions.updateEvent(event).then(() => {
            routeActions.push('/admin/event');
        });
    }

    createEvent() {
        let {actions, routeActions} = this.props;
        let event = {...this.state.event};
        delete event.type;

        actions.createEvent(event).then(() => {
            routeActions.push('/admin/event');
        });
    }

    handleChange(e) {
        this.state.event[e.target.id] = e.target.value;
        this.setState(this.state);
    }

    render() {
        let {intl: {formatMessage}, params, eventType} = this.props;
        let isEdit = !!params.eventId;
        let {event} = this.state;

        let submitClasses = classnames({
            'btn btn-sm': true,
            'btn-warning': isEdit,
            'btn-primary': !isEdit
        });

        return (
            <div>
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="event.title.many" />
                    </h1>
                    <Breadcrumb />
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div className="box box-info">
                                <form className="form">
                                    <div className="box-body">
                                        <div className="form-group">
                                            <label htmlFor="name">
                                                <FormattedMessage id="common.name.title" />
                                            </label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                id="name"
                                                onChange={this.handleChange}
                                                value={event.name}
                                                placeholder={formatMessage({id: 'common.name.title'})}
                                            />
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="type">
                                                <FormattedMessage id="common.type.title" />
                                            </label>
                                            <select
                                                id="type_id"
                                                className="form-control input-sm"
                                                value={event.type_id}
                                                onChange={this.handleChange}>
                                                <option disabled>
                                                    { formatMessage({id:'common.action.select.title'}) }
                                                </option>
                                                { eventType.page.results.map(item => {
                                                    return <option key={item.id} value={item.id}>{item.name}</option>;
                                                }) }
                                            </select>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="description">
                                                <FormattedMessage id="common.description.title" />
                                            </label>
                                            <textarea
                                                id="description"
                                                className="form-control input-sm"
                                                value={event.description}
                                                onChange={this.handleChange}>
                                            </textarea>
                                        </div>

                                        <div className="form-group">
                                            <label htmlFor="start">
                                                <FormattedMessage id="event.dateRange.title" />
                                            </label>
                                            <div className="input-group input-daterange">
                                                <input
                                                    type="text"
                                                    id="start"
                                                    className="form-control"
                                                    value={event.start}
                                                    onChange={this.handleChange}
                                                />
                                                <span className="input-group-addon">
                                                    <FormattedMessage id="common.to.title" />
                                                </span>
                                                <input
                                                    type="text"
                                                    id="end"
                                                    className="form-control"
                                                    value={event.end}
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                        </div>

                                        <button type="button" className={submitClasses} onClick={isEdit ? this.updateEvent : this.createEvent}>
                                            <FormattedMessage id="grid.action.submit.title.up" />
                                        </button>

                                        <a href="#" className="btn btn-link btn-sm" onClick={this.goBack}>
                                            <FormattedMessage id="common.action.cancel.title" />
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default injectIntl(EventForm);
