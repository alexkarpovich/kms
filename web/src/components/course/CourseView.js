import './CourseView.less';
import React, {Component, PropTypes} from 'react';
import {FormattedMessage} from 'react-intl';
import Breadcrumb from 'components/common/breadcrumb/Breadcrumb';
import LessonItem from './LessonItem';
import CommentBox from 'components/common/comment-box/CommentBox';

export default class CourseView extends Component {
    constructor(props) {
        super(props);

        this.postCourseComment = ::this.postCourseComment;
    }

    componentDidMount() {
        let {actions, params} = this.props;

        actions.loadCourse(params.courseId);
        actions.loadCommentList('course', params.courseId);
    }

    postCourseComment(comment) {
        let {actions, params} = this.props;

        return actions.postComment('course', params.courseId, comment).then(() => {
            actions.loadCommentList('course', params.courseId);
        });
    }

    render() {
        let {course, routeActions, auth: {authUser}, comment: {list: commentList}} = this.props;

        return (
            <div className="course-view">
                <section className="content-header">
                    <h1>
                        <FormattedMessage id="course.title.many" />
                    </h1>
                </section>
                <section className="content">
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h4>{course.item.name}</h4>
                            <div className="skills">
                                {course.item.skills.map(skill => <span key={skill.id} className="label label-success">{skill.name}</span>)}
                            </div>
                            <div className="description">
                                {course.item.description}
                            </div>
                            <div className="list-group">
                                {course.item.lessons.map((lesson, key) => {
                                    return <LessonItem key={key} lesson={lesson} routeActions={routeActions} />;
                                })}
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <CommentBox
                                authUser={authUser}
                                conversation={commentList}
                                postComment={this.postCourseComment}
                            />
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
