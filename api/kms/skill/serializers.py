from rest_framework import serializers
from .models import Skill, SkillCategory


class SubSkillCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SkillCategory


class SkillCategorySerializer(serializers.ModelSerializer):
    parent = SubSkillCategorySerializer(read_only=True)
    parent_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = SkillCategory
        fields = ('id', 'parent', 'parent_id', 'name', 'description', 'enabled')


class SkillSerializer(serializers.ModelSerializer):
    category = SkillCategorySerializer(read_only=True)
    category_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = Skill
        fields = ('id', 'category', 'category_id', 'name', 'description', 'enabled')
