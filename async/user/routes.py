from .handlers import UserListHandler, UserHandler


class UserRouterConnector(object):

    @staticmethod
    def add_routes(app):
        app.router.add_route('*', '/users', UserListHandler)
        app.router.add_route('*', '/users/{username}', UserHandler)
        return app
