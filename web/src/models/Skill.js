const SkillModel = {
    id: null,
    category: {},
    name: '',
    description: '',
    enabled: 'true'
};

export default SkillModel;
