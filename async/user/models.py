import sqlalchemy as sa
metadata = sa.MetaData()

User = sa.Table('kmsuser_user', metadata,
                sa.Column('id', sa.Integer, primary_key=True),
                sa.Column('username', sa.String(255)),
                sa.Column('email', sa.String(255)),
                sa.Column('password', sa.String(255)),
                )
