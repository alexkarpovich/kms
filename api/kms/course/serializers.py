from rest_framework import serializers
from skill.serializers import SkillSerializer
from lesson.serializers import LessonSerializer
from kmsuser.serializers import UserSerializer
from skill.models import Skill
from lesson.models import Lesson
from .models import Course


class CourseSerializer(serializers.ModelSerializer):
    skills = SkillSerializer(read_only=True, many=True)
    skill_ids = serializers.ListField(write_only=True, child=serializers.IntegerField(), required=False)
    lessons = LessonSerializer(read_only=True, many=True)
    lesson_ids = serializers.ListField(write_only=True, child=serializers.IntegerField(), required=False)
    author = UserSerializer(read_only=True)
    author_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = Course
        fields = ('id', 'name', 'description', 'complexity', 'skills', 'skill_ids', 'lessons', 'lesson_ids',
                  'author', 'author_id')

    def create(self, validated_data):
        skill_ids = validated_data.pop('skill_ids', None)
        lesson_ids = validated_data.pop('lesson_ids', None)
        author_id = validated_data.pop('author_id', self.context['request'].user.id)

        course = Course(**validated_data)
        course.save()

        course.author_id = author_id

        if skill_ids is not None:
            skills = Skill.objects.filter(id__in=skill_ids)
            course.skills = skills
        if lesson_ids is not None:
            lessons = Lesson.objects.filter(id__in=lesson_ids)
            course.lessons = lessons

        course.save()

        return course

    def update(self, course, validated_data):
        for attr, value in validated_data.items():
            if attr == 'skill_ids':
                skills = Skill.objects.filter(pk__in=value)
                course.skills = skills
            elif attr == 'lesson_ids':
                lessons = Lesson.objects.filter(pk__in=value)
                course.lessons = lessons
            else:
                setattr(course, attr, value)

        course.author_id = validated_data.pop('author_id', self.context['request'].user.id)

        course.save()

        return course
