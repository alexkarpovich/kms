import React, {Component, PropTypes} from 'react';

export default class CommonComponent extends Component {
    constructor(props) {
        super(props);

        this.goBack = ::this.goBack;
    }

    goBack() {
        this.props.routeActions.goBack();
    }
}
