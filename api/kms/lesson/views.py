from common.mixins import FilterModelViewSetMixin, CommentViewMixin, AllowAnyMixin
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from .models import Lesson, LessonType
from .serializers import LessonSerializer, LessonTypeSerializer


class LessonTypeViewSet(FilterModelViewSetMixin):
    queryset = LessonType.objects.all()
    serializer_class = LessonTypeSerializer
    search_fields = ('id', 'name', 'description')
    ordering_fields = ('id', 'name', 'description')


class LessonViewSet(AllowAnyMixin, FilterModelViewSetMixin, CommentViewMixin):
    model = Lesson
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer
    search_fields = ('id', 'name', 'type__name', 'content')
    ordering_fields = ('id', 'name', 'type', 'content')

    @detail_route(methods=['get'])
    def voted_users(self, request, pk=None):
        voted_users = Lesson.objects.get(pk=pk).get_voted_users()

        return Response(voted_users)
