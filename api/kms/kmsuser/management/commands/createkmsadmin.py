from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from kmsuser.models import User


class Command(BaseCommand):
    args = '<--username=... --email=... --password=...>'
    help = 'Create kms superuser'
    option_list = BaseCommand.option_list + (
        make_option('--username', action='store', dest='username'),
        make_option('--email', action='store', dest='email'),
        make_option('--password', action='store', dest='password'),
        )

    def handle(self, *args, **options):
        if 'username' not in options:
            raise CommandError('Option --username=... must be specified')

        if 'email' not in options:
            raise CommandError('Option --email=... must be specified')

        if 'password' not in options:
            raise CommandError('Option --password=... must be specified')

        try:
            User.objects.filter(username=options['username']).get()
        except:
            User.objects.create_superuser(options['username'],
                                             options['email'],
                                             options['password'])
