import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {routeActions} from 'react-router-redux'
import classnames from 'classnames'
import * as AuthActions from 'actions/auth'

export default class Register extends Component {
    constructor(props, context) {
        super(props, context);

        this.actions = this.props.actions;

        this.state = {
            username: '',
            password: '',
            confirmPassword: '',
            email: '',
            firstName: '',
            lastName: ''
        };

        this.handleChange = ::this.handleChange;
        this.register = ::this.register;
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.auth.authUser) {
            this.props.routeActions.push('/');
        }

        if (nextProps.auth.errors) {
            this.errors = nextProps.auth.errors;
        }
    }

    handleChange(e) {
        this.state[e.target.name] = e.target.value;

        if (this.errors && this.errors[e.target.name]) {
            delete this.errors[e.target.name];
        }

        this.setState(this.state);
    }

    register(event) {
        event.preventDefault();

        this.actions.registerUser(this.state);
    }

    render() {
        let errors = this.errors;

        return (
            <div>
                <section className="content">
                    <div className="row">
                        <div
                            className="login col-sm-offset-1 col-md-offset-2 col-lg-offset-4 col-sm-10 col-md-8 col-lg-4">
                            <div className="panel panel-default">
                                <div className="panel-heading">Register</div>
                                <div className="panel-body">
                                    <form>
                                        <div className={classnames('form-group', {'has-error': errors && errors.username})}>
                                            <label>Username</label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                name="username"
                                                onChange={this.handleChange}
                                                value={this.state.username}
                                            />
                                            {errors && errors.username ? (
                                                <div>
                                                    {errors.username.map((error, index) => <span key={`u_${index}`} className="help-block">{error}</span>)}
                                                </div>
                                            ) : ''}
                                        </div>
                                        <div className={classnames('form-group', {'has-error': errors && errors.email})}>
                                            <label>Email</label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                name="email"
                                                onChange={this.handleChange}
                                                value={this.state.email}
                                            />
                                            {errors && errors.email ? (
                                                <div>
                                                    {errors.email.map((error, index) => <span key={`e_${index}`} className="help-block">{error}</span>)}
                                                </div>
                                            ) : ''}
                                        </div>
                                        <div className={classnames('form-group', {'has-error': errors && errors.password})}>
                                            <label>Password</label>
                                            <input
                                                type="password"
                                                className="form-control input-sm"
                                                name="password"
                                                onChange={this.handleChange}
                                                value={this.state.password}
                                            />
                                            {errors && errors.password ? (
                                                <div>
                                                    {errors.password.map((error, index) => <span key={`p_${index}`} className="help-block">{error}</span>)}
                                                </div>
                                            ) : ''}
                                        </div>
                                        <div className={classnames('form-group', {'has-error': errors && errors.confirmPassword})}>
                                            <label>Confirm password</label>
                                            <input
                                                type="password"
                                                className="form-control input-sm"
                                                name="confirmPassword"
                                                onChange={this.handleChange}
                                                value={this.state.confirmPassword}
                                            />
                                            {errors && errors.confirmPassword ? (
                                                <div>
                                                    {errors.confirmPassword.map((error, index) => <span key={`cp_${index}`} className="help-block">{error}</span>)}
                                                </div>
                                            ) : ''}
                                        </div>
                                        <div className={classnames('form-group', {'has-error': errors && errors.firstName})}>
                                            <label>First name</label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                name="firstName"
                                                onChange={this.handleChange}
                                                value={this.state.firstName}
                                            />
                                            {errors && errors.firstName ? (
                                                <div>
                                                    {errors.firstName.map((error, index) => <span key={`fn_${index}`} className="help-block">{error}</span>)}
                                                </div>
                                            ) : ''}
                                        </div>
                                        <div className={classnames('form-group', {'has-error': errors && errors.lastName})}>
                                            <label>Last name</label>
                                            <input
                                                type="text"
                                                className="form-control input-sm"
                                                name="lastName"
                                                onChange={this.handleChange}
                                                value={this.state.lastName }
                                            />
                                            {errors && errors.lastName ? (
                                                <div>
                                                    {errors.lastName.map((error, index) => <span key={`ln_${index}`} className="help-block">{error}</span>)}
                                                </div>
                                            ) : ''}
                                        </div>
                                        <button
                                            type="submit"
                                            className="btn btn-sm btn-default"
                                            onClick={this.register}
                                        >Register</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default connect(
    state => state,
    dispatch => ({
        routeActions: bindActionCreators(routeActions, dispatch),
        actions: bindActionCreators({
            ...AuthActions
        }, dispatch)
    })
)(Register)
